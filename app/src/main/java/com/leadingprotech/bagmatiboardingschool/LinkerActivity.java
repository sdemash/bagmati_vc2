package com.leadingprotech.bagmatiboardingschool;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.leadingprotech.bagmatiboardingschool.account_chooser.AccountChooserAdapter;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.database_models.LoginModel;
import com.leadingprotech.bagmatiboardingschool.login.LoginActivity;
import com.leadingprotech.bagmatiboardingschool.navigation.Activity_Accounts;
import com.leadingprotech.bagmatiboardingschool.navigation.NavActivity;

import java.util.List;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;

public class LinkerActivity extends AppCompatActivity {

    public static final String MY_PREFS_NAME = "MyPrefsFile";
    public static final String SP_NAME_API_TOKEN = "apiToken";
    public String cid, passkey, client_name, img_url, tag;
    public AccountChooserAdapter accountChooserAdapter;
    String URL_2 = BASE_URL;
    String saved;
    Boolean loaded_state;
    nBulletinDatabase db;
    SharedPreferences sharedPreferences;
    Dialog dialog_accountChooser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_linker);

        db = new nBulletinDatabase(this);
        final List<LoginModel> loginModelList = db.getLoggedInAccounts();

        sharedPreferences = getSharedPreferences(SP_NAME_API_TOKEN, 0);


        if (loginModelList.size() == 0) {
            Intent intent = new Intent(LinkerActivity.this, NavActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        } else {
            if (sharedPreferences.getString("apiToken", "").trim().equals("")) {
                showAccountChooserDialog(loginModelList);
            } else {
                Intent intent = new Intent(LinkerActivity.this, Activity_Accounts.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                finish();
            }
        }

    }

    public void showAccountChooserDialog(final List<LoginModel> loginModelList) {

        dialog_accountChooser = new Dialog(this);
        dialog_accountChooser.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_accountChooser.setContentView(R.layout.dialog_accountchooser);
        dialog_accountChooser.setCanceledOnTouchOutside(true);
        dialog_accountChooser.setCancelable(false);

        final RecyclerView recyclerView = (RecyclerView) dialog_accountChooser.findViewById(R.id.recyclerView_accountChooserDialog);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        runOnUiThread(new Runnable() {
            public void run() {
                accountChooserAdapter = new AccountChooserAdapter(loginModelList, LinkerActivity.this);
                recyclerView.setAdapter(accountChooserAdapter);
            }
        });

        Button button_dialog_addaccount = (Button) dialog_accountChooser.findViewById(R.id.button_dialog_addaccount);
        button_dialog_addaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LinkerActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                finish();
            }
        });

        dialog_accountChooser.show();
    }
}
