package com.leadingprotech.bagmatiboardingschool.login;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.database_models.LoginModel;
import com.leadingprotech.bagmatiboardingschool.database_models.StudentDetailModel;
import com.leadingprotech.bagmatiboardingschool.navigation.Activity_Accounts;
import com.leadingprotech.bagmatiboardingschool.navigation.NavActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;


public class LoginActivity extends AppCompatActivity {
    public static final String SP_NAME_API_TOKEN = "apiToken";
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    private static final String DATE_PATTERN = "((19|20)\\d\\d)[/.-](0?[1-9]|1[012])[/.-](0?[1-9]|[12][0-9]|3[012])";
    private static final String CONTACT_PATTERN = "^[9]\\d{9}$";
    @BindView(R.id.input_dob)
    EditText _dobEditText;

   /*@BindView(R.id.input_dob_year)
    EditText _dobEditTextYear;
   @BindView(R.id.input_dob_month)
    EditText _dobEditTextMonth;
   @BindView(R.id.input_dob_day)
    EditText _dobEditTextDay;*/

    @BindView(R.id.input_contact)
    EditText _contactEditText;
    @BindView(R.id.btn_login)
    Button _loginButton;
    /*@Bind(R.id.link_signup)
    TextView _signupLink;*/
    nBulletinDatabase db;
    SharedPreferences sharedPreferences;
    private DatePickerDialog pickDob;
    private SimpleDateFormat dateFormatter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        db = new nBulletinDatabase(this);
      /*  dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        _dobEditText.setInputType(InputType.TYPE_NULL);

        showDobPicker();
        _dobEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickDob.show();
            }
        });*/


        sharedPreferences = getSharedPreferences(SP_NAME_API_TOKEN, 0);

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });

    }

    public void login() {

        if (!validate()) {
            onLoginFailed();
            return;
        }

//        _loginButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

//
//  final String email = _dobEditTextYear.getText().toString() + "-" + _dobEditTextMonth.getText().toString() + "-" + _dobEditTextDay.getText().toString();
        final String email = _dobEditText.getText().toString();

        final String password = _contactEditText.getText().toString();


        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String URL_FEED = BASE_URL + "student/login";
        StringRequest request = new StringRequest(Request.Method.POST,
                URL_FEED, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    final String error = jsonObject.getString("error");
                    if (error.equals("true")) {
                        Toast.makeText(LoginActivity.this, "We don't have any records of this login information.", Toast.LENGTH_SHORT).show();

                        _loginButton.setEnabled(true);

                    } else {
                        JSONObject obj = jsonObject.getJSONObject("student");

                        String fullname = obj.getString("full_name");
                        String dob_ad = obj.getString("dob_ad");
                        String dob_bs = obj.getString("dob_bs");
                        String contact = obj.getString("contact");
                        String permanent_address = obj.getString("permanent_address");
                        String temporary_address = obj.getString("temporary_address");
                        String image = obj.getString("image");
                        String api_token = obj.getString("api_token");
                        String description = obj.getString("description");

                        JSONObject obj1 = jsonObject.getJSONObject("class");
                        String classname = obj1.getString("name");
                        String routine = obj1.getString("routine");

                        SharedPreferences.Editor spEditor_LoggedIn = sharedPreferences.edit();
                        spEditor_LoggedIn.putString("apiToken", api_token);
                        spEditor_LoggedIn.apply();

                        db.addAccount(new LoginModel(api_token, fullname, image, classname));
                        db.addStudent(new StudentDetailModel(api_token, fullname, dob_ad, dob_bs, contact, permanent_address,
                                temporary_address, image, description, classname, routine));

                        Intent intent = new Intent(LoginActivity.this, Activity_Accounts.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                String errorText = "Unknown Error.";
                if (error instanceof TimeoutError) {
                    errorText = "Connection timed out.";
                }
                if (error instanceof NoConnectionError) {
                    errorText = "No Internet Connection.";
                }
                if (error instanceof AuthFailureError) {
                    errorText = "Wrong Email or Password.";
                }
                if (error instanceof com.android.volley.NetworkError) {
                    errorText = "Unable to connect to the Internet.";
                }
                if (error instanceof com.android.volley.ServerError) {
                    errorText = "Unable to connect to the Server.";
                }
                if (error instanceof com.android.volley.ParseError) {
                    errorText = "Unable to fetch data from the server.";
                }
                Toast.makeText(LoginActivity.this, errorText, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> pars = new HashMap<String, String>();
                pars.put("Content-Type", "application/x-www-form-urlencoded");
                return pars;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("username", email);
                params.put("password", password);
                return params;
            }

        };

        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });


        queue.add(request);


    }


    @Override
    public void onBackPressed() {
        List<LoginModel> loginModel = db.getLoggedInAccounts();
        if (loginModel.size() == 0) {
            Intent intent = new Intent(LoginActivity.this, NavActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else {
            Intent intent = new Intent(LoginActivity.this, Activity_Accounts.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
        finish();

    }

    public void onLoginSuccess() {
        _loginButton.setEnabled(true);
        finish();
    }

    public void onLoginFailed() {
        //Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

       /* if (!_dobEditTextMonth.getText().toString().trim().equals("") && Integer.parseInt(_dobEditTextMonth.getText().toString()) < 10) {
            _dobEditTextMonth.setText(String.format(Locale.ENGLISH, "0%d", Integer.parseInt(_dobEditTextMonth.getText().toString())));
        }
        if (!_dobEditTextDay.getText().toString().trim().equals("") && Integer.parseInt(_dobEditTextDay.getText().toString()) < 10) {
            _dobEditTextDay.setText(String.format(Locale.ENGLISH, "0%d", Integer.parseInt(_dobEditTextDay.getText().toString())));
        }*/

//        String dob = _dobEditTextYear.getText().toString() + "-" + _dobEditTextMonth.getText().toString() + "-" + _dobEditTextDay.getText().toString();
        String dob = _dobEditText.getText().toString();

        String contact = _contactEditText.getText().toString();
//        || !Pattern.matches(DATE_PATTERN, dob)

        if (dob.isEmpty()) { //|| !Pattern.matches(DATE_PATTERN, dob) || dob.length() > 10) {
//            _dobEditTextDay.setError("Enter a valid DOB");
            _dobEditText.setError("Enter a valid username.");
            valid = false;
        } else {
//            _dobEditTextDay.setError(null);
            _dobEditText.setError(null);
        }
//|| !Pattern.matches(CONTACT_PATTERN, contact)
        if (contact.isEmpty()) {// || contact.length() > 10) {
            _contactEditText.setError("Password cannot be empty.");
            valid = false;
        } else {
            _contactEditText.setError(null);
        }

        return valid;
    }


   /* private void showDobPicker() {


        Calendar newCalendar = Calendar.getInstance();
//        _dobEditText.setText(dateFormatter.format(newCalendar.getTime()));

        pickDob = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                _dobEditText.setText(dateFormatter.format(newDate.getTime()));

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


    }*/


}
