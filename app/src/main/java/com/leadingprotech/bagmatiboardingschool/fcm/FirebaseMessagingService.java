package com.leadingprotech.bagmatiboardingschool.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.SplashScreen;
import com.leadingprotech.bagmatiboardingschool.assignments.AssignmentActivity;
import com.leadingprotech.bagmatiboardingschool.attendance.AttendanceActivity;
import com.leadingprotech.bagmatiboardingschool.contact.ContactActivity;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.database_models.LoginModel;
import com.leadingprotech.bagmatiboardingschool.download.DownloadActivity;
import com.leadingprotech.bagmatiboardingschool.event.EventActivity;
import com.leadingprotech.bagmatiboardingschool.gallery_album.GalleryAlbumActivity;
import com.leadingprotech.bagmatiboardingschool.inbox.InboxActivity;
import com.leadingprotech.bagmatiboardingschool.navigation.NavActivity;
import com.leadingprotech.bagmatiboardingschool.news.NewsActivity;
import com.leadingprotech.bagmatiboardingschool.notice.NoticeActivity;
import com.leadingprotech.bagmatiboardingschool.resource.ResourceActivity;
import com.leadingprotech.bagmatiboardingschool.video_list.VideoListActivity;

import java.util.List;

import static com.leadingprotech.bagmatiboardingschool.inbox.PersonalMessageFragment.SP_NAME_API_TOKEN;


public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    Intent in;
    nBulletinDatabase db;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        db = nBulletinDatabase.getInstance(getApplicationContext());

        if (null != remoteMessage.getData().get("api_token")) { // personal student notification
            showNotification(remoteMessage.getData().get("message"), remoteMessage.getData().get("title"), remoteMessage.getData().get("type"), remoteMessage.getData().get("api_token"));
        } else { // general notifications
            showNotification(remoteMessage.getData().get("message"), remoteMessage.getData().get("title"), remoteMessage.getData().get("type"));
        }
    }

    private void showNotification(String message, String title, String type, String apiToken) {

        List<LoginModel> loginModelList = db.getLoggedInAccounts();
        Log.d("Personal Notification", "test" + apiToken + " " + type);


        if (null != type && loginModelList.size() > 0) {

            Log.d("Personal Notification", "test" + apiToken + " " + type);
            switch (type) {
//                case "notice":
//                    in = new Intent(getApplicationContext(), NoticeActivity.class);
//                    break;
                case "news":
                    in = new Intent(getApplicationContext(), NewsActivity.class);
                    break;
                case "file":
                    in = new Intent(getApplicationContext(), DownloadActivity.class);
                    break;
                case "resource":
                    in = new Intent(getApplicationContext(), ResourceActivity.class);
                    break;
                case "event":
                    in = new Intent(getApplicationContext(), EventActivity.class);
                    break;
                case "gallery":
                    in = new Intent(getApplicationContext(), GalleryAlbumActivity.class);
                    break;
                case "video_gallery":
                    in = new Intent(getApplicationContext(), VideoListActivity.class);
                    break;
                case "contact":
                    in = new Intent(getApplicationContext(), ContactActivity.class);
                    break;

                case "personal":
                    in = new Intent(getApplicationContext(), InboxActivity.class);
                    in.putExtra("position", 1);

                    break;
                case "group":
                    in = new Intent(getApplicationContext(), InboxActivity.class);
                    in.putExtra("position", 0);

                    break;
                case "attendance":
                    in = new Intent(getApplicationContext(), AttendanceActivity.class);

                    break;
                case "assignment":
                    in = new Intent(getApplicationContext(), AssignmentActivity.class);

                    break;
                default:
                    in = new Intent(getApplicationContext(), SplashScreen.class);

                    break;
            }


            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            int icon = R.mipmap.ic_launcher;

            PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), in, PendingIntent.FLAG_UPDATE_CURRENT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(icon)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);


            Notification notification = builder.build();

            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);


            if (getStudentApiToken().equals(apiToken)) {
                manager.notify((int) System.currentTimeMillis(), notification);
            }

        }

    }


    public String getStudentApiToken() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(SP_NAME_API_TOKEN, 0);
        return sharedPreferences.getString("apiToken", "NA");

    }


    // for general notification without api token sent along with it
    private void showNotification(String message, String title, String type) {


        if (null != type) {

            Log.d("General Notification", type);

            switch (type) {
                case "notice":
                    List<LoginModel> loginModel = db.getLoggedInAccounts();
                    if (loginModel.size() == 0) {
                        in = new Intent(getApplicationContext(), NavActivity.class);
                    } else {
                        in = new Intent(getApplicationContext(), NoticeActivity.class);
                    }
                    break;
                case "news":
                    in = new Intent(getApplicationContext(), NewsActivity.class);
                    break;
                case "file":
                    in = new Intent(getApplicationContext(), DownloadActivity.class);
                    break;
                case "resource":
                    in = new Intent(getApplicationContext(), ResourceActivity.class);
                    break;
                case "event":
                    in = new Intent(getApplicationContext(), EventActivity.class);
                    break;
                case "gallery":
                    in = new Intent(getApplicationContext(), GalleryAlbumActivity.class);
                    break;
                default:
                    in = new Intent(getApplicationContext(), SplashScreen.class);

                    break;
            }
        } else {
            in = new Intent(getApplicationContext(), SplashScreen.class);

        }

        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int icon = R.mipmap.ic_launcher;

        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), in, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(icon)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        Notification notification = builder.build();

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);


        manager.notify((int) System.currentTimeMillis(), notification);

    }
}
