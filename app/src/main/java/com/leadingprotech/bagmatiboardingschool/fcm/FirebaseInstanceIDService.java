package com.leadingprotech.bagmatiboardingschool.fcm;

import android.content.SharedPreferences;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.util.HashMap;
import java.util.Map;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;


public class FirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static String URL_1 = BASE_URL + "token?api_key=";

    String apiToken;

    public String getSavedToken() {
        SharedPreferences sp = getApplicationContext().getSharedPreferences("UserCid", MODE_PRIVATE);
        return sp.getString("cid", null);
    }

    @Override
    public void onTokenRefresh() {
        apiToken = getSavedToken();

        String token = FirebaseInstanceId.getInstance().getToken();
        System.out.println("THIS IS TOKEN" + token);
        registerToken(token);
    }

    private void registerToken(final String token) {

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest sr = new StringRequest(Request.Method.POST, URL_1 + apiToken, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", token);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }
}
