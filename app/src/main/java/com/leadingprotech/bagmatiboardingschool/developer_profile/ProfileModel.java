package com.leadingprotech.bagmatiboardingschool.developer_profile;


public class ProfileModel {
    String name;
    int image;
    public ProfileModel(){

    }
    public ProfileModel(String name, int image){
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
