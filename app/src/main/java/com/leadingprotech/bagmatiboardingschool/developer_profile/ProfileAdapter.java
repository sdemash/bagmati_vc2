package com.leadingprotech.bagmatiboardingschool.developer_profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.leadingprotech.bagmatiboardingschool.R;

import java.util.List;


public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.MyViewHolder> {
    List<ProfileModel> profileModelList;
    Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView text;
        public MyViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.img_view);
            text = (TextView) itemView.findViewById(R.id.txt_view);
        }
    }
    public ProfileAdapter(List<ProfileModel> profileModelList, Context context){
        this.profileModelList = profileModelList;
        this.context = context;
    }
    @Override
    public ProfileAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_profile, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProfileAdapter.MyViewHolder holder, int position) {
        final ProfileModel profileModel = profileModelList.get(position);

        holder.img.setImageResource(profileModel.getImage());
        holder.text.setText(profileModel.getName());

    }

    @Override
    public int getItemCount() {
        return profileModelList.size();
    }


}
