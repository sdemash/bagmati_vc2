package com.leadingprotech.bagmatiboardingschool.developer_profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.database_models.LoginModel;
import com.leadingprotech.bagmatiboardingschool.helper.Config;
import com.leadingprotech.bagmatiboardingschool.navigation.Activity_Accounts;
import com.leadingprotech.bagmatiboardingschool.navigation.NavActivity;

import java.util.ArrayList;
import java.util.List;

public class DeveloperActivity extends AppCompatActivity {
    List<ProfileModel> profileModelList = new ArrayList<>();
    RecyclerView recyclerView;
    ProfileAdapter profileAdapter;

    nBulletinDatabase db;
    TextView version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developer);

        db = nBulletinDatabase.getInstance(getApplicationContext());
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Developers' Profile");
        }
        version = (TextView) findViewById(R.id.versionCode);
        recyclerView = (RecyclerView) findViewById(R.id.developer_recycler);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        version.setText(String.format("%s, App Version %s (VC %s)", getResources().getString(R.string.app_name), Config.VERSION_NAME, Config.VERSION_CODE));
        prepareDeveloper();
        profileAdapter = new ProfileAdapter(profileModelList, getApplicationContext());
        recyclerView.setAdapter(profileAdapter);
    }

    private void prepareDeveloper() {
        ProfileModel profileModel = new ProfileModel("RESPONSIVE WEBSITE", R.drawable.layer11);
        profileModelList.add(profileModel);

        profileModel = new ProfileModel("IOS APPLICATION", R.drawable.layer10);
        profileModelList.add(profileModel);

        profileModel = new ProfileModel("GRAPHIC DESIGN", R.drawable.layer8);
        profileModelList.add(profileModel);

        profileModel = new ProfileModel("ANDROID APPLICATION", R.drawable.layer6);
        profileModelList.add(profileModel);

        profileModel = new ProfileModel("CREATIVE SYSTEM", R.drawable.layer5);
        profileModelList.add(profileModel);

        profileModel = new ProfileModel("CUSTOMIZED SOFTWARE", R.drawable.layer7);
        profileModelList.add(profileModel);
    }

    public void onBackpressed() {
        List<LoginModel> loginModel = db.getLoggedInAccounts();
        if (loginModel.size() == 0) {
            Intent intent = new Intent(DeveloperActivity.this, NavActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else {
            Intent intent = new Intent(DeveloperActivity.this, Activity_Accounts.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            List<LoginModel> loginModel = db.getLoggedInAccounts();
            if (loginModel.size() == 0) {
                Intent intent = new Intent(DeveloperActivity.this, NavActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                Intent intent = new Intent(DeveloperActivity.this, Activity_Accounts.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
