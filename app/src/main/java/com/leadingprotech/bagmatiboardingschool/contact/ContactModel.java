package com.leadingprotech.bagmatiboardingschool.contact;


public class ContactModel {
    private String id, name, designation, contact, email, image;
    private int order;

    public ContactModel() {

    }

    public ContactModel(String id, String name, String designation, String contact, String email, String image, int order) {
        this.id = id;
        this.name = name;
        this.designation = designation;
        this.contact = contact;
        this.email = email;
        this.image = image;
        this.order = order;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
