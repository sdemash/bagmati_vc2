package com.leadingprotech.bagmatiboardingschool.contact;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.helper.CircleTransformGlide;

import java.util.List;


public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyViewHolder> {
    private List<ContactModel> contactModelList;
    private String contact_no, email_id;
    private Context context;


    public ContactAdapter(List<ContactModel> contactModelList, Context context) {
        this.context = context;
        this.contactModelList = contactModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_contact_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final ContactModel contactModel = contactModelList.get(position);

        String img_url = contactModel.getImage();
        Glide.with(context)
                .load(img_url)
                .placeholder(R.drawable.unknown_user)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .thumbnail(0.5f).transform(new CircleTransformGlide(context))
                .into(holder.img);
        holder.name.setText(contactModel.getName());
        holder.designation.setText(contactModel.getDesignation());
        holder.phone.setText(contactModel.getContact());
        holder.email.setText(contactModel.getEmail());
        holder.row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contact_no = contactModel.getContact();
                email_id = contactModel.getEmail();
                PopupMenu popup = new PopupMenu(view.getContext(), view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
                popup.show();
            }
        });
        holder.popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contact_no = contactModel.getContact();
                email_id = contactModel.getEmail();
                PopupMenu popup = new PopupMenu(view.getContext(), view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
                popup.show();
            }


        });
    }

    @Override
    public int getItemCount() {
        return contactModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CardView row;
        TextView name, designation, phone, email;
        ImageView img, popup;

        public MyViewHolder(View itemView) {
            super(itemView);
            row = (CardView) itemView.findViewById(R.id.row_contact);
            img = (ImageView) itemView.findViewById(R.id.img_contact);
            name = (TextView) itemView.findViewById(R.id.tv_contact_name);
            designation = (TextView) itemView.findViewById(R.id.tv_contact_designation);
            phone = (TextView) itemView.findViewById(R.id.tv_contact_number);
            email = (TextView) itemView.findViewById(R.id.tv_contact_email);
            popup = (ImageView) itemView.findViewById(R.id.popup);
        }
    }

    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        public MyMenuItemClickListener() {
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.call_pop:
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + contact_no));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    context.getApplicationContext().startActivity(intent);
                    return true;
                case R.id.email_pop:
                    Intent in = new Intent(Intent.ACTION_SEND);
                    in.setType("text/plain");
                    in.putExtra(Intent.EXTRA_EMAIL, new String[]{email_id});
                    context.startActivity(in);
                    return true;
                default:
            }
            return false;
        }
    }
}