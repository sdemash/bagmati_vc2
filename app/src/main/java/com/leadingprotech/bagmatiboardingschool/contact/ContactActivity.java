package com.leadingprotech.bagmatiboardingschool.contact;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.database_models.LoginModel;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;
import com.leadingprotech.bagmatiboardingschool.helper.VolleyManager;
import com.leadingprotech.bagmatiboardingschool.navigation.Activity_Accounts;
import com.leadingprotech.bagmatiboardingschool.navigation.NavActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.API_TOKEN;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;


public class ContactActivity extends AppCompatActivity {
    LinearLayout no_data;
    ConnectionManager connectionManager;
    String id, name, designation, email, contact, data;
    int order;
    ProgressBar progressBar;
    SwipeRefreshLayout swipe;
    int spancount = 2;
    int spacing = 20;
    boolean includeEdge = true;
    nBulletinDatabase db;

    String final_URL = BASE_URL + "contact?api_key=" + API_TOKEN;
    List<com.leadingprotech.bagmatiboardingschool.contact.ContactModel> contactModelList = new ArrayList<>();
    com.leadingprotech.bagmatiboardingschool.contact.ContactAdapter contactAdapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Contacts");
        }
        db = nBulletinDatabase.getInstance(getApplicationContext());
        connectionManager = new ConnectionManager(this);

        no_data = (LinearLayout) findViewById(R.id.contact_no_data);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_contact);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_contact);
        progressBar = (ProgressBar) findViewById(R.id.progress_contact);

        if (connectionManager.isNetworkConnected()) {
            db.deleteContact();
            prepareContactData();

        } else {
            offlineData();
            Snackbar snackbar = Snackbar.make(swipe, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    prepareContactData();
                }
            });
            snackbar.show();
        }


        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(true);
                if (connectionManager.isNetworkConnected()) {
                    db.deleteContact();
                    prepareContactData();

                } else {
                    offlineData();
                    Snackbar snackbar = Snackbar.make(swipe, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareContactData();
                        }
                    });
                    snackbar.show();
                }
            }
        });

    }

    private void prepareContactData() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, final_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    contactModelList.clear();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("contact");
                    for (int i = 0; i < jsonArray.length(); ++i) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        id = object.getString("id");
                        name = object.getString("name");
                        designation = object.optString("designation");
                        email = object.optString("email");
                        contact = object.getString("contact");
                        order = i + 1;
                        data = object.getString("image_thumb");

                        contactModelList.add(new com.leadingprotech.bagmatiboardingschool.contact.ContactModel(id, name, designation, contact, email, data, order));
                        db.addContact(new com.leadingprotech.bagmatiboardingschool.contact.ContactModel(id, name, designation, contact, email, data, order));
                    }
                    if (contactModelList.size() == 0) {
                        swipe.setRefreshing(false);
                        progressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                    } else {
                        swipe.setRefreshing(false);
                        contactAdapter = new com.leadingprotech.bagmatiboardingschool.contact.ContactAdapter(contactModelList, ContactActivity.this);
                        recyclerView.setAdapter(contactAdapter);
                        progressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                offlineData();
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(swipe, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareContactData();
                        }
                    });
                    snackbar.show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(ContactActivity.this, "Oops ! We are experiencing some problem.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        VolleyManager.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

    }

    public void onlineData() {
        swipe.setRefreshing(false);
        contactModelList = db.getContacts();
        contactAdapter = new com.leadingprotech.bagmatiboardingschool.contact.ContactAdapter(contactModelList, ContactActivity.this);
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        contactAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(contactAdapter);

    }

    public void offlineData() {
        swipe.setRefreshing(false);
        contactModelList = db.getContacts();
        contactAdapter = new com.leadingprotech.bagmatiboardingschool.contact.ContactAdapter(contactModelList, ContactActivity.this);
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        contactAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(contactAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        List<LoginModel> loginModel = db.getLoggedInAccounts();
        if (loginModel.size() == 0) {
            Intent intent = new Intent(ContactActivity.this, NavActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else {
            Intent intent = new Intent(ContactActivity.this, Activity_Accounts.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            List<LoginModel> loginModel = db.getLoggedInAccounts();
            if (loginModel.size() == 0) {
                Intent intent = new Intent(ContactActivity.this, NavActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                Intent intent = new Intent(ContactActivity.this, Activity_Accounts.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
            finish();
        }
        if (id == R.id.refresh_view) {
            recyclerView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            if (connectionManager.isNetworkConnected()) {
                db.deleteContact();
                prepareContactData();

            } else {
                offlineData();
                Snackbar snackbar = Snackbar.make(swipe, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        prepareContactData();
                    }
                });
                snackbar.show();

            }
        }
        return super.onOptionsItemSelected(item);
    }

    private String getSavedToken() {
        SharedPreferences sp = getApplicationContext().getSharedPreferences("UserCid", MODE_PRIVATE);
        return sp.getString("cid", String.valueOf(sp));
    }
}
