package com.leadingprotech.bagmatiboardingschool.gallery_album;

public class GalleryAlbumModel {


    private int id;
    private String title;
    private String updated_at;
    private String cover_image;

    public GalleryAlbumModel() {

    }

    public GalleryAlbumModel(int id, String title, String updated_at, String cover_image) {
        this.id = id;
        this.title = title;
        this.updated_at = updated_at;
        this.cover_image = cover_image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCover_image() {
        return cover_image;
    }

    public void setCover_image(String cover_image) {
        this.cover_image = cover_image;
    }
}
