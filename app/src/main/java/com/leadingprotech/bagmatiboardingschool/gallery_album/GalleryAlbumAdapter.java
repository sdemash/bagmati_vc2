package com.leadingprotech.bagmatiboardingschool.gallery_album;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.leadingprotech.bagmatiboardingschool.gallery.GalleryActivity;
import com.leadingprotech.bagmatiboardingschool.R;

import java.util.List;

public class GalleryAlbumAdapter extends RecyclerView.Adapter<GalleryAlbumAdapter.MyViewHolder> {
    private List<GalleryAlbumModel> galleryAlbumModelList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_album;
        TextView title_album;
        String id;
        String time;
        String img_url;
        public MyViewHolder(View itemView) {
            super(itemView);
            img_album = (ImageView) itemView.findViewById(R.id.img_gallery_album);
            title_album = (TextView) itemView.findViewById(R.id.gallery_album_title);
        }
    }

    public GalleryAlbumAdapter(List<GalleryAlbumModel> galleryAlbumModelList, Context context){
        this.galleryAlbumModelList = galleryAlbumModelList;
        this.context = context;
    }

    @Override
    public GalleryAlbumAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_gallery_album, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GalleryAlbumAdapter.MyViewHolder holder, int position) {
        final GalleryAlbumModel galleryAlbumModel = galleryAlbumModelList.get(position);
        holder.img_url = galleryAlbumModel.getCover_image();
        Glide.with(context)
                .load(holder.img_url)
                .placeholder(R.drawable.no_thumb)
                .centerCrop()
                .into(holder.img_album);
        holder.title_album.setText(galleryAlbumModel.getTitle());
        holder.id = String.valueOf(galleryAlbumModel.getId());

        holder.img_album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(context, GalleryActivity.class);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                in.putExtra("id", holder.id);
                in.putExtra("title", holder.title_album.getText().toString());
                context.startActivity(in);
            }
        });

    }

    @Override
    public int getItemCount() {
        return galleryAlbumModelList.size();
    }


}
