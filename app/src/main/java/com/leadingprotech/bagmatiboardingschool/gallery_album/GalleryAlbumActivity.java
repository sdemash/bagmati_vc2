package com.leadingprotech.bagmatiboardingschool.gallery_album;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.database_models.LoginModel;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;
import com.leadingprotech.bagmatiboardingschool.helper.VolleyManager;
import com.leadingprotech.bagmatiboardingschool.navigation.Activity_Accounts;
import com.leadingprotech.bagmatiboardingschool.navigation.NavActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.API_TOKEN;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;

public class GalleryAlbumActivity extends AppCompatActivity {
    public ArrayList<GalleryAlbumModel> galleryAlbumModelList;
    ConnectionManager connectionManager;
    LinearLayout no_album;
    RelativeLayout root;
    ProgressBar progressBar;
    String URL_1 = BASE_URL + "gallery?api_key=" + API_TOKEN;
    RecyclerView recyclerView;
    GalleryAlbumAdapter galleryAlbumAdapter;
    TextView dataNotAvailable;

    nBulletinDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Gallery");
        }
        setContentView(R.layout.activity_gallery_album);

        db = nBulletinDatabase.getInstance(getApplicationContext());

        connectionManager = new ConnectionManager(getApplicationContext());

        no_album = (LinearLayout) findViewById(R.id.layout_no_album);
        root = (RelativeLayout) findViewById(R.id.rootLayout);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_gallery_album);
        progressBar = (ProgressBar) findViewById(R.id.progress_gallery_album);
        dataNotAvailable = (TextView) findViewById(R.id.noDataAvailable);
        galleryAlbumModelList = new ArrayList<>();
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if (connectionManager.isNetworkConnected()) {
            prepareGalleryAlbum();
        } else {
            no_album.setVisibility(View.VISIBLE);
            dataNotAvailable.setText("NO DATA FOUND");
            progressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);

            Snackbar snackbar = Snackbar.make(root, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    prepareGalleryAlbum();
                }
            });
            snackbar.show();
        }
    }

    private void prepareGalleryAlbum() {
        galleryAlbumModelList.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_1, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("gallery");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        int id = object.getInt("id");
                        String title = object.getString("title");
                        String update_at = object.getString("updated_at");
                        String cover_image = object.getString("cover_image");
                        galleryAlbumModelList.add(new GalleryAlbumModel(id, title, update_at, cover_image));
                    }


                    if (galleryAlbumModelList.isEmpty()) {
                        no_album.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.GONE);
                    } else {
                        no_album.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        galleryAlbumAdapter = new GalleryAlbumAdapter(galleryAlbumModelList, getApplicationContext());
                        recyclerView.setAdapter(galleryAlbumAdapter);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);

            }
        });
        VolleyManager.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

    }

    public String getSavedToken() {
        SharedPreferences sp = getApplicationContext().getSharedPreferences("UserCid", MODE_PRIVATE);
        return sp.getString("cid", null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        List<LoginModel> loginModel = db.getLoggedInAccounts();
        if (loginModel.size() == 0) {
            Intent intent = new Intent(GalleryAlbumActivity.this, NavActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else {
            Intent intent = new Intent(GalleryAlbumActivity.this, Activity_Accounts.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            List<LoginModel> loginModel = db.getLoggedInAccounts();
            if (loginModel.size() == 0) {
                Intent intent = new Intent(GalleryAlbumActivity.this, NavActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                Intent intent = new Intent(GalleryAlbumActivity.this, Activity_Accounts.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
            finish();
        } else if (id == R.id.refresh_view) {
            if (connectionManager.isNetworkConnected()) {
                galleryAlbumModelList.clear();
                prepareGalleryAlbum();
            } else {
                no_album.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);

                Snackbar snackbar = Snackbar.make(root, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        prepareGalleryAlbum();
                    }
                });
                snackbar.show();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
