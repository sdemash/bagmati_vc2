package com.leadingprotech.bagmatiboardingschool.about_us;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.github.siyamed.shapeimageview.mask.PorterCircularImageView;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;
import com.leadingprotech.bagmatiboardingschool.helper.VolleyManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.API_TOKEN;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;

public class AboutActivity extends AppCompatActivity {
    ConnectionManager connectionManager;
    String URL_1 = BASE_URL + "about?api_key=" + API_TOKEN;
    CardView cv1;
    CardView cv2;
    CardView cv3;
    CardView cv4;
    RelativeLayout relative, pop_menu;
    ProgressBar progressBar;
    Snackbar snackbar;
    String organization, email, cont, web, about;
    String add;
    String longitude, latitude;
    String coordinate;
    TextView name, contact, website, address;
    WebView description;
    PorterCircularImageView logo;
    // FOr FAB MENUS
    private boolean fabStatus = false;
    private FrameLayout fraToolFloat;
    private FloatingActionButton fabSetting;
    private FloatingActionButton fabSub1;
    private FloatingActionButton fabSub2;
    private FloatingActionButton fabSub3;
    private FloatingActionButton fabSub4;
    private LinearLayout linFabSetting;
    private LinearLayout linFab1;
    private LinearLayout linFab2;
    private LinearLayout linFab3;
    private LinearLayout linFab4;
    RequestManager glide;

    TextView no_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("About Us");
        }
        glide = Glide.with(this);

        fraToolFloat = (FrameLayout) findViewById(R.id.fraToolFloat);
        fabSetting = (FloatingActionButton) findViewById(R.id.fabSetting);
        fabSub1 = (FloatingActionButton) findViewById(R.id.fabSub1);
        fabSub2 = (FloatingActionButton) findViewById(R.id.fabSub2);
        fabSub3 = (FloatingActionButton) findViewById(R.id.fabSub3);
        fabSub4 = (FloatingActionButton) findViewById(R.id.fabSub4);

        linFab1 = (LinearLayout) findViewById(R.id.linFab1);
        linFab2 = (LinearLayout) findViewById(R.id.linFab2);
        linFab3 = (LinearLayout) findViewById(R.id.linFab3);
        linFab4 = (LinearLayout) findViewById(R.id.linFab4);

        cv1 = (CardView) findViewById(R.id.cvFab1);
        cv2 = (CardView) findViewById(R.id.cvFab2);
        cv3 = (CardView) findViewById(R.id.cvFab3);
        cv4 = (CardView) findViewById(R.id.cvFab4);

        linFabSetting = (LinearLayout) findViewById(R.id.linFabSetting);

        connectionManager = new ConnectionManager(getApplicationContext());
        pop_menu = (RelativeLayout) findViewById(R.id.long_listener_layout);
        no_info = (TextView) findViewById(R.id.no_info);

        name = (TextView) findViewById(R.id.about_org_name);
        contact = (TextView) findViewById(R.id.about_org_contact);
        website = (TextView) findViewById(R.id.about_org_web);
        address = (TextView) findViewById(R.id.about_org_address);
        description = (WebView) findViewById(R.id.about_org_describe);
        relative = (RelativeLayout) findViewById(R.id.relative_layout_about);
        logo = (PorterCircularImageView) findViewById(R.id.org_logo_about);
/*
        fab = (FloatingActionButton) view.findViewById(R.id.fab_about);
*/
        progressBar = (ProgressBar) findViewById(R.id.progress_about);
        coordinate = getSavedCoordinates();

        ImageView imageView = new ImageView(getApplicationContext());
        imageView.setImageResource(R.drawable.context_menu);


        fabSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fabStatus) {
                    hideFab();
                } else {
                    showFab();
                }
            }
        });

        fraToolFloat.setClickable(true);
        fabSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fabStatus) {
                    hideFab();
                } else {
                    showFab();
                }
            }
        });
        hideFab();

        // CALL
        fabSub1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (contact != null) {
                    Intent in = new Intent(Intent.ACTION_DIAL);
                    in.setData(Uri.parse("tel:" + contact.getText().toString()));
                    in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    startActivity(in);
                } else {
                    Toast.makeText(AboutActivity.this, "Call not available this time", Toast.LENGTH_SHORT).show();

                }
            }
        });


        // EMAIL
        fabSub2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (email != null) {
                    Intent in = new Intent(Intent.ACTION_SEND);
                    in.setType("text/plain");
                    in.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
                    startActivity(in);
                } else {
                    Toast.makeText(AboutActivity.this, "Cannot email at this time.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        //WEBSITE
        fabSub3.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View view) {
                                           if (website != null) {
                                               try {
                                                   Intent in = new Intent(Intent.ACTION_VIEW, Uri.parse(website.getText().toString()));
                                                   view.getContext().startActivity(in);
                                               } catch (ActivityNotFoundException exception) {
                                                   exception.printStackTrace();
                                                   Toast.makeText(AboutActivity.this, "No application to open", Toast.LENGTH_SHORT).show();
                                               }
                                           } else {
                                               Toast.makeText(AboutActivity.this, "Cannot view web at this time", Toast.LENGTH_SHORT).show();
                                           }
                                       }
                                   }
        );


        //LOCATION
        fabSub4.setOnClickListener(new View.OnClickListener()

                                   {
                                       @Override
                                       public void onClick(View view) {
                                           if (longitude != null && latitude != null) {

                                               try {
                                                   Intent in = new Intent(Intent.ACTION_VIEW,
                                                           Uri.parse("geo:0,0?q=" + longitude + "," + latitude + "(" + name.getText().toString() + ")"));
                                                   startActivity(in);
                                               } catch (ActivityNotFoundException exception) {
                                                   exception.printStackTrace();
                                                   Snackbar snackbar = Snackbar.make(description, "Application not found to open the map", Snackbar.LENGTH_SHORT).setAction("", null);
                                                   snackbar.show();
                                               }
                                           } else {
                                               Toast.makeText(AboutActivity.this, "Location not available", Toast.LENGTH_SHORT).show();
                                           }
                                       }
                                   }
        );


        if (connectionManager.isNetworkConnected()) {
            prepareAboutData();
        } else {
            progressBar.setVisibility(View.GONE);
            relative.setVisibility(View.VISIBLE);
            name.setText(getSavedTitle());
            contact.setText(getSavedContact());
            address.setText(getAddress());
            glide.load(R.drawable.institution_logo)
                    .centerCrop()
                    .into(logo);
            description.loadData(getAbout(), "text/html; charset=UTF-8", null);
            website.setText(getWebsite());
            email = getSavedEmail();

            if (getSavedCoordinates() != null) {
                String geo_array[] = getSavedCoordinates().split(",");

                try {
                    longitude = geo_array[0];
                    latitude = geo_array[1];
                } catch (Exception exception) {
                    exception.printStackTrace();

                }
            }




      /*  fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent in = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("geo:0,0?q=" + longitude + "," + latitude + "(" + add + ")"));
                    startActivity(in);
                } catch (ActivityNotFoundException exception) {
                    exception.printStackTrace();
                    Snackbar snackbar = Snackbar.make(description, "Application not found to open the map", Snackbar.LENGTH_SHORT).setAction("", null);
                    snackbar.show();
                }

            }
        });
*/
        }
    }

    private void prepareAboutData() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progressBar.setVisibility(View.GONE);
                            relative.setVisibility(View.VISIBLE);
                            JSONObject object = new JSONObject(response);
                            JSONObject aboutUs = object.getJSONObject("about");
                            Log.e("progres", String.valueOf(response));
                            organization = aboutUs.getString("organization_name");
//                            image = object.getString("image");
                            email = aboutUs.getString("email");
                            cont = aboutUs.getString("contact");
                            web = aboutUs.getString("website");
                            add = aboutUs.getString("address");
                            about = aboutUs.getString("description");


                            SharedPreferences.Editor spEditor = getSharedPreferences("Users", Context.MODE_PRIVATE).edit();
                            spEditor.putString("address", add);
                            spEditor.putString("website", web);
                            spEditor.putString("about", about);
                            spEditor.putString("contact", cont);
                            spEditor.putString("email", email);
                            spEditor.putString("name", organization);
                            spEditor.putString("coordinate", organization);


                            address.setText(add);

                            name.setText(organization);
                            contact.setText(cont);
                            website.setText(web);
                            description.loadData((about), "text/html; charset=UTF-8", null);

                            glide.load(R.drawable.institution_logo)
                                    .centerCrop()
                                    .into(logo);

                            JSONArray jsonArray = aboutUs.getJSONArray("location");
                            JSONObject obj = jsonArray.getJSONObject(0);
                            coordinate = obj.getString("coordinate");

                            spEditor.putString("coordinate", coordinate);
                            spEditor.apply();

                            String coordinate_array[] = coordinate.split(",");
                            try {
                                longitude = coordinate_array[0];
                                latitude = coordinate_array[1];
                            } catch (Exception exception) {
                                exception.printStackTrace();
                            }
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                no_info.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);

                if (error instanceof NoConnectionError) {
                    snackbar = Snackbar.make(name, "Network Error", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareAboutData();
                        }
                    });
                    snackbar.show();
                }
                if (error instanceof NetworkError) {
                    snackbar = Snackbar.make(name, "Network Error", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareAboutData();
                        }
                    });
                    snackbar.show();
                }
            }
        });
        VolleyManager.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);


    }

    public String getAddress() {
        SharedPreferences sp = getSharedPreferences("Users", Context.MODE_PRIVATE);
        return sp.getString("address", null);
    }

    public String getWebsite() {
        SharedPreferences sp = getSharedPreferences("Users", Context.MODE_PRIVATE);
        return sp.getString("website", null);
    }

    public String getAbout() {
        SharedPreferences sp = getSharedPreferences("Users", Context.MODE_PRIVATE);
        return sp.getString("about", null);
    }

    public String getSavedLogo() {
        SharedPreferences sp = getSharedPreferences("Users", Context.MODE_PRIVATE);
        return sp.getString("logo", null);
    }

    public String getSavedContact() {
        SharedPreferences sp = getSharedPreferences("Users", Context.MODE_PRIVATE);
        return sp.getString("contact", null);
    }

    public String getSavedEmail() {
        SharedPreferences sp = getSharedPreferences("Users", Context.MODE_PRIVATE);
        return sp.getString("email", null);
    }

    public String getSavedTitle() {
        SharedPreferences sp = getSharedPreferences("Users", Context.MODE_PRIVATE);
        return sp.getString("name", null);
    }

    public String getSavedCoordinates() {
        SharedPreferences sp = getSharedPreferences("Users", Context.MODE_PRIVATE);
        return sp.getString("coordinate", null);
    }


    public void onBackpressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackpressed();
        }
        if (id == R.id.refresh_view) {

            if (connectionManager.isNetworkConnected()) {
                prepareAboutData();
            } else {
                snackbar = Snackbar.make(name, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        prepareAboutData();
                    }
                });
                snackbar.show();
            }
        }
        return super.onOptionsItemSelected(item);
    }


    private void hideFab() {
        fabSub1.hide();
        fabSub2.hide();
        fabSub3.hide();
        fabSub4.hide();
        cv1.setVisibility(View.INVISIBLE);
        cv2.setVisibility(View.INVISIBLE);
        cv3.setVisibility(View.INVISIBLE);
        cv4.setVisibility(View.INVISIBLE);

        fabSetting.setImageResource(R.drawable.fab_closed);
        fraToolFloat.setBackgroundColor(Color.argb(0, 255, 255, 255));
        fabStatus = false;
    }

    private void showFab() {
        fabSub1.show();
        fabSub2.show();
        fabSub3.show();
        fabSub4.show();
        cv1.setVisibility(View.VISIBLE);
        cv2.setVisibility(View.VISIBLE);
        cv3.setVisibility(View.VISIBLE);
        cv4.setVisibility(View.VISIBLE);

        fabSetting.setImageResource(R.drawable.fab_open);
        fraToolFloat.setBackgroundColor(Color.argb(0, 255, 255, 255));
        fabStatus = true;
    }
}
