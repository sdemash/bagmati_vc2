package com.leadingprotech.bagmatiboardingschool.assignments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.services.Service_AssigmentsDownload;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AssignmentAdapter extends RecyclerView.Adapter<AssignmentAdapter.MyViewHolder> {

    private List<AssignmentModel> assignmentModelList;
    private Context context;


    public AssignmentAdapter(List<AssignmentModel> assignmentModelList, Context context) {
        this.context = context;
        this.assignmentModelList = assignmentModelList;

    }

    @Override
    public AssignmentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_assignment_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AssignmentAdapter.MyViewHolder holder, int position) {
        final AssignmentModel assignmentModel = assignmentModelList.get(position);
        holder.isLink = false;
        holder.isDocument = false;
        holder.isDownloaded = false;

        holder.title.setText(assignmentModel.getTitle());

        if (!assignmentModel.getRemark().isEmpty()) {
            holder.remarks.setVisibility(View.VISIBLE);
            holder.remarks.loadData(assignmentModel.getRemark(), "text/html; charset=UTF-8", null);
            holder.remarks.setBackgroundColor(Color.TRANSPARENT);

        } else {
            holder.remarks.setVisibility(View.GONE);
        }

        if (!assignmentModel.getDue_date().isEmpty()) {
            holder.due_date.setVisibility(View.VISIBLE);

            holder.due_date.setText(String.format("Due Date : %s", assignmentModel.getDue_date()));
        } else {
            holder.due_date.setVisibility(View.GONE);
        }
        holder.teacher.setText(String.format("Assigned by : %s", assignmentModel.getBy_teacher()));
        holder.subject.setText(assignmentModel.getSubject());

        if (!assignmentModel.getDocument().isEmpty()) {
            holder.isDocument = true;

            holder.fileName = assignmentModel.getDocument().substring(assignmentModel.getDocument().lastIndexOf("/") + 1, assignmentModel.getDocument().length());
            holder.file = new File(Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.app_name) + " Downloads/" + holder.fileName);

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy-MM-dd", Locale.US);
            Date date = null;
            try {
                date = simpleDateFormat.parse(assignmentModel.getDue_date());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (holder.file.exists()) {
                holder.img_download.setBackgroundColor(ContextCompat.getColor(context, R.color.green));
                holder.img_download.setImageResource(R.drawable.download_complete_res);
                holder.isDownloaded = true;
            }

        }

        if (!assignmentModel.getLink().isEmpty()) {
            holder.isLink = true;
        }


        holder.download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (holder.isDownloaded) {
                    String filePath = holder.file.getAbsolutePath();

                    if (holder.isLink && holder.isDocument) {

                        showDialog(assignmentModel, holder.fileName, true, filePath);

                    } else {

                        openFile(filePath);
                    }


                } else {

                    if (holder.isDocument && !holder.isLink) {
                        Intent intent = new Intent(view.getContext(), Service_AssigmentsDownload.class);
                        intent.putExtra("title", holder.fileName);
                        intent.putExtra("link", assignmentModel.getDocument().replaceAll(" ", "%20"));
                        view.getContext().startService(intent);


                    } else if (!holder.isDocument && holder.isLink) {

                        String urlLink = assignmentModel.getLink();

                        if (urlLink != null && !urlLink.isEmpty()) {
                            if (!urlLink.startsWith("https://") && !urlLink.startsWith("http://")) {
                                urlLink = "http://" + urlLink;
                            }

                            try {
                                Intent in = new Intent(Intent.ACTION_VIEW, Uri.parse(urlLink));
                                view.getContext().startActivity(in);
                            } catch (ActivityNotFoundException exception) {
                                exception.printStackTrace();
                                Toast.makeText(context, "No application to open this link.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else if (holder.isLink && holder.isDocument) {

                        showDialog(assignmentModel, holder.fileName, false, "");

                    }

                }


            }
        });
    }

    @Override
    public int getItemCount() {
        return assignmentModelList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_download;
        File file;
        String fileName;
        boolean isDocument, isLink, isDownloaded;
        CardView download;
        TextView title, subject, teacher,  due_date;
        WebView remarks;

        public MyViewHolder(View itemView) {
            super(itemView);
            img_download = (ImageView) itemView.findViewById(R.id.status_download);
            download = (CardView) itemView.findViewById(R.id.cv_download);
            title = (TextView) itemView.findViewById(R.id.assignment_title);
            subject = (TextView) itemView.findViewById(R.id.subject);

            teacher = (TextView) itemView.findViewById(R.id.teacher);

            remarks = (WebView) itemView.findViewById(R.id.remarks);
            due_date = (TextView) itemView.findViewById(R.id.due_date);


        }
    }


    public void showDialog(final AssignmentModel assignmentModel, final String fileName, final boolean downloaded, final String filePath) {

        final Dialog dialog_download = new Dialog(context, R.style.DialogTheme);
        dialog_download.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_download.getWindow().setBackgroundDrawableResource(R.color.transparent);
        dialog_download.setContentView(R.layout.dialog_assignment);
        dialog_download.setCanceledOnTouchOutside(true);
        dialog_download.setCancelable(true);

        TextView title = (TextView) dialog_download.findViewById(R.id.textTitle);
        title.setText("Check Assignment Links");

        Button downloadLink = (Button) dialog_download.findViewById(R.id.btn_download);
        Button referenceLink = (Button) dialog_download.findViewById(R.id.btn_reference);

        if (downloaded) {
            downloadLink.setText("Open File");
        }


        downloadLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (downloaded) {
                    openFile(filePath);

                } else {
                    Intent intent = new Intent(view.getContext(), Service_AssigmentsDownload.class);
                    intent.putExtra("title", fileName);
                    intent.putExtra("link", assignmentModel.getDocument().replaceAll(" ", "%20"));
                    view.getContext().startService(intent);

                }

                dialog_download.dismiss();

            }


        });


        referenceLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String urlLink = assignmentModel.getLink();

                if (urlLink != null && !urlLink.isEmpty()) {
                    if (!urlLink.startsWith("https://") && !urlLink.startsWith("http://")) {
                        urlLink = "http://" + urlLink;
                    }

                    try {
                        Intent in = new Intent(Intent.ACTION_VIEW, Uri.parse(urlLink));
                        view.getContext().startActivity(in);
                    } catch (ActivityNotFoundException exception) {
                        exception.printStackTrace();
                        Toast.makeText(context, "No application to open this link.", Toast.LENGTH_SHORT).show();
                    }
                }

                dialog_download.dismiss();


            }


        });

        dialog_download.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);

        dialog_download.show();

        //Set the dialog to immersive
        dialog_download.getWindow().getDecorView().setSystemUiVisibility(
                ((Activity) context).getWindow().getDecorView().getSystemUiVisibility());

//Clear the not focusable flag from the window
        dialog_download.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    }

    public void openFile(String filePath) {
        try {
            Uri uri = Uri.parse("file://" + filePath);
            Intent in = new Intent(android.content.Intent.ACTION_VIEW);
            if (filePath.contains(".doc") || filePath.contains(".docx")) {
                in.setDataAndType(uri, "application/msword");
            } else if (filePath.contains(".pdf")) {
                in.setDataAndType(uri, "application/pdf");
            } else if (filePath.contains(".ppt") || uri.toString().contains(".pptx")) {
                in.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (filePath.contains(".xls") || uri.toString().contains(".xlsx")) {
                in.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (filePath.contains(".zip") || filePath.contains(".rar")) {
                in.setDataAndType(uri, "application/x.wav");
            } else if (filePath.contains(".rtf")) {
                // RTF file
                in.setDataAndType(uri, "application/rtf");
            } else if (filePath.contains(".wav") || filePath.contains(".mp3")) {
                // WAV audio file
                in.setDataAndType(uri, "audio/x-wav");
            } else if (filePath.contains(".gif")) {
                // GIF file
                in.setDataAndType(uri, "image/gif");
            } else if (filePath.contains(".jpg") || filePath.contains(".jpeg") || filePath.contains(".png")) {
                // JPG file
                in.setDataAndType(uri, "image/jpeg");
            } else if (filePath.contains(".txt")) {
                // Text file
                in.setDataAndType(uri, "text/plain");
            } else if (filePath.contains(".3gp") || filePath.contains(".mpg") || filePath.contains(".mpeg") || filePath.contains(".mpe") || filePath.contains(".mp4") || filePath.contains(".avi")) {
                // Video files
                in.setDataAndType(uri, "video/*");
            } else {
                in.setDataAndType(uri, "*/*");
            }
            in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(in);
        } catch (ActivityNotFoundException exception) {
            Toast.makeText(context, "No application found to open the file.", Toast.LENGTH_SHORT).show();
        }
    }

}
