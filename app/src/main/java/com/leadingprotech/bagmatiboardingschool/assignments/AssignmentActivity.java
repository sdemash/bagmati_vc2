package com.leadingprotech.bagmatiboardingschool.assignments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.database_models.LoginModel;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;
import com.leadingprotech.bagmatiboardingschool.helper.VolleyManager;
import com.leadingprotech.bagmatiboardingschool.navigation.Activity_Accounts;
import com.leadingprotech.bagmatiboardingschool.navigation.NavActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;

public class AssignmentActivity extends AppCompatActivity {

    public static final String SP_NAME_API_TOKEN = "apiToken";


    protected List<AssignmentModel> assignmentModelList = new ArrayList<>();
    protected AssignmentAdapter assignmentAdapter;
    ConnectionManager connectionManager;
    TextView error_msg;
    ProgressBar progressBar;
    String URL = BASE_URL + "student/assignment?api_token=";
    SwipeRefreshLayout swipe;
    RecyclerView recyclerView;
    nBulletinDatabase db;
    RelativeLayout noData;

    private BroadcastReceiver SentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            prepareAssignmentsData();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment);

        db = nBulletinDatabase.getInstance(getApplicationContext());
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Homework/Assignments");
        }
        noData = (RelativeLayout) findViewById(R.id.no_data);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_download);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_downloads);
        progressBar = (ProgressBar) findViewById(R.id.progress_download);
        error_msg = (TextView) findViewById(R.id.error_msg);
        connectionManager = new ConnectionManager(this);

        check_Permission();

        registerDownloadBroadcast();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (connectionManager.isNetworkConnected()) {
                    prepareAssignmentsData();
                }
            }
        }, 1500);

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(true);
                if (connectionManager.isNetworkConnected()) {
                    prepareAssignmentsData();
                } else {
                    progressBar.setVisibility(View.GONE);

                    swipe.setRefreshing(false);
                    loadOfflineData();
                    Snackbar snackbar = Snackbar.make(swipe, "No Internet Connection", Snackbar.LENGTH_SHORT).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareAssignmentsData();
                        }
                    });
                    snackbar.show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        List<LoginModel> loginModel = db.getLoggedInAccounts();
        if (loginModel.size() == 0) {
            Intent intent = new Intent(AssignmentActivity.this, NavActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else {
            Intent intent = new Intent(AssignmentActivity.this, Activity_Accounts.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            List<LoginModel> loginModel = db.getLoggedInAccounts();
            if (loginModel.size() == 0) {
                Intent intent = new Intent(AssignmentActivity.this, NavActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                Intent intent = new Intent(AssignmentActivity.this, Activity_Accounts.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    private void prepareAssignmentsData() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL + getApiToken(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);

                            if (!object.getBoolean("error")) {
                                assignmentModelList.clear();
                                progressBar.setVisibility(View.GONE);
                                JSONArray jsonArray = object.getJSONArray("assignment");

                                if (jsonArray.length() > 0) {
                                    db.deleteAssignments(getApiToken());
                                }
                                for (int i = 0; i < jsonArray.length(); ++i) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    String id = jsonObject.getString("id");
                                    String title = jsonObject.getString("title");
                                    String link = jsonObject.getString("link");
                                    String subject = jsonObject.getString("subject");
                                    String due_date = jsonObject.getString("due_date");
                                    String document = jsonObject.getString("document");
                                    String remark = jsonObject.getString("remark");
                                    String teacher_name = jsonObject.getString("by_teacher");
                                    String given_date = jsonObject.getString("updated_at");

                                    db.addAssignmentRecord(new AssignmentModel(id, subject, title, document, link, remark, due_date, given_date, teacher_name), getApiToken());
                                }
                            }
                            swipe.setRefreshing(false);
                            loadOfflineData();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipe.setRefreshing(false);

                loadOfflineData();

                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(swipe, "No Internet Connection", Snackbar.LENGTH_SHORT).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareAssignmentsData();
                        }
                    });
                    snackbar.show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(AssignmentActivity.this, "Oops ! We are experiencing some problem.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyManager.getInstance(this).addToRequestQueue(stringRequest);

    }

    private void loadOfflineData() {
        assignmentModelList.clear();
        assignmentModelList = db.getAllAssignments(getApiToken());

        if (assignmentModelList.size() == 0) {
            noData.setVisibility(View.VISIBLE);
        } else {
            noData.setVisibility(View.GONE);
        }
        assignmentAdapter = new AssignmentAdapter(assignmentModelList, AssignmentActivity.this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(AssignmentActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(assignmentAdapter);
    }

    public String getApiToken() {
        SharedPreferences sp = getSharedPreferences(SP_NAME_API_TOKEN, 0);
        return sp.getString("apiToken", "");
    }


    private void check_Permission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(AssignmentActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 100);
            } else {
                loadOfflineData();
            }
        } else {
            loadOfflineData();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 100: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    prepareAssignmentsData();

                } else {
                    Toast.makeText(AssignmentActivity.this, "Permission was not granted. :(", Toast.LENGTH_SHORT).show();
                    finish();
                }
                return;
            }
        }
    }

    public void registerDownloadBroadcast() {
        LocalBroadcastManager.getInstance(AssignmentActivity.this).registerReceiver(
                SentReceiver, new IntentFilter("com.leadingprotech.assignment.downloaded"));
    }

    public void unRegisterDownloadBroadcast() {
        LocalBroadcastManager.getInstance(AssignmentActivity.this)
                .unregisterReceiver(SentReceiver);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unRegisterDownloadBroadcast();

    }
}
