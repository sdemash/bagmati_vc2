package com.leadingprotech.bagmatiboardingschool.resource;


public class ResourceModel {
    String id, title, url,  date_time;

    public ResourceModel(String id, String title, String url, String date_time){
        this.id = id;
        this.title = title;
        this.url = url;
        this.date_time = date_time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }
}

