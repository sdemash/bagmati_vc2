package com.leadingprotech.bagmatiboardingschool.resource;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.leadingprotech.bagmatiboardingschool.R;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ResourceAdapter extends RecyclerView.Adapter<ResourceAdapter.MyViewHolder> {
    private List<ResourceModel> resourceModelList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CardView resource;
        String date_time, resource_id, url;
        TextView number_date, date, title, description;
        public MyViewHolder(View itemView) {
            super(itemView);
            resource = (CardView) itemView.findViewById(R.id.cv_resource);
            number_date = (TextView) itemView.findViewById(R.id.date_number_resource);
            date = (TextView) itemView.findViewById(R.id.date_resource);
            title = (TextView) itemView.findViewById(R.id.title_resource);
            description= (TextView) itemView.findViewById(R.id.description_resource);
        }
    }
    public ResourceAdapter(List<ResourceModel> resourceModelList){
        this.resourceModelList = resourceModelList;
    }

    @Override
    public ResourceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_resource_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ResourceAdapter.MyViewHolder holder, int position) {
        ResourceModel resourceModel =  resourceModelList.get(position);
        holder.resource_id = resourceModel.getId();
        holder.title.setText(resourceModel.getTitle());
        holder.description.setText(resourceModel.getUrl());

       holder.date_time = resourceModel.getDate_time();

       SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(holder.date_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        Log.d("aa", month + "");
        int day = cal.get(Calendar.DAY_OF_MONTH);
        String monthString = new DateFormatSymbols().getShortMonths()[month];
        final String tarik = day +"";
        final String bakii = monthString + ", "+  year;
        SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
        String dayT = outFormat.format(date);

        holder.url = resourceModel.getUrl();
        holder.number_date.setText(tarik);
        holder.date.setText(bakii);
        holder.resource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(holder.url));
                    view.getContext().startActivity(browserIntent);
                }
                catch (Exception exception){
                    exception.printStackTrace();
                    Toast.makeText(view.getContext(), "An error occured", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return resourceModelList.size();
    }


}
