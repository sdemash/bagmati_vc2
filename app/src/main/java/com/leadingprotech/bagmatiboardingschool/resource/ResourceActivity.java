package com.leadingprotech.bagmatiboardingschool.resource;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.database_models.LoginModel;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;
import com.leadingprotech.bagmatiboardingschool.helper.VolleyManager;
import com.leadingprotech.bagmatiboardingschool.navigation.Activity_Accounts;
import com.leadingprotech.bagmatiboardingschool.navigation.NavActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.API_TOKEN;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;

public class ResourceActivity extends AppCompatActivity {
    ConnectionManager connectionManager;
    LinearLayout no_data;
    TextView dataNotAvailable;
    String URL = BASE_URL + "resource?api_key=" + API_TOKEN;

    nBulletinDatabase db;
    SwipeRefreshLayout swiper;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    ResourceAdapter resourceAdapter;
    private List<ResourceModel> resourceModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resource);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Resources");
        }

        connectionManager = new ConnectionManager(this);
        no_data = (LinearLayout) findViewById(R.id.linear_no_resource);
        db = nBulletinDatabase.getInstance(getApplicationContext());
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_resource);
        swiper = (SwipeRefreshLayout) findViewById(R.id.swipe_resource);
        progressBar = (ProgressBar) findViewById(R.id.progress_resource);
        dataNotAvailable = (TextView) findViewById(R.id.noDataAvailable);


        if (connectionManager.isNetworkConnected()) {
            prepareResourceData();
        } else {
            progressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
            dataNotAvailable.setText("NO DATA FOUND");
        }


        swiper.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swiper.setRefreshing(true);
                if (connectionManager.isNetworkConnected()) {
                    prepareResourceData();
                } else {
                    swiper.setRefreshing(false);
                    Snackbar snackbar = Snackbar.make(swiper, "No Internet Connection", Snackbar.LENGTH_SHORT).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareResourceData();
                        }
                    });
                    snackbar.show();
                }

            }
        });
    }


    private void prepareResourceData() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            resourceModelList.clear();
                            progressBar.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            JSONArray jsonArray = jsonObject.getJSONArray("resource");
                            for (int i = 0; i < jsonArray.length(); ++i) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String id = object.getString("id");
                                String title = object.getString("title");
                                String url = object.getString("url");
                                String date_time = object.getString("updated_at");
                                resourceModelList.add(new ResourceModel(id, title, url, date_time));
                            }
                            if (resourceModelList.size() == 0) {
                                swiper.setRefreshing(false);
                                recyclerView.setVisibility(View.GONE);
                                progressBar.setVisibility(View.GONE);
                                no_data.setVisibility(View.VISIBLE);
                            } else {
                                swiper.setRefreshing(false);
                                resourceAdapter = new ResourceAdapter(resourceModelList);
                                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                                recyclerView.setLayoutManager(layoutManager);
                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                recyclerView.setAdapter(resourceAdapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);

                if (error instanceof TimeoutError) {
                    Snackbar snackbar = Snackbar.make(swiper, "Time Out Error", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareResourceData();
                        }
                    });
                    snackbar.show();

                } else if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(swiper, "No Internet Connection", Snackbar.LENGTH_SHORT).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareResourceData();
                        }
                    });
                    snackbar.show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(ResourceActivity.this, "Oops ! We are experiencing some problem.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        VolleyManager.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        List<LoginModel> loginModel = db.getLoggedInAccounts();
        if (loginModel.size() == 0) {
            Intent intent = new Intent(ResourceActivity.this, NavActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else {
            Intent intent = new Intent(ResourceActivity.this, Activity_Accounts.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            List<LoginModel> loginModel = db.getLoggedInAccounts();
            if (loginModel.size() == 0) {
                Intent intent = new Intent(ResourceActivity.this, NavActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                Intent intent = new Intent(ResourceActivity.this, Activity_Accounts.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
            finish();
        }
        if (id == R.id.refresh_view) {
            recyclerView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            if (connectionManager.isNetworkConnected()) {
                no_data.setVisibility(View.GONE);
                prepareResourceData();
            } else {
                no_data.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                Snackbar snackbar = Snackbar.make(swiper, "No Internet Connection", Snackbar.LENGTH_SHORT).setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        prepareResourceData();
                    }
                });
                snackbar.show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public String getSavedToken() {
        SharedPreferences sp = getApplicationContext().getSharedPreferences("UserCid", MODE_PRIVATE);
        return sp.getString("cid", null);
    }
}
