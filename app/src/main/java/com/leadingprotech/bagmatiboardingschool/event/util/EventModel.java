package com.leadingprotech.bagmatiboardingschool.event.util;

public class EventModel {

    /**
     * id : 8
     * title : exam holiday for the BCT 2069 on the occassion of the first term examination for the success result. Best of luck
     * start_date : 2016-12-18
     * start_time : 02:36:18
     * end_date : 2016-12-30
     * end_time : 13:15:00
     * is_all_day : 0
     * background_color : #0f0
     * description :
     */

    private String id;
    private int eventId;
    private String title;
    private String date;
    private String monthYear;
    private String start_time;
    private String end_time;
    private int is_all_day;
    private String background_color;
    private String description;



    public EventModel(String id, int eventId, String title, String date, String monthYear, String start_time, String end_time, int is_all_day, String background_color, String description) {
        this.id = id;
        this.eventId = eventId;
        this.title = title;
        this.date = date;
        this.monthYear = monthYear;
        this.start_time = start_time;
        this.end_time = end_time;
        this.is_all_day = is_all_day;
        this.background_color = background_color;
        this.description = description;
    }

    public EventModel() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMonthYear() {
        return monthYear;
    }

    public void setMonthYear(String monthYear) {
        this.monthYear = monthYear;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public int getIs_all_day() {
        return is_all_day;
    }

    public void setIs_all_day(int is_all_day) {
        this.is_all_day = is_all_day;
    }

    public String getBackground_color() {
        return background_color;
    }

    public void setBackground_color(String background_color) {
        this.background_color = background_color;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
