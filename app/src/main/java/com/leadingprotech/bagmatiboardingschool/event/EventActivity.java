package com.leadingprotech.bagmatiboardingschool.event;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.database_models.LoginModel;
import com.leadingprotech.bagmatiboardingschool.event.adapter.CalendarListAdapter;
import com.leadingprotech.bagmatiboardingschool.event.adapter.EventListAdapter;
import com.leadingprotech.bagmatiboardingschool.event.util.EventModel;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;
import com.leadingprotech.bagmatiboardingschool.helper.VolleyManager;
import com.leadingprotech.bagmatiboardingschool.navigation.Activity_Accounts;
import com.leadingprotech.bagmatiboardingschool.navigation.NavActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.API_TOKEN;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;

public class EventActivity extends AppCompatActivity {
    //final String URL = BASE_URL+"event?api_key="+API_TOKEN;
    final String URL = BASE_URL + "event?api_key=" + API_TOKEN;
    public GregorianCalendar cal_month, cal_month_copy;
    String monthYear;
    RelativeLayout header;
    ConnectionManager connectionManager;
    View view, view2, view3;
    TableRow tableRow;
    List<EventModel> eventModelList = new ArrayList<>();
    List<EventModel> eventDateList = new ArrayList<>();
    nBulletinDatabase db;
    RecyclerView gridview;
    RecyclerView.ItemDecoration itemDecoration;
    private CalendarListAdapter cal_adapter;
    private TextView tv_month;
    private RecyclerView recyclerView_List;
    private EventListAdapter list_adapter;
    private DividerItemDecoration eventItemDecoration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarEvents);
        setSupportActionBar(toolbar);

        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Events");
        }
        db = nBulletinDatabase.getInstance(getApplicationContext());
        connectionManager = new ConnectionManager(this);
        header = (RelativeLayout) findViewById(R.id.header);
//        view = (View) findViewById(R.id.View);
        view2 = (View) findViewById(R.id.View2);
        view3 = (View) findViewById(R.id.View3);
        tableRow = (TableRow) findViewById(R.id.tableRow1);
        gridview = (RecyclerView) findViewById(R.id.gv_calendar);
        recyclerView_List = (RecyclerView) findViewById(R.id.lv_android);
        eventItemDecoration = new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL);
        itemDecoration = new DividerItemDecorationGrid(
                getResources().getDimensionPixelSize(R.dimen.photos_list_spacing),
                getResources().getInteger(R.integer.photo_list_preview_columns));


        eventDateList = db.getAllEvents();
        if ((connectionManager.isNetworkConnected() && eventDateList.size() == 0) || connectionManager.isNetworkConnected()) {

            prepareEvents();

        } else {
            createUI();

        }

        final GestureDetector mGestureDetector = new GestureDetector(getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });


        gridview.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());


                if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {

                    int position = recyclerView.getChildPosition(child);
                    ((CalendarListAdapter) recyclerView.getAdapter()).setSelected(child, position);
                    String selectedGridDate = CalendarListAdapter.day_string
                            .get(position);

                    String[] separatedTime = selectedGridDate.split("-");
                    String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                    int gridvalue = Integer.parseInt(gridvalueString);

                    if ((gridvalue > 10) && (position < 8)) {
                        setPreviousMonth();
                        refreshCalendar();
                    } else if ((gridvalue < 7) && (position > 28)) {
                        setNextMonth();
                        refreshCalendar();
                    }
                    ((CalendarListAdapter) recyclerView.getAdapter()).setSelected(child, position);


                    ((CalendarListAdapter) recyclerView.getAdapter()).getPositionList(selectedGridDate, EventActivity.this);


                }

                return false;
            }


            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    public void createUI() {
        header.setVisibility(View.VISIBLE);
//        view.setVisibility(View.VISIBLE);
        view2.setVisibility(View.VISIBLE);
        view3.setVisibility(View.VISIBLE);
        tableRow.setVisibility(View.VISIBLE);
        gridview.setVisibility(View.VISIBLE);
        recyclerView_List.setVisibility(View.VISIBLE);
        cal_month = (GregorianCalendar) GregorianCalendar.getInstance();
        cal_month_copy = (GregorianCalendar) cal_month.clone();
        eventDateList = db.getAllEvents();
        cal_adapter = new CalendarListAdapter(this, cal_month, eventDateList);


        tv_month = (TextView) findViewById(R.id.tv_month);
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
        monthYear = tv_month.getText().toString();
        getEventList();
        ImageButton previous = (ImageButton) findViewById(R.id.ib_prev);

        previous.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setPreviousMonth();
                refreshCalendar();
            }
        });

        ImageButton next = (ImageButton) findViewById(R.id.Ib_next);
        next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setNextMonth();
                refreshCalendar();

            }
        });

        gridview.removeItemDecoration(itemDecoration);
        gridview.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 7);
        gridview.setLayoutManager(layoutManager);
        gridview.addItemDecoration(itemDecoration);
        cal_adapter.notifyDataSetChanged();
        gridview.setAdapter(cal_adapter);


    }

    public void getEventList() {
        eventModelList = db.getEvents(monthYear);
        recyclerView_List.removeItemDecoration(eventItemDecoration);
        recyclerView_List.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView_List.setLayoutManager(layoutManager);
        recyclerView_List.addItemDecoration(eventItemDecoration);
        list_adapter = new EventListAdapter(EventActivity.this, eventModelList);
        list_adapter.notifyDataSetChanged();
        recyclerView_List.setAdapter(list_adapter);

    }

    protected void setNextMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1),
                    cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) + 1);
        }

    }

    protected void setPreviousMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMinimum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1),
                    cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) - 1);
        }

    }

    public void refreshCalendar() {
        cal_adapter.refreshDays();
        cal_adapter.notifyDataSetChanged();
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
        monthYear = tv_month.getText().toString();
        list_adapter.notifyDataSetChanged();
        getEventList();
    }

    public void prepareEvents() {
        header.setVisibility(View.GONE);
//        view.setVisibility(View.GONE);
        view2.setVisibility(View.GONE);
        view3.setVisibility(View.GONE);
        tableRow.setVisibility(View.GONE);
        gridview.setVisibility(View.GONE);
        recyclerView_List.setVisibility(View.GONE);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar3);
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("event");

                    if (jsonArray.length() > 0) {
                        db.deleteEvent();
                    }
                    for (int i = 0; i < jsonArray.length(); ++i) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        int eventId = object.getInt("id");
                        String title = object.getString("title");
                        String start_date = object.getString("start_date");
                        String start_time = object.getString("start_time");
                        String end_date = object.getString("end_date");
                        String end_time = object.getString("end_time");
                        int isadllday = object.getInt("is_all_day");
                        String background_color = object.getString("background_color");
                        String description = object.getString("description");

                        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        Date startDate = null;
                        Date endDate = null;
                        try {
                            startDate = formatter.parse(start_date);
                            System.out.println(startDate);
                            endDate = formatter.parse(end_date);
                            System.out.println(endDate);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        Calendar start = Calendar.getInstance();
                        start.setTime(startDate);

                        Calendar end = Calendar.getInstance();
                        end.setTime(endDate);

                        while (!start.after(end)) {
                            Date targetDay = start.getTime();
                            // Do Work Here

                            final SimpleDateFormat newFormat2 = new SimpleDateFormat("MMMM yyyy");
                            String date = formatter.format(targetDay);
                            String monthYear = newFormat2.format(targetDay);
                            Log.d("date", date);
                            Log.d("full", monthYear);
                            db.addEvent(new EventModel("", eventId, title, date, monthYear, start_time, end_time, isadllday, background_color, description));
                            start.add(Calendar.DATE, 1);
                        }

                    }
                    progressBar.setVisibility(View.GONE);
                    createUI();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                createUI();
                if (error instanceof NoConnectionError) {

                    Snackbar snackbar = Snackbar.make(header, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    });
                    snackbar.show();
                } else if (error instanceof ServerError) {

                    Toast.makeText(EventActivity.this, "Oops ! We are experiencing some problem.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        VolleyManager.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

    }


    @Override
    public void onBackPressed() {
        List<LoginModel> loginModel = db.getLoggedInAccounts();
        if (loginModel.size() == 0) {
            Intent intent = new Intent(EventActivity.this, NavActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else {
            Intent intent = new Intent(EventActivity.this, Activity_Accounts.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            List<LoginModel> loginModel = db.getLoggedInAccounts();
            if (loginModel.size() == 0) {
                Intent intent = new Intent(EventActivity.this, NavActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                Intent intent = new Intent(EventActivity.this, Activity_Accounts.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
            finish();
        }
        if (id == R.id.refresh_view) {
            if (connectionManager.isNetworkConnected()) {
                prepareEvents();
            } else {
                createUI();
                Snackbar snackbar = Snackbar.make(header, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        prepareEvents();
                    }
                });
                snackbar.show();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
