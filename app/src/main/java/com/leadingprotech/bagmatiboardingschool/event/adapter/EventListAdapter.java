package com.leadingprotech.bagmatiboardingschool.event.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.leadingprotech.bagmatiboardingschool.event.ViewEventActivity;
import com.leadingprotech.bagmatiboardingschool.event.util.EventModel;
import com.leadingprotech.bagmatiboardingschool.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.MyViewHolder> {

    private final Context context;
    private List<EventModel> eventModelList;


    public EventListAdapter(Context context, List<EventModel> contactList) {

        this.context = context;
        this.eventModelList = contactList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final EventModel eventModel = eventModelList.get(position);

        if (position == 0){
            holder.separator.setVisibility(View.INVISIBLE);
        }else{
            holder.separator.setVisibility(View.VISIBLE);
        }

        SimpleDateFormat monthDay = new SimpleDateFormat("MMM dd - EEEE");
        final SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        final SimpleDateFormat date2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String start = null;
        String end = null;
        Date startDate =new Date();
        Date endDate =new Date();
        Date eventDay = new Date();
        try {
            eventDay = date.parse(eventModel.getDate());
            startDate = date2.parse(eventModel.getDate()+" " + eventModel.getStart_time());
            endDate = date2.parse(eventModel.getDate()+" " + eventModel.getEnd_time());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        start= date2.format(startDate);
        end= date2.format(endDate);
        holder.tv_date.setText(monthDay.format(eventDay));
        holder.tv_event.setText(eventModel.getTitle());
        final String finalStart = start;
        final String finalEnd = end;
        if(eventModel.getIs_all_day()==0){
            holder.allday= "false";
        }
        else {
            holder.allday= "true";
        }
        holder.row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(view.getContext(), ViewEventActivity.class);

                in.putExtra("title", eventModel.getTitle());
                in.putExtra("date", eventModel.getDate());
                if (eventModel.getStart_time().equals("") || eventModel.getEnd_time().equals("")) {
                    in.putExtra("time", "");
                } else {
                    in.putExtra("time", eventModel.getStart_time() + "-" + eventModel.getEnd_time());

                }
                in.putExtra("allday", holder.allday);
                in.putExtra("start", eventModel.getStart_time());
                in.putExtra("end", eventModel.getEnd_time());
                in.putExtra("description", eventModel.getDescription());
                view.getContext().startActivity(in);
            }
        });
    }

    @Override
    public int getItemCount() {
        return eventModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_date, tv_event;
        LinearLayout row;
        View separator;
        String allday;


        public MyViewHolder(View view) {
            super(view);
            separator = (View) view.findViewById(R.id.lineSeparator);
            row = (LinearLayout) view.findViewById(R.id.eventRowitem);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            tv_event = (TextView) view.findViewById(R.id.tv_event);

        }
    }


}
