package com.leadingprotech.bagmatiboardingschool.event;

import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.navigation.NavActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ViewEventActivity extends AppCompatActivity {
    FloatingActionButton fab_event;
    Boolean allday;

    TextView title, time, date, initial, message;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_event);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("View Event Details");
        }
        title = (TextView) findViewById(R.id.view_event_title);
        time = (TextView) findViewById(R.id.view_time_event);
        date = (TextView) findViewById(R.id.view_event_date);
        message = (TextView) findViewById(R.id.view_event_message);
        initial = (TextView) findViewById(R.id.img_view_event);
        fab_event = (FloatingActionButton) findViewById(R.id.share_fab_events);

        title.setText(getIntent().getStringExtra("title"));
        if (getIntent().getStringExtra("time").equals("")) {
            time.setVisibility(View.GONE);
        } else {
            time.append(getIntent().getStringExtra("time"));
        }
        if (getIntent().getStringExtra("allday").equals("false")) {
            allday = false;
        } else {
            allday = true;
        }


        SimpleDateFormat formatDate = new SimpleDateFormat("MMM dd, yyyy");
        SimpleDateFormat eventdate = new SimpleDateFormat("yyyy-MM-dd");
        Date eventDay = new Date();
        String eventDate = "";
        try {
            eventDay = eventdate.parse(getIntent().getStringExtra("date"));
            eventDate = formatDate.format(eventDay);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String start = getIntent().getStringExtra("start");
        String[] startTime = start.split(":");
        //Toast.makeText(this, startTime[0], Toast.LENGTH_SHORT).show();
        final Calendar beginCal = Calendar.getInstance();
        beginCal.setTime(eventDay);
        beginCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(startTime[0]));
        beginCal.set(Calendar.MINUTE, Integer.parseInt(startTime[1]));

        String end = getIntent().getStringExtra("end");
        String[] endTime = end.split(":");

        final Calendar endCal = Calendar.getInstance();
        endCal.setTime(eventDay);
        endCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(endTime[0]));
        endCal.set(Calendar.MINUTE, Integer.parseInt(endTime[1]));

        date.setText(eventDate);
        message.setText(getIntent().getStringExtra("description"));


        String first = title.getText().toString().substring(0, 1).toUpperCase();
        initial.setText(first);
        final String finalEventDate = eventDate;
        fab_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_INSERT)
                        .setData(CalendarContract.Events.CONTENT_URI)
                        .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginCal.getTimeInMillis())
                        .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endCal.getTimeInMillis())
                        .putExtra(CalendarContract.Events.TITLE, getIntent().getStringExtra("title"))
                        .putExtra(CalendarContract.Events.DESCRIPTION, getIntent().getStringExtra("description"))
                        .putExtra(CalendarContract.Events.ALL_DAY, allday);

                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.view_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        if (id == R.id.home_view) {
            Intent in = new Intent(getApplicationContext(), NavActivity.class);
            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(in);
        }
        return super.onOptionsItemSelected(item);
    }
}
