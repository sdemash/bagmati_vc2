package com.leadingprotech.bagmatiboardingschool.leave_request;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;
import com.leadingprotech.bagmatiboardingschool.helper.VolleyManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;

public class Activity_LeaveRequest extends AppCompatActivity implements View.OnClickListener {
    public static final String SP_NAME_API_TOKEN = "apiToken";
    final String URL_1 = BASE_URL + "student/leave?api_token=";
    ConnectionManager connectionManager;
    Snackbar snackbar;
    TextInputLayout one, two, three;
    EditText startDate, endDate, description;
    Button send;
    String CLIENT_APITOKEN;
    SharedPreferences sharedPreferences;


    nBulletinDatabase db;


    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    private SimpleDateFormat dateFormatter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_request);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Leave Request Form");
        }
        sharedPreferences = getSharedPreferences(SP_NAME_API_TOKEN, 0);
        CLIENT_APITOKEN = sharedPreferences.getString("apiToken", "");

        db = nBulletinDatabase.getInstance(getApplicationContext());
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        connectionManager = new ConnectionManager(Activity_LeaveRequest.this);
        startDate = (EditText) findViewById(R.id.startDate_leaveRequest);
        endDate = (EditText) findViewById(R.id.endDate_leaveRequest);
        description = (EditText) findViewById(R.id.description_LeaveRequest);


        send = (Button) findViewById(R.id.send_feedback);
        one = (TextInputLayout) findViewById(R.id.startDate_inputlayout_leave);
        two = (TextInputLayout) findViewById(R.id.endDate_inputlayout_leave);
        three = (TextInputLayout) findViewById(R.id.desc_inputlayout_leave);

        startDate.setOnClickListener(this);
        endDate.setOnClickListener(this);

        startDate.setInputType(InputType.TYPE_NULL);
        startDate.requestFocus();

        endDate.setInputType(InputType.TYPE_NULL);

        setDateTimeField();

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (connectionManager.isNetworkConnected()) {
                    try {
                        prepareToSendFeedback();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    Snackbar snackbar = Snackbar.make(startDate, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                prepareToSendFeedback();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    snackbar.show();
                }
            }
        });

    }

    private void prepareToSendFeedback() throws ParseException {
        final String from_date = startDate.getText().toString();
        final String to_date = endDate.getText().toString();
        final String description_leave = description.getText().toString();

        if (from_date.isEmpty()) {
            startDate.setError("Empty field");
            return;
        } else if (to_date.isEmpty()) {
            endDate.setError("Empty Field");
            return;
        } else if (description_leave.isEmpty()) {
            description.setError("Empty Field");
            return;
        } else if (dateFormatter.parse(to_date).before(dateFormatter.parse(from_date))) {
            startDate.setError("Start date should be an earlier date than end one.");
            return;
        } else {
            Snackbar snackbar1 = Snackbar.make(startDate, "Submitting your leave request please wait .... ", Snackbar.LENGTH_LONG).setAction("Retry", null);
            snackbar1.getView().setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark));
            Snackbar.SnackbarLayout snack_view = (Snackbar.SnackbarLayout) snackbar1.getView();
            snack_view.addView(new ProgressBar(getApplicationContext()));
            snackbar1.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_1 + CLIENT_APITOKEN, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Log.d("TAG", response);
                        JSONObject jsonObject = new JSONObject(response);
                        if (!jsonObject.getBoolean("error")) { //success if error is false
                            snackbar = Snackbar.make(startDate, "Your leave request has been submitted.", Snackbar.LENGTH_SHORT).setAction("", null);
                            snackbar.show();
                            startDate.setText("");
                            endDate.setText("");
                            description.setText("");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof NetworkError) {
                        snackbar = Snackbar.make(startDate, "Network Error", Snackbar.LENGTH_SHORT).setAction("Retry", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    prepareToSendFeedback();
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        snackbar.show();

                    } else if (error instanceof TimeoutError) {
                        snackbar = Snackbar.make(startDate, "Connection Timed Out", Snackbar.LENGTH_SHORT).setAction("Retry", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    prepareToSendFeedback();
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        snackbar.show();
                    } else if (error instanceof NoConnectionError) {
                        snackbar = Snackbar.make(startDate, "No Internet Connection", Snackbar.LENGTH_SHORT).setAction("Retry", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    prepareToSendFeedback();
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        snackbar.show();
                    } else if (error instanceof ServerError) {
                        Toast.makeText(getApplicationContext(), "Oops ! We are experiencing some problem.", Toast.LENGTH_SHORT).show();
                    }


                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("start_at", to_date);
                    params.put("end_at", from_date);
                    params.put("description", description_leave);
                    return params;
                }
            };
            VolleyManager.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

        }
    }


    public String getSavedToken() {
        SharedPreferences sp = getSharedPreferences("UserCid", Context.MODE_PRIVATE);
        return sp.getString("cid", String.valueOf(sp));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (view == startDate) {
            fromDatePickerDialog.show();
        } else if (view == endDate) {
            toDatePickerDialog.show();
        }
    }

    private void setDateTimeField() {


        Calendar newCalendar = Calendar.getInstance();
        startDate.setText(dateFormatter.format(newCalendar.getTime()));
        endDate.setText(dateFormatter.format(newCalendar.getTime()));

        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                startDate.setText(dateFormatter.format(newDate.getTime()));
                endDate.requestFocus();

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                endDate.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }


}






