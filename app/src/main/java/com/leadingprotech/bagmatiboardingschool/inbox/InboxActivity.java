package com.leadingprotech.bagmatiboardingschool.inbox;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.navigation.Activity_Accounts;


public class InboxActivity extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    PersonalMessageFragment personalMessageFragment;
    GroupMessageFragment groupMessageFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Inbox");
        }

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        personalMessageFragment = new PersonalMessageFragment();
        groupMessageFragment = new GroupMessageFragment();

        viewPagerAdapter.addFragments(groupMessageFragment, "Group Message");
        viewPagerAdapter.addFragments(personalMessageFragment, "Personal Message");


        int position = 0;
        if (getIntent() != null) {

            position = getIntent().getIntExtra("position", position);
        } else {
            position = 0;
        }

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(position);

    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(InboxActivity.this, Activity_Accounts.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();

        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        } else if (id == R.id.refresh_view) {

            personalMessageFragment.downloadData();
            groupMessageFragment.downloadData();
            //Toast.makeText(this, "Refreshing...", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh_menu, menu);
        return true;
    }
}
