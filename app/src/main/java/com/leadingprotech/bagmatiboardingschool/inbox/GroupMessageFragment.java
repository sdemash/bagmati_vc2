package com.leadingprotech.bagmatiboardingschool.inbox;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;


public class GroupMessageFragment extends Fragment {


    public static final String SP_NAME_API_TOKEN = "apiToken";
    RecyclerView recyclerView;
    List<ChatModel> chatModelList = new ArrayList<>();
    List<ChatModel> chatModelListDownload = new ArrayList<>();
    ChatAdapter chatAdapter;
    nBulletinDatabase db;
    ConnectionManager connectionManager;

    SwipeRefreshLayout swipe;

    SharedPreferences sharedPreferences;
    String CLIENT_APITOKEN;


    public GroupMessageFragment() {
        // Required empty public constructor
    }

    public void downloadData() {
        chatModelListDownload.clear();
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String URL_FEED = BASE_URL + "student/messages/group?api_token=" + CLIENT_APITOKEN;
        JsonObjectRequest obreq = new JsonObjectRequest(Request.Method.GET, URL_FEED, null, new
                Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("respon grup msg", response.toString());
                            JSONArray obj = response.getJSONArray("message");

                            if (obj.length() > 0) {
                                db.deleteInboxMsgs(CLIENT_APITOKEN, 0);

                            }
                            for (int i = 0; i < obj.length(); i++) {
                                JSONObject jsonObject = obj.getJSONObject(i);
                                String from = jsonObject.getString("from");
                                String name = jsonObject.getString("name");
                                String image = jsonObject.getString("image");
                                String message = jsonObject.getString("message");
                                String created_at = jsonObject.getString("created_at");

                                ChatModel chatModel = new ChatModel(CLIENT_APITOKEN, 0, message, created_at, image, from, name, 0);
                                chatModelListDownload.add(chatModel);

                                db.addInboxMessage(chatModel);
                            }
                            swipe.setRefreshing(false);
                            if (connectionManager.isNetworkConnected()) {
                                onlinePopulateRecyclerView(chatModelListDownload);

                            } else {
                                populateRecyclerView();

                            }

                        } catch (JSONException e) {
                            swipe.setRefreshing(false);

                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                String errorText = "Unknown Error.";
                if (error instanceof TimeoutError) {
                    errorText = "Connection timed out.";
                }
                if (error instanceof NoConnectionError) {
                    errorText = "No Internet Connection.";
                }
                if (error instanceof AuthFailureError) {
                    errorText = "Wrong Email or Password.";
                }
                if (error instanceof com.android.volley.NetworkError) {
                    errorText = "Unable to connect to the Internet.";
                }
                if (error instanceof com.android.volley.ServerError) {

                    errorText = "Unable to connect to the Server.";
                }
                if (error instanceof com.android.volley.ParseError) {
                    errorText = "Unable to fetch data from the server.";
                }


                Snackbar snackbar = Snackbar.make(swipe, errorText, Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        downloadData();
                    }
                });
                snackbar.show();
            }
        });
        queue.add(obreq);

        obreq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.group_message_fragment, container, false);
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe_inbox_message);
        connectionManager = new ConnectionManager(getContext());

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView_chat);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(true);
                if (connectionManager.isNetworkConnected()) {
                    downloadData();
                } else {
                    swipe.setRefreshing(false);
                    Snackbar snackbar = Snackbar.make(swipe, "No Internet Connection", Snackbar.LENGTH_SHORT).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            downloadData();
                        }
                    });
                    snackbar.show();
                }
            }
        });

        downloadData();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = getActivity().getSharedPreferences(SP_NAME_API_TOKEN, 0);
        CLIENT_APITOKEN = sharedPreferences.getString("apiToken", "");
        db = nBulletinDatabase.getInstance(getContext());
    }

    public void populateRecyclerView() {

        chatModelList = db.getInboxMessages(0, CLIENT_APITOKEN);
        chatAdapter = new ChatAdapter(chatModelList, getContext());
        recyclerView.setAdapter(chatAdapter);
        if (null != chatModelList) {
            int last_index = chatModelList.size();

            recyclerView.scrollToPosition(last_index - 1);
        }

    }

    public void onlinePopulateRecyclerView(List<ChatModel> chatList) {
        chatAdapter = new ChatAdapter(chatList, getContext());
        recyclerView.setAdapter(chatAdapter);
        if (null != chatList) {
            int last_index = chatList.size();

            recyclerView.scrollToPosition(last_index - 1);
        }

    }
}
