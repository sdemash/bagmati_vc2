package com.leadingprotech.bagmatiboardingschool.inbox;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.helper.CircleTransformGlide;

import java.util.List;


public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int RECEIVED_TYPE = 0;
    public static final int SENT_TYPE = 1;

    private List<ChatModel> chatModelList;
    private Context context;

    public ChatAdapter(List<ChatModel> chatModelList, Context context) {
        this.context = context;
        this.chatModelList = chatModelList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case RECEIVED_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_bubble_received, parent, false);
                return new ReceivedViewHolder(view);
            case SENT_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_bubble_sent, parent, false);
                return new SentViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ChatModel object = chatModelList.get(position);
        if (object != null) {
            switch (object.getType()) {
                case RECEIVED_TYPE:
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        ((ReceivedViewHolder) holder).tv_msg.setText(Html.fromHtml(object.getMsg(), Html.FROM_HTML_MODE_COMPACT));

                    } else {
                        ((ReceivedViewHolder) holder).tv_msg.setText(Html.fromHtml(object.getMsg()));
                    }

                    ((ReceivedViewHolder) holder).tv_time.setText(object.getDatetime());

                    Glide.with(context)
//                            .load(object.getImage())
                            .load(R.drawable.institution_logo)
                            .centerCrop().transform(new CircleTransformGlide(context))
                            .into(((ReceivedViewHolder) holder).imgView_dp);
                    break;
                case SENT_TYPE:

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        ((SentViewHolder) holder).tv_msg.setText(Html.fromHtml(object.getMsg(), Html.FROM_HTML_MODE_COMPACT));

                    } else {
                        ((SentViewHolder) holder).tv_msg.setText(Html.fromHtml(object.getMsg()));
                    }

                    ((SentViewHolder) holder).tv_time.setText(object.getDatetime());

                    Glide.with(context)
                            .load(R.drawable.institution_logo)
                            .centerCrop().transform(new CircleTransformGlide(context))
                            .into(((SentViewHolder) holder).imgView_dp);
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        if (chatModelList == null)
            return 0;
        return chatModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (chatModelList != null) {
            ChatModel chatModel = chatModelList.get(position);
            if (chatModel != null) {
                return chatModel.getType();
            }
        }
        return 0;
    }

    public static class ReceivedViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_time, tv_msg;
        private ImageView imgView_dp;

        public ReceivedViewHolder(View itemView) {
            super(itemView);
            tv_msg = (TextView) itemView.findViewById(R.id.tv_received_text);
            tv_time = (TextView) itemView.findViewById(R.id.tv_received_time);
            imgView_dp = (ImageView) itemView.findViewById(R.id.imageView_receivedProImage);
        }
    }

    public static class SentViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_time, tv_msg;
        private ImageView imgView_dp;

        public SentViewHolder(View itemView) {
            super(itemView);
            tv_msg = (TextView) itemView.findViewById(R.id.tv_sent_text);
            tv_time = (TextView) itemView.findViewById(R.id.tv_sent_time);
            imgView_dp = (ImageView) itemView.findViewById(R.id.imageView_sentProImage);
        }
    }
}