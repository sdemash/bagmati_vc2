package com.leadingprotech.bagmatiboardingschool.inbox;


public class ChatModel {

    int type; // type : sent or received
    String msg;
    String datetime;
    private int TYPE; // group or personal message ; 1 for personal
    private String image;
    private String from;
    private String sender_name;
    private String api_token;


    public ChatModel(String api_token, int type, String msg, String datetime, String image, String from, String sender_name, int TYPE) {
        this.setApi_token(api_token);
        this.type = type;
        this.msg = msg;
        this.datetime = datetime;
        this.setImage(image);
        this.setFrom(from);
        this.setSender_name(sender_name);
        this.TYPE = TYPE;


    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }


    public String getApi_token() {
        return api_token;
    }

    public void setApi_token(String api_token) {
        this.api_token = api_token;
    }

    public int getTYPE() {
        return TYPE;
    }

    public void setTYPE(int TYPE) {
        this.TYPE = TYPE;
    }
}
