package com.leadingprotech.bagmatiboardingschool.account_chooser;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.leadingprotech.bagmatiboardingschool.LinkerActivity;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_models.LoginModel;
import com.leadingprotech.bagmatiboardingschool.navigation.Activity_Accounts;
import com.mikepenz.materialdrawer.view.BezelImageView;

import java.util.List;

/**
 * Created by karsk on 16/02/2017.
 */

public class AccountChooserAdapter extends RecyclerView.Adapter<AccountChooserAdapter.MyViewHolder> {

    public static final String SP_NAME_API_TOKEN = "apiToken";
    List<LoginModel> loginModelList;
    SharedPreferences sharedPreferences;
    private Context context;

    public AccountChooserAdapter(List<LoginModel> loginModelList, Context context) {
        this.context = context;
        this.loginModelList = loginModelList;
        sharedPreferences = context.getSharedPreferences(SP_NAME_API_TOKEN, 0);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loggedin, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final LoginModel loginModel = loginModelList.get(position);

        holder.title.setText(loginModel.getFull_name());
        holder.className.setText(loginModel.getClass_name());
        Glide.with(context)
                .load(loginModel.getImage())
                .thumbnail(0.5f)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(holder.imageView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor spEditor_LoggedIn = sharedPreferences.edit();
                spEditor_LoggedIn.putString("apiToken", loginModel.getApi_token());
                spEditor_LoggedIn.apply();

                Intent intent = new Intent(view.getContext(), Activity_Accounts.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                view.getContext().startActivity(intent);

                ((LinkerActivity) context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return loginModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title, className;
        BezelImageView imageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.cardview_list_title);
            className = (TextView) itemView.findViewById(R.id.tv_dialog_class);
            imageView = (BezelImageView) itemView.findViewById(R.id.cardview_image);

        }
    }
}
