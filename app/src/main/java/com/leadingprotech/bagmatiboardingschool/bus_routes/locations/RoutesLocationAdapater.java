package com.leadingprotech.bagmatiboardingschool.bus_routes.locations;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;

import java.util.List;

/**
 * Created by karsk on 13/02/2017.
 */

public class RoutesLocationAdapater extends RecyclerView.Adapter<RoutesLocationAdapater.MyViewHolder> {

    nBulletinDatabase database;
    private List<RoutesLocationModel> routesLocationModelList;
    private Context context;


    public RoutesLocationAdapater(List<RoutesLocationModel> routesLocationModelList, Context context) {
        this.context = context;
        this.routesLocationModelList = routesLocationModelList;
        database = new nBulletinDatabase(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_routes_location, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final RoutesLocationModel miscListModel = routesLocationModelList.get(position);

        holder.title.setText(miscListModel.getLocation());
        holder.position.setText(String.valueOf(holder.getAdapterPosition() + 1));

    }

    @Override
    public int getItemCount() {
        return routesLocationModelList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title, position;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tv_route_location);
            position = (TextView) itemView.findViewById(R.id.position);

        }

    }
}
