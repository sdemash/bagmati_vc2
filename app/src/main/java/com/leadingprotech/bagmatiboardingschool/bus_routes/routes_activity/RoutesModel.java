package com.leadingprotech.bagmatiboardingschool.bus_routes.routes_activity;

/**
 * Created by karsk on 13/02/2017.
 */

public class RoutesModel {

    int routeId;
    String title;

    public RoutesModel(int routeId, String title) {
        this.routeId = routeId;
        this.title = title;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
