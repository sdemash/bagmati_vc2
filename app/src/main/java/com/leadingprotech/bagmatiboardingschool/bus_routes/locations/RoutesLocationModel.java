package com.leadingprotech.bagmatiboardingschool.bus_routes.locations;

/**
 * Created by karsk on 13/02/2017.
 */

public class RoutesLocationModel {

    int routeId;
    String location;
    double latitude, longitude;

    public RoutesLocationModel(int routeId, String location, double latitude, double longitude) {
        this.routeId = routeId;
        this.location = location;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
