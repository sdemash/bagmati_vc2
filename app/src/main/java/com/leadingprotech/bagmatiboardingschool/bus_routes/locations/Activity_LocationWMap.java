package com.leadingprotech.bagmatiboardingschool.bus_routes.locations;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.ui.IconGenerator;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.bus_routes.DirectionsJSONParser;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.lat;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.lng;

public class Activity_LocationWMap extends AppCompatActivity implements OnMapReadyCallback {

    RecyclerView recyclerView;

    List<RoutesLocationModel> routesLocationModelList = new ArrayList<>();
    List<RoutesLocationModel> routesLocationMapModelList = new ArrayList<>();
    RoutesLocationAdapater routesLocationAdapater;
    nBulletinDatabase database;

    Polyline polyline;
    LatLng origin;
    String title;
    int route_id;
    int maxPoints, currPoint = 0;
    private SupportMapFragment googleMapFrag;
    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locationwmap);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        database = nBulletinDatabase.getInstance(Activity_LocationWMap.this);

        Intent intentExtra = getIntent(); // <-- returns null
        if (intentExtra != null) {
            Bundle extras = intentExtra.getExtras();
            route_id = extras.getInt("route_id");
            title = extras.getString("title");
            if(getSupportActionBar()!=null) {
                getSupportActionBar().setTitle(title);
            }

        } else {
            finish();
        }

        populateLocationData();

        if (googleMapFrag == null) {
            FragmentManager fmanager = getSupportFragmentManager();
            Fragment fragment = fmanager.findFragmentById(R.id.fragment_map);
            googleMapFrag = (SupportMapFragment) fragment;
            googleMapFrag.getMapAsync(this);

            // check if map is created successfully or not
            if (googleMapFrag == null) {
                Toast.makeText(this,
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_routes_location);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(Activity_LocationWMap.this);
        recyclerView.setLayoutManager(layoutManager);

        populateRecyclerView();
    }

    public void populateRecyclerView() {
        routesLocationModelList.clear();

        Cursor cursor = database.getAllRouteLocationData(route_id);
        TextView textView_noTrips = (TextView) findViewById(R.id.tv_routes_null);
        if (cursor.getCount() == 0) {
            textView_noTrips.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            return;
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            textView_noTrips.setVisibility(View.GONE);
        }
        try {
            cursor.moveToFirst();
            do {
                int route_id = cursor.getInt(1);
                String title = cursor.getString(2);
                double latitude = cursor.getDouble(3);
                double longitude = cursor.getDouble(4);

                RoutesLocationModel routesLocationModel = new RoutesLocationModel(route_id, title, latitude, longitude);

                routesLocationModelList.add(routesLocationModel);
            }
            while (cursor.moveToNext());
        } finally {
            cursor.close();
        }
        runOnUiThread(new Runnable() {
            public void run() {
                routesLocationAdapater = new RoutesLocationAdapater(routesLocationModelList, Activity_LocationWMap.this);
                recyclerView.setAdapter(routesLocationAdapater);
            }
        });
    }

    public void populateLocationData() {
        routesLocationMapModelList.clear();
        Cursor cursor1 = database.getAllRouteLocationData(0);
        if (cursor1.getCount() == 0) {
            origin = new LatLng(lat, lng);
        } else {
            try {
                cursor1.moveToFirst();
                origin = new LatLng(cursor1.getDouble(3), cursor1.getDouble(4));
                RoutesLocationModel routesLocationModel = new RoutesLocationModel(route_id, cursor1.getString(2), cursor1.getDouble(3), cursor1.getDouble(4));
                routesLocationMapModelList.add(routesLocationModel);
            } finally {
                cursor1.close();
            }
        }

        Cursor cursor = database.getAllRouteLocationData(route_id);

        if (cursor.getCount() == 0) {
            return;
        } else {
        }
        try {
            cursor.moveToLast();
            do {
                int route_id = cursor.getInt(1);
                String title = cursor.getString(2);
                double latitude = cursor.getDouble(3);
                double longitude = cursor.getDouble(4);
                RoutesLocationModel routesLocationModel = new RoutesLocationModel(route_id, title, latitude, longitude);
                routesLocationMapModelList.add(routesLocationModel);
            }
            while (cursor.moveToPrevious());
        } finally {
            cursor.close();
        }

        maxPoints = routesLocationMapModelList.size() - 1;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

       /* Marker marker = googleMap.addMarker(new MarkerOptions().position(origin)
                .title(routesLocationMapModelList.get(currPoint).getLocation())
                .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));*/
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        // googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        // googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        // googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        // googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);

        // Showing / hiding your current location
        if (ActivityCompat.checkSelfPermission(Activity_LocationWMap.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Activity_LocationWMap.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details
        }
        // Enable / Disable zooming controls
        googleMap.getUiSettings().setZoomControlsEnabled(false);

        // Enable / Disable my location button
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        // Enable / Disable Compass icon
        googleMap.getUiSettings().setCompassEnabled(true);

        // Enable / Disable Rotate gesture
        googleMap.getUiSettings().setRotateGesturesEnabled(true);

        // Enable / Disable zooming functionality
        googleMap.getUiSettings().setZoomGesturesEnabled(true);

        plotLocations();

    }

    public void plotLocations() {
        if (currPoint < maxPoints) {
            currPoint++;
            LatLng latLng = new LatLng(routesLocationMapModelList.get(currPoint).getLatitude(),
                    routesLocationMapModelList.get(currPoint).getLongitude());
            LatLng dest = latLng;
            String url = getDirectionsUrl(origin, dest);

            DownloadTask downloadTask = new DownloadTask();

            downloadTask.execute(url);

            IconGenerator iconFactory = new IconGenerator(this);
            iconFactory.setBackground(ContextCompat.getDrawable(this, R.drawable.circle_shape));
            iconFactory.setTextAppearance(R.style.MapIconFont);


            Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng)
                    .title(routesLocationMapModelList.get(currPoint).getLocation())
                    .icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(String.valueOf(maxPoints-currPoint+1)))) // map position to match the location list position
                    .anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV()));
                    /*.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));*/

            origin = dest;
        } else {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (int i = 0; i <= maxPoints; i++) {
                builder.include(new LatLng(routesLocationMapModelList.get(i).getLatitude(),
                        routesLocationMapModelList.get(i).getLongitude()));
                Log.d("plotLocations: ", "" + i);
            }
            final LatLngBounds bounds = builder.build();
//            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, getResources().getDisplayMetrics())));
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                    new LatLng(routesLocationMapModelList.get(0).getLatitude(),
                            routesLocationMapModelList.get(0).getLongitude()), 12.5f);
            googleMap.animateCamera(location);
        }
        Log.d("maxcurr", maxPoints + " " + currPoint);
    }



    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuilder sb = new StringBuilder();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Excepturl", e.toString());
        } finally {
            if(iStream!=null) {
                iStream.close();
                urlConnection.disconnect();
            }
        }
        return data;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            super.onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("parsed", jObject.toString());
                DirectionsJSONParser parser = new DirectionsJSONParser();

                JSONArray routesArray = jObject.getJSONArray("routes");
                // Grab the first route
                JSONObject route = routesArray.getJSONObject(0);

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            if (result != null) {

                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }
                    Log.d("pointss", points + "");
                    Log.d("loog", result + "");

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(10);
                    lineOptions.color(ContextCompat.getColor(Activity_LocationWMap.this,R.color.colorAccent));
                }

                // Drawing polyline in the Google Map for the i-th route
                polyline = googleMap.addPolyline(lineOptions);

//            LatLngBounds.Builder builder = new LatLngBounds.Builder();
//            builder.include(new LatLng(picklat, picklong));
//            builder.include(new LatLng(destlat, destlong));
//            final LatLngBounds bounds = builder.build();
//            googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
//                @Override
//                public void onMapLoaded() {
//                    googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics())));
//                }
//            });
//            googleMap.setPadding(0, 0, 0, 0);
            }
            plotLocations();
        }
    }

}
