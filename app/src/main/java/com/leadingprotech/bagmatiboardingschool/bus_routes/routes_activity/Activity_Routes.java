package com.leadingprotech.bagmatiboardingschool.bus_routes.routes_activity;

import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;
import com.leadingprotech.bagmatiboardingschool.helper.VolleyManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.API_TOKEN;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;

public class Activity_Routes extends AppCompatActivity {

    RecyclerView recyclerView;
    List<RoutesModel> routesModelList = new ArrayList<>();
    RoutesAdapater routesAdapter;
    nBulletinDatabase database;
    ConnectionManager connectionManager;
    RelativeLayout root ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routes);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Bus Routes");
        }

        database = new nBulletinDatabase(this);
        connectionManager = new ConnectionManager(getApplicationContext());

        root = (RelativeLayout) findViewById(R.id.rootLayout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_routes);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        Cursor cursor = database.getAllRouteData();
        if (null != cursor && cursor.getCount() > 0) {
            cursor.close();

            populateRecyclerView();
        } else {

            prepareRoutesData();
        }

    }

    public void prepareRoutesData() {

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar3);
        progressBar.setVisibility(View.VISIBLE);

        String URL = BASE_URL + "bus-route?api_key=" + API_TOKEN;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println("BUSROUTES " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("bus-route");

                    if (jsonArray.length() > 0) {
                        database.deleteAllRouteData();
                        database.deleteAllRouteLocationData();
                    }

                    for (int i = 0; i < jsonArray.length(); ++i) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        String name = object.getString("name");
                        JSONArray addresses = object.getJSONArray("address");
                        long id = database.insertRouteData(name, i);

                        for (int j = 0; j < addresses.length(); ++j) {

                            JSONObject address = addresses.getJSONObject(j);
                            double latitude = address.getDouble("latitude");
                            double longitude = address.getDouble("longitude");
                            String location = address.getString("address");
                            database.insertRouteLocationData(i, location, latitude, longitude);

                        }

                    }
                    progressBar.setVisibility(View.GONE);

                    populateRecyclerView();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                populateRecyclerView();

                if (error instanceof NoConnectionError) {

                    Snackbar snackbar = Snackbar.make(root, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    });
                    snackbar.show();
                } else if (error instanceof ServerError) {

                    Toast.makeText(Activity_Routes.this, "Oops ! We are experiencing some problem.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        VolleyManager.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

    }

    public void populateRecyclerView() {
        routesModelList.clear();

        Cursor cursor = database.getAllRouteData();
        TextView textView_noTrips = (TextView) findViewById(R.id.tv_routes_null);
        if (cursor.getCount() == 0) {
            textView_noTrips.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            return;
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            textView_noTrips.setVisibility(View.GONE);
        }
        try {
            cursor.moveToFirst();
            do {
                int route_id = cursor.getInt(0);
                String title = cursor.getString(1);

                RoutesModel miscListModel = new RoutesModel(route_id, title);

                routesModelList.add(miscListModel);
            }
            while (cursor.moveToNext());
        } finally {
            cursor.close();
        }
        runOnUiThread(new Runnable() {
            public void run() {
                routesAdapter = new RoutesAdapater(routesModelList, Activity_Routes.this);
                recyclerView.setAdapter(routesAdapter);
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.refresh_view) {

            if (connectionManager.isNetworkConnected()) {
                prepareRoutesData();
            }
        } else if (id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh_menu, menu);
        return true;
    }
}
