package com.leadingprotech.bagmatiboardingschool.bus_routes.routes_activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.bus_routes.locations.Activity_LocationWMap;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;

import java.util.List;

/**
 * Created by karsk on 13/02/2017.
 */

public class RoutesAdapater extends RecyclerView.Adapter<RoutesAdapater.MyViewHolder> {

    nBulletinDatabase database;
    private List<RoutesModel> routesModelList;
    private Context context;


    public RoutesAdapater(List<RoutesModel> routesModelList, Context context) {
        this.context = context;
        this.routesModelList = routesModelList;
        database = new nBulletinDatabase(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_routes, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final RoutesModel miscListModel = routesModelList.get(position);

        holder.title.setText(miscListModel.getTitle());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Activity_LocationWMap.class);
                intent.putExtra("route_id", miscListModel.getRouteId());
                intent.putExtra("title", miscListModel.getTitle());

                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return routesModelList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tv_route_title);
            cardView = (CardView) itemView.findViewById(R.id.cardView_routes);
        }

    }
}
