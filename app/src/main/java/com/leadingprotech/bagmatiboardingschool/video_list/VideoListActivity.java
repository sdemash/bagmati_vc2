package com.leadingprotech.bagmatiboardingschool.video_list;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.database_models.LoginModel;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;
import com.leadingprotech.bagmatiboardingschool.helper.VolleyManager;
import com.leadingprotech.bagmatiboardingschool.navigation.Activity_Accounts;
import com.leadingprotech.bagmatiboardingschool.navigation.NavActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.API_TOKEN;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;

public class VideoListActivity extends AppCompatActivity {
    public ArrayList<VideoListModel> videoListModels;
    ConnectionManager connectionManager;
    LinearLayout no_video;
    ProgressBar progressBar;
    String URL_1 = BASE_URL + "video?api_key=" + API_TOKEN;
    RecyclerView recyclerView;
    VideoListAdapter videoListAdapter;
    TextView dataNotAvailable;
    Snackbar snackbar;
    SwipeRefreshLayout swipe;
    nBulletinDatabase db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Video Gallery");
        }
        setContentView(R.layout.activity_video_list);

        db = nBulletinDatabase.getInstance(getApplicationContext());

        connectionManager = new ConnectionManager(getApplicationContext());

        no_video = (LinearLayout) findViewById(R.id.layout_no_video);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_to_refresh);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_videolist);
        progressBar = (ProgressBar) findViewById(R.id.progress_video);
        dataNotAvailable = (TextView) findViewById(R.id.noDataAvailable);
        videoListModels = new ArrayList<>();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if (connectionManager.isNetworkConnected()) {
            offlineData();
            prepareVideoGallery();
        } else {
            offlineData();
        }

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(true);
                if (connectionManager.isNetworkConnected()) {
                    prepareVideoGallery();
                } else {
                    offlineData();
                    snackbar = Snackbar.make(swipe, "No internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareVideoGallery();
                        }
                    });
                    snackbar.show();
                }
            }
        });

    }

    public void offlineData() {
        videoListModels = db.getVideosList();

        if (videoListModels.size() == 0) {
            swipe.setRefreshing(false);
            progressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            dataNotAvailable.setVisibility(View.VISIBLE);

        } else {
            progressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            dataNotAvailable.setVisibility(View.GONE);

            swipe.setRefreshing(false);
            videoListAdapter = new VideoListAdapter(videoListModels, VideoListActivity.this);
            recyclerView.setAdapter(videoListAdapter);

        }


    }


    private void prepareVideoGallery() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            videoListModels.clear();

                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("video");
                            if (jsonArray.length() > 0) {
                                db.deleteAllVideo();
                            }

                            for (int i = 0; i < jsonArray.length(); ++i) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                int id = object.getInt("id");
                                String title = object.getString("title");
                                String description = object.getString("description");
                                String url = object.getString("url");
                                String image = object.getString("image");

                                String video_id = object.getString("video_id");
                                String date_time = object.getString("updated_at");

                                db.addVideos(new VideoListModel(id, title, description, url, image, video_id, date_time));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        offlineData();

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                offlineData();


                if (error instanceof NoConnectionError) {
                    snackbar = Snackbar.make(swipe, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareVideoGallery();
                        }
                    });
                    snackbar.show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(VideoListActivity.this, "Oops ! We are experiencing some problem.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyManager.getInstance(VideoListActivity.this).addToRequestQueue(stringRequest);


    }

    public String getSavedToken() {
        SharedPreferences sp = getApplicationContext().getSharedPreferences("UserCid", MODE_PRIVATE);
        return sp.getString("cid", null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        List<LoginModel> loginModel = db.getLoggedInAccounts();
        if (loginModel.size() == 0) {
            Intent intent = new Intent(VideoListActivity.this, NavActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else {
            Intent intent = new Intent(VideoListActivity.this, Activity_Accounts.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            List<LoginModel> loginModel = db.getLoggedInAccounts();
            if (loginModel.size() == 0) {
                Intent intent = new Intent(VideoListActivity.this, NavActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                Intent intent = new Intent(VideoListActivity.this, Activity_Accounts.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
            finish();
        } else if (id == R.id.refresh_view) {
            if (connectionManager.isNetworkConnected()) {
                videoListModels.clear();
                prepareVideoGallery();
            } else {
                no_video.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);

                Snackbar snackbar = Snackbar.make(swipe, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        prepareVideoGallery();
                    }
                });
                snackbar.show();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
