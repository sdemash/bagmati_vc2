package com.leadingprotech.bagmatiboardingschool.video_list;

public class VideoListModel {


    private int id;
    private String title;
    private String description;
    private String url;
    private String image;
    private String video_id;
    private String updated_at;


    public VideoListModel(int id, String title, String description, String url, String image, String video_id, String updated_at) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.url = url;
        this.image = image;
        this.video_id = video_id;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
