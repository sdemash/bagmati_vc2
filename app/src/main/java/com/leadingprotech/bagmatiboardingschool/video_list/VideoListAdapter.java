package com.leadingprotech.bagmatiboardingschool.video_list;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.leadingprotech.bagmatiboardingschool.R;

import java.util.List;

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.MyViewHolder> {
    private List<VideoListModel> videoListModels;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_album;
        TextView title_album;

        public MyViewHolder(View itemView) {
            super(itemView);
            img_album = (ImageView) itemView.findViewById(R.id.imageView_thumb);
            title_album = (TextView) itemView.findViewById(R.id.textView_title);
        }
    }

    public VideoListAdapter(List<VideoListModel> videoListModels, Context context) {
        this.videoListModels = videoListModels;
        this.context = context;
    }

    @Override
    public VideoListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final VideoListAdapter.MyViewHolder holder, int position) {
        final VideoListModel videoListModel = videoListModels.get(position);
        Glide.with(context)
                .load(videoListModel.getImage())
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .placeholder(R.drawable.no_thumb)
                .into(holder.img_album);
        holder.title_album.setText(videoListModel.getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(context, VideoPlayerActivity.class);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                in.putExtra("video_id", videoListModel.getVideo_id());
                in.putExtra("title", videoListModel.getTitle());
                in.putExtra("description", videoListModel.getDescription());
                context.startActivity(in);
            }
        });

    }

    @Override
    public int getItemCount() {
        return videoListModels.size();
    }


}
