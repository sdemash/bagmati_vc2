package com.leadingprotech.bagmatiboardingschool.notice;

public class NoticeModel {
    String id, title, message, date_time, status;

    public NoticeModel(String id, String title, String message, String date_time, String status){
        this.id = id;
        this.title = title;
        this.message = message;
        this.date_time = date_time;
        this.status= status;
    }

    public NoticeModel(){

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

}
