package com.leadingprotech.bagmatiboardingschool.notice;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.navigation.NavActivity;

public class ViewNoticeActivity extends AppCompatActivity {
    Spanned sp;
    nBulletinDatabase db;
    FloatingActionButton fab;
    float mRatio = 1.0f;
    float fontSize = 18;
    LinearLayout layout;
    WebView message;

    TextView title, day, date, din, initial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_notice);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle((getIntent().getStringExtra("topic") != null) ? getIntent().getStringExtra("topic") : "Notice");

        }

        db = nBulletinDatabase.getInstance(getApplicationContext());

        layout = (LinearLayout) findViewById(R.id.layer_zoom);
        title = (TextView) findViewById(R.id.view_notice_title);
        day = (TextView) findViewById(R.id.view_notice_weekday);
        date = (TextView) findViewById(R.id.view_notice_date);
        din = (TextView) findViewById(R.id.view_din_notice);
        message = (WebView) findViewById(R.id.view_notice_message);
        initial = (TextView) findViewById(R.id.img_view_notice);
        fab = (FloatingActionButton) findViewById(R.id.share_fab);

        if (getIntent().getExtras() != null) {
            title.setText(getIntent().getStringExtra("topic"));
            day.setText(getIntent().getStringExtra("din"));
            din.setText(getIntent().getStringExtra("date_number"));
            date.setText(getIntent().getStringExtra("date"));
            sp = Html.fromHtml(getIntent().getStringExtra("message"));
            message.loadData(getIntent().getStringExtra("message"), "text/html; charset=UTF-8", null);

            message.setBackgroundColor(Color.TRANSPARENT);
            db.updateNotice(getIntent().getStringExtra("id"), String.valueOf(1));
        }


        String first = title.getText().toString().substring(0, 1).toUpperCase();
        initial.setText(first);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(Intent.ACTION_SEND);
                in.setType("text/plain");
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                in.putExtra(Intent.EXTRA_SUBJECT, "Title: ");
                in.putExtra(Intent.EXTRA_TEXT, title.getText().toString() + "\n\n" + sp.toString()
                        + "\n\n" + "Source: " + getResources().getString(R.string.institution_name));
                startActivity(Intent.createChooser(in, "Share"));
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.view_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        if (id == R.id.home_view) {
            Intent in = new Intent(getApplicationContext(), NavActivity.class);
            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(in);
        }
        return super.onOptionsItemSelected(item);
    }

}
