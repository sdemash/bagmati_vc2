package com.leadingprotech.bagmatiboardingschool.notice;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.leadingprotech.bagmatiboardingschool.R;

import org.jsoup.Jsoup;


public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.MyViewholder> {
    private List<NoticeModel> noticeModelList;

    private Context context;

    public class MyViewholder extends RecyclerView.ViewHolder {
        ImageView notify;
        View margin;
        RelativeLayout row;
        String date_time, notice_id, din;
        public TextView date_number, date, title, description;

        public MyViewholder(View itemView) {
            super(itemView);
            row = (RelativeLayout) itemView.findViewById(R.id.notice_row);
            date_number = (TextView) itemView.findViewById(R.id.number_date);
            date = (TextView) itemView.findViewById(R.id.text_date);
            title = (TextView) itemView.findViewById(R.id.tv_title);
            description = (TextView) itemView.findViewById(R.id.tv_description);
            margin = (View) itemView.findViewById(R.id.notice_margin);
            notify = (ImageView) itemView.findViewById(R.id.circle_notify);
        }
    }

    public NoticeAdapter(List<NoticeModel> noticeModelList) {
        this.noticeModelList = noticeModelList;
        this.context = context;
    }

    @Override
    public MyViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notice, parent, false);

        return new MyViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewholder holder, int position) {
        final NoticeModel noticeModel = noticeModelList.get(position);

        if (position == noticeModelList.size() - 1) {
            holder.margin.setVisibility(View.GONE);
        }
        holder.title.setText(noticeModel.getTitle().substring(0, 1).toUpperCase() + noticeModel.getTitle().substring(1));
        holder.description.setText(Jsoup.parse(noticeModel.getMessage()).text());
        holder.notice_id = noticeModel.getId();
        holder.date_time = noticeModel.getDate_time();
        if (noticeModel.getStatus().equals("0")) {
            holder.notify.setImageResource(R.drawable.circle);
        } else {
            holder.notify.setImageResource(R.drawable.circle_grey);
        }

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(holder.date_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int days = cal.get(Calendar.DAY_OF_WEEK);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        String monthString = new DateFormatSymbols().getShortMonths()[month];
        final String tarik = day + "";
        final String baki = monthString + "," + " " + year;
        final String din = days + "";
        holder.date_number.setText(tarik);
        holder.date.setText(baki);

        SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
        String dayT = outFormat.format(date);
        holder.din = dayT;


        holder.row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(view.getContext(), ViewNoticeActivity.class);
                in.putExtra("id", noticeModel.getId());
                in.putExtra("topic", holder.title.getText().toString());
                in.putExtra("date", holder.date.getText().toString());
                in.putExtra("date_number", holder.date_number.getText().toString());
                in.putExtra("message", noticeModel.getMessage());
                in.putExtra("din", holder.din);
                view.getContext().startActivity(in);
            }
        });

    }

    public void setNewNoticeData(List<NoticeModel> noticeList){
        this.noticeModelList = noticeList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return noticeModelList.size();
    }
}
