package com.leadingprotech.bagmatiboardingschool.notice;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;
import com.leadingprotech.bagmatiboardingschool.helper.VolleyManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.API_TOKEN;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;

public class NoticeActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<NoticeModel>> {
    public static final String SHARED_STATUS = "status_loader";

    String URL_NOTICE = BASE_URL + "notice?api_key=" + API_TOKEN;
    LinearLayout no_data;
    Snackbar snackbar;
    ConnectionManager connectionManager;
    ProgressBar progressBar;
    SwipeRefreshLayout swipe;
    List<NoticeModel> noticeModelList;
    List<NoticeModel> noticeModelListOnline = new ArrayList<>();
    nBulletinDatabase db;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    String id, title, message, date_time;


    private RecyclerView recyclerView;
    private NoticeAdapter noticeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Notice");
        }

        connectionManager = new ConnectionManager(getApplicationContext());
        db = nBulletinDatabase.getInstance(getApplicationContext());
        sp = getSharedPreferences(SHARED_STATUS, Context.MODE_PRIVATE);
        editor = sp.edit();

        no_data = (LinearLayout) findViewById(R.id.layout_no_notice);
        progressBar = (ProgressBar) findViewById(R.id.progress_notice);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_to_refresh);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_notice);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        noticeModelList = db.getNotice();

        noticeAdapter = new NoticeAdapter(noticeModelList);

        if (getNoticeStatus()) {
            if (connectionManager.isNetworkConnected()) {
                offlineData();
                prepareNoticeData();
            } else {
                offlineData();
            }
        } else {
            prepareNoticeData();
        }


        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(true);
                if (connectionManager.isNetworkConnected()) {
                    prepareNoticeData();
                } else {
                    offlineData();
                    snackbar = Snackbar.make(swipe, "No internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareNoticeData();
                        }
                    });
                    snackbar.show();
                }
            }
        });


    }


    private void prepareNoticeData() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_NOTICE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            noticeModelListOnline.clear();

                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("notice");
                            for (int i = 0; i < jsonArray.length(); ++i) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                id = object.getString("id");
                                title = object.getString("title");
                                message = object.getString("message");
                                date_time = object.getString("updated_at");
                                boolean b = db.addNotice(new NoticeModel(id, title, message, date_time, String.valueOf(0)));
                                if (!b) {
                                    db.updateNotice(new NoticeModel(id, title, message, date_time, String.valueOf(0)), id);
                                }
                                NoticeModel noticeModel = new NoticeModel(id, title, message, date_time, String.valueOf(0));
                                noticeModelListOnline.add(noticeModel);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        editor.putBoolean("notice_status", true);
                        editor.apply();
                        editor.commit();

                        for (NoticeModel user1 : noticeModelList) {
                            boolean flag = false;
                            for (NoticeModel user2 : noticeModelListOnline) {
                                if (user1.getId().equals(user2.getId())) {

                                    flag = true;
                                    break;
                                }
                            }

                            if (!flag) {

                                db.deleteANotice(user1.getId());
                            }
                        }
                        offlineData();

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                offlineData();

                Log.d("deleeteCommonnnn", "notice error!");

                if (error instanceof NoConnectionError) {
                    snackbar = Snackbar.make(swipe, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareNoticeData();
                        }
                    });
                    snackbar.show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(NoticeActivity.this, "Oops ! We are experiencing some problem.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyManager.getInstance(NoticeActivity.this).addToRequestQueue(stringRequest);


    }

    public String getSavedToken() {
        SharedPreferences sp = getSharedPreferences("UserCid", Context.MODE_PRIVATE);
        return sp.getString("cid", null);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onResume() {
        super.onResume();
        prepareNoticeData();

    }

    public void offlineData() {
        noticeModelList = db.getNotice();

        if (noticeModelList.size() == 0) {
            swipe.setRefreshing(false);
            progressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
            noticeAdapter = new NoticeAdapter(noticeModelList);
            recyclerView.setAdapter(noticeAdapter);

        } else {
            progressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            no_data.setVisibility(View.GONE);

            swipe.setRefreshing(false);
            noticeAdapter = new NoticeAdapter(noticeModelList);
            recyclerView.setAdapter(noticeAdapter);

        }


    }

    public String getSavedFCM() {
        SharedPreferences sp = getSharedPreferences("fcm_token", Context.MODE_PRIVATE);
        return sp.getString("token", null);
    }


    @Override
    public Loader<List<NoticeModel>> onCreateLoader(int id, Bundle args) {
        return new NoticeLoader(NoticeActivity.this, URL_NOTICE);
    }

    @Override
    public void onLoadFinished(Loader<List<NoticeModel>> loader, List<NoticeModel> data) {
        if (data != null) {
            progressBar.setVisibility(View.GONE);
            // recyclerView.setVisibility(View.VISIBLE);
            swipe.setRefreshing(false);
            // noticeAdapter.setNewNoticeData(data);
            offlineData();

        }
    }

    @Override
    public void onLoaderReset(Loader<List<NoticeModel>> loader) {
        noticeAdapter.setNewNoticeData(new ArrayList<NoticeModel>());
    }


    public boolean getNoticeStatus() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_STATUS, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("notice_status", false);
    }

}
