package com.leadingprotech.bagmatiboardingschool.notice;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.leadingprotech.bagmatiboardingschool.BuildConfig;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;
import com.leadingprotech.bagmatiboardingschool.helper.VolleyManager;
import com.leadingprotech.bagmatiboardingschool.navigation.NavActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.API_TOKEN;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.UPDATE_URL;

public class NoticeFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<NoticeModel>> {
    public static final String SHARED_STATUS = "status_loader";
    String URL_NOTICE = BASE_URL + "notice?api_key=" + API_TOKEN;
    View layout;
    String URL_FCM_1 = BASE_URL + "";
    TextView no_data;
    Snackbar snackbar;
    ConnectionManager connectionManager;

    ProgressBar progressBar;
    SwipeRefreshLayout swipe;
    String id, title, message, date_time;
    List<NoticeModel> noticeModelList;
    List<NoticeModel> noticeModelListOnline = new ArrayList<>();
    nBulletinDatabase db;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    private RecyclerView recyclerView;
    private NoticeAdapter noticeAdapter;

    public NoticeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.fragment_notice, container, false);
        setHasOptionsMenu(true);

        no_data = (TextView)layout.findViewById(R.id.no_data);
        progressBar = (ProgressBar) layout.findViewById(R.id.progress_notice);
        swipe = (SwipeRefreshLayout) layout.findViewById(R.id.swipe_to_refresh);
        recyclerView = (RecyclerView) layout.findViewById(R.id.recycler_view_notice);

        return layout;

    }

    private void prepareFcmToken() {
        StringRequest sr = new StringRequest(Request.Method.POST, URL_FCM_1 + "token?api_key=" + API_TOKEN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("RESPONSE TOKEN", response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        VolleyManager.getInstance(getContext()).addToRequestQueue(sr);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        connectionManager = new ConnectionManager(getContext());
        db = nBulletinDatabase.getInstance(getContext());
        sp = getActivity().getSharedPreferences(SHARED_STATUS, Context.MODE_PRIVATE);
        editor = sp.edit();


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        noticeModelList = db.getNotice();

        noticeAdapter = new NoticeAdapter(noticeModelList);

        if (getNoticeStatus()) {
            if (connectionManager.isNetworkConnected()) {
                offlineData();
                prepareNoticeData();
            } else {
                offlineData();
            }
        } else {
            prepareNoticeData();
        }


        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(true);
                prepareFcmToken();
                if (connectionManager.isNetworkConnected()) {
                    prepareNoticeData();
                    ((NavActivity) getActivity()).prepareNoticeSlider();
                } else {
                    offlineData();
                    snackbar = Snackbar.make(swipe, "No internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareNoticeData();
                        }
                    });
                    snackbar.show();
                }
            }
        });


    }

    private void prepareNoticeData() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_NOTICE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            noticeModelListOnline.clear();

                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("notice");
                            for (int i = 0; i < jsonArray.length(); ++i) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                id = object.getString("id");
                                title = object.getString("title");
                                message = object.getString("message");
                                date_time = object.getString("updated_at");
                                boolean b = db.addNotice(new NoticeModel(id, title, message, date_time, String.valueOf(0)));
                                if (!b) {
                                    db.updateNotice(new NoticeModel(id, title, message, date_time, String.valueOf(0)), id);
                                }
                                NoticeModel noticeModel = new NoticeModel(id, title, message, date_time, String.valueOf(0));
                                noticeModelListOnline.add(noticeModel);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        editor.putBoolean("notice_status", true);
                        editor.apply();
                        editor.commit();

                        Log.d("deleeteCommonnnn", noticeModelList.size() + "");

                        for (NoticeModel user1 : noticeModelList) {
                            boolean flag = false;
                            for (NoticeModel user2 : noticeModelListOnline) {
                                if (user1.getId().equals(user2.getId())) {

                                    flag = true;
                                    break;
                                }
                            }

                            if (!flag) {

                                db.deleteANotice(user1.getId());
                            }
                        }

                        offlineData();

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                offlineData();

                if (error instanceof NoConnectionError) {
                    snackbar = Snackbar.make(swipe, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareNoticeData();
                        }
                    });
                    snackbar.show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getActivity(), "Oops ! We are experiencing some problem.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        VolleyManager.getInstance(getActivity()).addToRequestQueue(stringRequest);


    }

    public String getSavedToken() {
        SharedPreferences sp = getContext().getSharedPreferences("UserCid", Context.MODE_PRIVATE);
        return sp.getString("cid", null);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.nav, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.refresh_tab) {

            prepareFcmToken();
            if (connectionManager.isNetworkConnected()) {
                prepareNoticeData();
                ((NavActivity) getActivity()).prepareNoticeSlider();
            } else {
                offlineData();
                snackbar = Snackbar.make(swipe, "No internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        prepareNoticeData();
                    }
                });
                snackbar.show();
            }
        } else if (id == R.id.check_update) {
            checkUpdate();

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        prepareNoticeData();
    }

    public void offlineData() {
        noticeModelList = db.getNotice();

        if (noticeModelList.size() == 0) {
            swipe.setRefreshing(false);
            progressBar.setVisibility(View.GONE);
//            recyclerView.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
            noticeAdapter = new NoticeAdapter(noticeModelList);
            recyclerView.setAdapter(noticeAdapter);

        } else {
            progressBar.setVisibility(View.GONE);
//            recyclerView.setVisibility(View.VISIBLE);
            no_data.setVisibility(View.GONE);

            swipe.setRefreshing(false);
            noticeAdapter = new NoticeAdapter(noticeModelList);
            recyclerView.setAdapter(noticeAdapter);

        }


    }


    @Override
    public Loader<List<NoticeModel>> onCreateLoader(int id, Bundle args) {
        return new NoticeLoader(getContext(), URL_NOTICE);
    }

    @Override
    public void onLoadFinished(Loader<List<NoticeModel>> loader, List<NoticeModel> data) {
        if (data != null) {
            progressBar.setVisibility(View.GONE);
            // recyclerView.setVisibility(View.VISIBLE);
            swipe.setRefreshing(false);
            // noticeAdapter.setNewNoticeData(data);
            offlineData();

        }
    }

    @Override
    public void onLoaderReset(Loader<List<NoticeModel>> loader) {
        noticeAdapter.setNewNoticeData(new ArrayList<NoticeModel>());
    }


    public boolean getNoticeStatus() {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(SHARED_STATUS, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("notice_status", false);
    }

    ///check for update

    private void checkUpdate() {

        swipe.setRefreshing(true);
        AlertDialog.Builder builder;
        final AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.custom_update_dialog,
                (ViewGroup) getActivity().findViewById(R.id.rootLayout));

        final TextView title = (TextView) layout.findViewById(R.id.title);
        final WebView message = (WebView) layout.findViewById(R.id.message);
        Button update, later;

        update = (Button) layout.findViewById(R.id.button_update);
        later = (Button) layout.findViewById(R.id.button_later);

        builder = new AlertDialog.Builder(getActivity());
        builder.setView(layout);
        alertDialog = builder.create();

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String appPackageName = getActivity().getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

                alertDialog.dismiss();
            }
        });
        later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });


        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                UPDATE_URL + API_TOKEN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                swipe.setRefreshing(false);
                boolean _error = true;

                try {
                    Log.d("RESPONSE", response);
                    JSONObject jsonObject = new JSONObject(response);
                    _error = jsonObject.getBoolean("error");

                    if (!_error && jsonObject.getString("status_code").equals("200")) {
                        JSONObject admin = jsonObject.getJSONObject("school");
                        final String version_code = admin.getString("version_code");
                        String description = admin.getString("description");

                        title.setText(R.string.text_app_update);

                        message.loadData(description, "text/html; charset=UTF-8", null);


                        if (version_code != null && !version_code.isEmpty() && BuildConfig.VERSION_CODE < Integer.parseInt(version_code)) {

                            alertDialog.show();

                        } else {
                            Snackbar snackbar = Snackbar.make(swipe, "You have the latest version of " + getActivity().getResources().getString(R.string.app_name) + " app.", 3000);
                            snackbar.show();
                        }


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                swipe.setRefreshing(false);

                Snackbar snackbar = Snackbar.make(swipe, "Update information not available.", 3000);
                snackbar.show();
            }
        });

        // Adding request to request queue
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyManager.getInstance(getActivity()).addToRequestQueue(stringRequest);
    }
}
