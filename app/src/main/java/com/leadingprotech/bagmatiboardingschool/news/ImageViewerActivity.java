package com.leadingprotech.bagmatiboardingschool.news;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;
import com.github.chrisbanes.photoview.PhotoView;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ImageViewerActivity extends AppCompatActivity {
    FloatingActionButton back, download;
    PhotoView img;

    ProgressDialog pd;
    File directory;
    FileOutputStream out;
    InputStream in;
    HttpURLConnection con;
    int fileLength;
    int total = 0;
    private String timestamp = String.valueOf(System.currentTimeMillis());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_viewer);
        img = (PhotoView) findViewById(R.id.img_viewer);
        back = (FloatingActionButton) findViewById(R.id.img_back);

        download = (FloatingActionButton) findViewById(R.id.img_download);
        directory = new File(Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.app_name) + " Downloads/");

        pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pd.setIndeterminate(false);
        pd.setCancelable(true);
        pd.setMax(100);
        pd.setTitle("Please wait...");
        pd.setMessage("Downloading file");

        final String path = getIntent().getStringExtra("img");

        if (getIntent().getBooleanExtra("routine", false)) {
            download.setVisibility(View.VISIBLE);
        }
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (new ConnectionManager(ImageViewerActivity.this).isNetworkConnected()) {
                    pd.show();
                    final Handler handler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            pd.dismiss();
                            download.setImageResource(R.drawable.download_complete_res);
                            Toast.makeText(ImageViewerActivity.this, "Routine Successfully Downloaded to " + directory.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                        }
                    };
                    final Handler progressHandler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            pd.setProgress((total * 100) / fileLength);
                        }
                    };
                    Thread downloadThread = new Thread() {
                        public void run() {
                            try {
                                URL url = new URL(path);
                                con = (HttpURLConnection) url.openConnection();
                                con.setRequestMethod("GET");
                                con.setDoInput(true);
                                con.connect();
                                in = con.getInputStream();
                                fileLength = con.getContentLength();
                                System.out.println(con.getResponseCode());
                                if (!directory.exists()) {
                                    directory.mkdirs();
                                }

                                out = new FileOutputStream(directory + "/routine" + timestamp + ".jpg");
                                byte[] buffer = new byte[1024];
                                int len1 = 0;
                                total = 0;
                                while ((len1 = in.read(buffer)) > 0) {
                                    out.write(buffer, 0, len1);
                                    total += len1;
                                    progressHandler.sendEmptyMessage(0);
                                }
                                handler.sendEmptyMessage(0);
                                total = 0;
                                in.close();
                                out.close();

                            } catch (java.io.IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    downloadThread.start();
                } else {
                    Toast.makeText(ImageViewerActivity.this, "Connection Error!", Toast.LENGTH_SHORT).show();
                }
            }
        });


        Target<Bitmap> bitmap = Glide.with(this)
                .load(path)
                .asBitmap()
                .thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .fitCenter()
                .into(img);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}