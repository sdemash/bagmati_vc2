package com.leadingprotech.bagmatiboardingschool.news;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


public class NewsLoader extends AsyncTaskLoader<List<NewsModel>> {
    String uri;
    Context context;

    nBulletinDatabase db;

    public NewsLoader(Context context, String uri) {
        super(context);
        this.context = context;
        this.uri = uri;

    }

    @Override
    public List<NewsModel> loadInBackground() {
        db = nBulletinDatabase.getInstance(context);
        RequestQueue queue = Volley.newRequestQueue(context);


        StringRequest stringRequest = new StringRequest(Request.Method.GET, uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("news");
                            for (int i = 0; i < jsonArray.length(); ++i) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                final String id = object.getString("id");
                                String title = object.getString("title");
                                String message = object.getString("message");
                                final String image = object.getString("image");
                                String date_time = object.getString("updated_at");
                                boolean b = db.addNews(new NewsModel(id, title, message, date_time, image, String.valueOf(0)));
                                if (!b){
                                    db.updateNews(new NewsModel(id, title, message, date_time, image, String.valueOf(0)), id);
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(stringRequest);

        List<NewsModel> newsModelList;
        newsModelList = db.getNews();
        return newsModelList;
    }

}
