package com.leadingprotech.bagmatiboardingschool.news;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.github.chrisbanes.photoview.PhotoView;
import com.leadingprotech.bagmatiboardingschool.R;

import java.io.File;

public class ViewImageActivity extends AppCompatActivity {
    ImageView back;
    PhotoView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);

        img = (PhotoView) findViewById(R.id.img_viewer);
        final String path = getIntent().getStringExtra("img");
        File file = new File(path);
        if (file.exists()){
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            img.setImageBitmap(bitmap);
        }

        back = (ImageView) findViewById(R.id.img_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
