package com.leadingprotech.bagmatiboardingschool.news;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.leadingprotech.bagmatiboardingschool.R;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> {
    private List<NewsModel> newsModelList;
    private Context context;


    public NewsAdapter(List<NewsModel> newsModelList, Context context) {
        this.newsModelList = newsModelList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_news_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final NewsAdapter.MyViewHolder holder, int position) {
        final NewsModel newsModel = newsModelList.get(position);
        holder.title.setText(Html.fromHtml(newsModel.getTitle().substring(0, 1).toUpperCase() + newsModel.getTitle().substring(1)));

        if (newsModel.getStatus().equals("0")) {
            holder.notifiy.setImageResource(R.drawable.circle);
        } else {
            holder.notifiy.setImageResource(R.drawable.circle_grey);
        }

        if (position == newsModelList.size() - 1) {
            // Your last item
            holder.hr_line.setVisibility(View.GONE);
        } else {
            holder.hr_line.setVisibility(View.VISIBLE);

        }


        holder.description.setText(Html.fromHtml(newsModel.getDescription()).toString().trim());
        holder.id = newsModel.getId();
        holder.date_time = newsModel.getDate_time();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(holder.date_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        final int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        final String monthString = new DateFormatSymbols().getShortMonths()[month];
        final String tarik = day + "";
        final String baki = monthString + "," + " " + year;

        holder.number_date.setText(tarik);
        holder.date.setText(baki);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE");
        final String dayT = simpleDateFormat.format(date);

        String img_url = newsModel.getImage();
        if (img_url.equals("")) {
            holder.img.setVisibility(View.GONE);
            String first = holder.title.getText().toString().substring(0, 1).toUpperCase();
            holder.initial.setText(first);
            if (holder.initial.getText().toString().contains("H")) {
                holder.initial.setBackgroundResource(R.drawable.round_rect_green);
            } else if (holder.initial.getText().toString().contains("D")) {
                holder.initial.setBackgroundResource(R.drawable.round_rect_blue);
            }
            holder.initial.setVisibility(View.VISIBLE);
        } else {
            holder.initial.setVisibility(View.GONE);
            holder.img.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .load(newsModel.getImage())
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .centerCrop()
                    .into(holder.img);

        }


        holder.news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(view.getContext(), ViewNewsActivity.class);
                in.putExtra("id", newsModel.getId());
                in.putExtra("title", holder.title.getText().toString());
                in.putExtra("din", holder.number_date.getText().toString());
                in.putExtra("date", monthString + "\n" + year);
                in.putExtra("message", newsModel.getDescription());
                in.putExtra("image", newsModel.getImage());
                in.putExtra("weekday", dayT);
                view.getContext().startActivity(in);
            }
        });

    }

    public void setNewNewsData(List<NewsModel> newsList) {
        this.newsModelList = newsList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return newsModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        protected TextView number_date, date, title, description, initial;
        CardView news;
        ImageView img, notifiy;
        String date_time, id;
        View hr_line;

        public MyViewHolder(View itemView) {
            super(itemView);
            news = (CardView) itemView.findViewById(R.id.cv_news);
            initial = (TextView) itemView.findViewById(R.id.first_letter_tv);
            number_date = (TextView) itemView.findViewById(R.id.numerical_date_news);
            date = (TextView) itemView.findViewById(R.id.news_date);
            title = (TextView) itemView.findViewById(R.id.title_news);
            description = (TextView) itemView.findViewById(R.id.description_news);
            img = (ImageView) itemView.findViewById(R.id.img_round_rect);
            notifiy = (ImageView) itemView.findViewById(R.id.news_notify);
            hr_line = (View) itemView.findViewById(R.id.hr_line);


        }
    }


}
