package com.leadingprotech.bagmatiboardingschool.news;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.database_models.LoginModel;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;
import com.leadingprotech.bagmatiboardingschool.helper.VolleyManager;
import com.leadingprotech.bagmatiboardingschool.navigation.Activity_Accounts;
import com.leadingprotech.bagmatiboardingschool.navigation.NavActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.API_TOKEN;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;

public class NewsActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<NewsModel>> {
    String URL = BASE_URL + "news?api_key=" + API_TOKEN;

    RelativeLayout main_layout;
    LinearLayout no_data;
    ProgressBar progressBar;

    nBulletinDatabase db;
    SwipeRefreshLayout swipe;
    RecyclerView recyclerView;
    List<NewsModel> newsModelList;
    ConnectionManager connectionManager;
    List<NewsModel> newsModelListOnline = new ArrayList<>();
    private NewsAdapter newsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("News");
        }
        main_layout = (RelativeLayout) findViewById(R.id.main_layout_news);
        no_data = (LinearLayout) findViewById(R.id.layout_no_data);
        no_data.setVisibility(View.INVISIBLE);
        connectionManager = new ConnectionManager(this);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_news);
        progressBar = (ProgressBar) findViewById(R.id.progress_news);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_news);
        db = nBulletinDatabase.getInstance(getApplicationContext());

        newsModelList = db.getNews();

        newsAdapter = new NewsAdapter(newsModelList, getApplicationContext());


        if (getNewsStatus()) {
            if (connectionManager.isNetworkConnected()) {
                offlineData();
                prepareNewsData();
            } else {
                offlineData();
            }
        } else {
            prepareNewsData();
        }

        if (connectionManager.isNetworkConnected()) {
            prepareNewsData();
        } else {
            offlineData();

        }


        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(true);
                if (connectionManager.isNetworkConnected()) {
                    prepareNewsData();
                } else {
                    offlineData();
                    Snackbar snackbar = Snackbar.make(main_layout, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareNewsData();
                        }
                    });
                    snackbar.show();
                }
            }
        });
    }


    private void prepareNewsData() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            newsModelListOnline.clear();

                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("news");
                            for (int i = 0; i < jsonArray.length(); ++i) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                final String id = object.getString("id");
                                String title = object.getString("title");
                                String message = object.getString("message");
                                final String image = object.getString("image");
                                String date_time = object.getString("updated_at");
                                boolean b = db.addNews(new NewsModel(id, title, message, date_time, image, String.valueOf(0)));
                                if (!b) {
                                    db.updateNews(new NewsModel(id, title, message, date_time, image, String.valueOf(0)), id);
                                }
                                NewsModel newsModel = new NewsModel(id, title, message, date_time, image, String.valueOf(0));
                                newsModelListOnline.add(newsModel);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        SharedPreferences.Editor sp = getApplicationContext().getSharedPreferences("news_loader", Context.MODE_PRIVATE).edit();
                        sp.putBoolean("news_status", true);
                        sp.apply();
                        sp.commit();

                        for (NewsModel user1 : newsModelList) {
                            boolean flag = false;
                            for (NewsModel user2 : newsModelListOnline) {
                                if (user1.getId().equals(user2.getId())) {
                                    flag = true;
                                    break;
                                }
                            }

                            if (!flag) {

                                db.deleteANews(user1.getId());
                            }
                        }

                        offlineData();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                offlineData();


                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(main_layout, "No Internet Connection", Snackbar.LENGTH_INDEFINITE).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareNewsData();
                        }
                    });
                    snackbar.show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(NewsActivity.this, "Oops ! We are experiencing some problem.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyManager.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void offlineData() {
        newsModelList = db.getNews();
        if (newsModelList.size() != 0) {
            no_data.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            swipe.setRefreshing(false);
            Log.e("news1", String.valueOf(newsModelList));
            newsAdapter = new NewsAdapter(newsModelList, getApplicationContext());
            recyclerView.setAdapter(newsAdapter);
        } else {
            swipe.setRefreshing(false);
            progressBar.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
        }


    }


    private String getSavedToken() {
        SharedPreferences sp = getApplicationContext().getSharedPreferences("UserCid", MODE_PRIVATE);
        return sp.getString("cid", null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        List<LoginModel> loginModel = db.getLoggedInAccounts();
        if (loginModel.size() == 0) {
            Intent intent = new Intent(NewsActivity.this, NavActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else {
            Intent intent = new Intent(NewsActivity.this, Activity_Accounts.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            List<LoginModel> loginModel = db.getLoggedInAccounts();
            if (loginModel.size() == 0) {
                Intent intent = new Intent(NewsActivity.this, NavActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                Intent intent = new Intent(NewsActivity.this, Activity_Accounts.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
            finish();
        }
        if (id == R.id.refresh_view) {
            if (connectionManager.isNetworkConnected()) {
                prepareNewsData();
            } else {
                offlineData();
                Snackbar snackbar = Snackbar.make(main_layout, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        prepareNewsData();
                    }
                });
                snackbar.show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        prepareNewsData();
    }

    @Override
    public Loader<List<NewsModel>> onCreateLoader(int id, Bundle args) {
        return new NewsLoader(getApplicationContext(), URL);
    }

    @Override
    public void onLoadFinished(Loader<List<NewsModel>> loader, List<NewsModel> data) {
        if (data != null) {
            progressBar.setVisibility(View.GONE);
            // recyclerView.setVisibility(View.VISIBLE);
            swipe.setRefreshing(false);
            // noticeAdapter.setNewNoticeData(data);
            offlineData();

        }

    }

    @Override
    public void onLoaderReset(Loader<List<NewsModel>> loader) {
        newsAdapter.setNewNewsData(new ArrayList<NewsModel>());

    }

    public boolean getNewsStatus() {
        SharedPreferences sharedPreferences = getSharedPreferences("news_loader", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("news_status", false);
    }
}
