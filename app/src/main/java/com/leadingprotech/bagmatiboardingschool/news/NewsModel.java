package com.leadingprotech.bagmatiboardingschool.news;


public class NewsModel {
    String id,  title, description, date_time, image, status;
    public NewsModel(){

    }
    public NewsModel(String id,  String title, String description, String date_time, String image, String status){
        this.id = id;
        this.date_time = date_time;
        this.title = title;
        this.description = description;
        this.image = image;
        this.status = status;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

