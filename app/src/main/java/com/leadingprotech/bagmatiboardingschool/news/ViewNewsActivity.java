package com.leadingprotech.bagmatiboardingschool.news;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.navigation.NavActivity;

import java.text.DecimalFormat;

public class ViewNewsActivity extends AppCompatActivity {
    WebView webView;
    RelativeLayout rel;
    nBulletinDatabase db;
    float fontSize = 18;
    TextView title, initial, weekday;
    ImageView image;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getIntent().getStringExtra("title"));
        }
        setContentView(R.layout.activity_view_news);
        title = (TextView) findViewById(R.id.view_news_title);
        weekday = (TextView) findViewById(R.id.view_news_weekday);

        webView = (WebView) findViewById(R.id.view_news_message);
        initial = (TextView) findViewById(R.id.img_view_news);
        image = (ImageView) findViewById(R.id.img_news);
        fab = (FloatingActionButton) findViewById(R.id.share_fab_news);
        rel = (RelativeLayout) findViewById(R.id.rel);
        db = nBulletinDatabase.getInstance(getApplicationContext());

        db.updateNews(getIntent().getStringExtra("id"), String.valueOf(1));
        title.setText(getIntent().getStringExtra("title"));
        weekday.setText(getIntent().getStringExtra("weekday"));
        webView.loadData(getIntent().getStringExtra("message"), "text/html; charset=UTF-8", null);
        webView.setBackgroundColor(Color.TRANSPARENT);

        DecimalFormat formatter = new DecimalFormat("00");
        weekday.setText(getIntent().getStringExtra("date"));
        initial.setText(formatter.format(Integer.valueOf(getIntent().getStringExtra("din"))));


        final String path = getIntent().getStringExtra("image");
        if (path.isEmpty()) {
            image.setVisibility(View.GONE);
        } else {
            Glide.with(getApplicationContext())
                    .load(path)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(image);
        }


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(Intent.ACTION_SEND);
                in.setType("text/plain");
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                in.putExtra(Intent.EXTRA_SUBJECT, "Title: ");
                in.putExtra(Intent.EXTRA_TEXT, title.getText().toString() + "\n\n" +
                        Html.fromHtml(getIntent().getStringExtra("message")) + "\n\n" +
                        "Source: " + getResources().getString(R.string.institution_name));
                startActivity(Intent.createChooser(in, "Share"));
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getApplicationContext(), ImageViewerActivity.class);
                in.putExtra("img", path);
                startActivity(in);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.view_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        if (id == R.id.home_view) {
            Intent in = new Intent(getApplicationContext(), NavActivity.class);
            startActivity(in);
        }
        return super.onOptionsItemSelected(item);
    }

    public String getSavedOrg() {
        SharedPreferences sp = getApplicationContext().getSharedPreferences("Users", MODE_PRIVATE);
        return sp.getString("name", null);
    }
}
