package com.leadingprotech.bagmatiboardingschool.navigation;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.leadingprotech.bagmatiboardingschool.BuildConfig;
import com.leadingprotech.bagmatiboardingschool.LinkerActivity;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.about_us.AboutActivity;
import com.leadingprotech.bagmatiboardingschool.assignments.AssignmentActivity;
import com.leadingprotech.bagmatiboardingschool.attendance.AttendanceActivity;
import com.leadingprotech.bagmatiboardingschool.bus_routes.routes_activity.Activity_Routes;
import com.leadingprotech.bagmatiboardingschool.contact.ContactActivity;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.database_models.LoginModel;
import com.leadingprotech.bagmatiboardingschool.database_models.StudentDetailModel;
import com.leadingprotech.bagmatiboardingschool.developer_profile.DeveloperActivity;
import com.leadingprotech.bagmatiboardingschool.diary.Activity_Diary;
import com.leadingprotech.bagmatiboardingschool.download.DownloadActivity;
import com.leadingprotech.bagmatiboardingschool.event.EventActivity;
import com.leadingprotech.bagmatiboardingschool.gallery_album.GalleryAlbumActivity;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;
import com.leadingprotech.bagmatiboardingschool.helper.VolleyManager;
import com.leadingprotech.bagmatiboardingschool.inbox.InboxActivity;
import com.leadingprotech.bagmatiboardingschool.leave_request.Activity_LeaveRequest;
import com.leadingprotech.bagmatiboardingschool.login.LoginActivity;
import com.leadingprotech.bagmatiboardingschool.news.ImageViewerActivity;
import com.leadingprotech.bagmatiboardingschool.news.NewsActivity;
import com.leadingprotech.bagmatiboardingschool.notice.NoticeActivity;
import com.leadingprotech.bagmatiboardingschool.profile.Activity_Profile;
import com.leadingprotech.bagmatiboardingschool.resource.ResourceActivity;
import com.leadingprotech.bagmatiboardingschool.result.Activity_Result;
import com.leadingprotech.bagmatiboardingschool.video_list.VideoListActivity;
import com.leadingprotech.bagmatiboardingschool.widget.IconWidget;
import com.leadingprotech.bagmatiboardingschool.widget.NewAppWidget2;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.API_TOKEN;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.UPDATE_URL;

public class Activity_Accounts extends AppCompatActivity {

    public static final String SAVED_TOKEN_FILE = "fcm_token";
    public static final String SP_NAME_API_TOKEN = "apiToken";
    String URL_FCM_1 = BASE_URL + "student/token?api_token=";
    long backPressedTime = 0;
    ViewPager viewPager;
    nBulletinDatabase db;
    Toolbar toolbar;
    ConnectionManager connectionManager;
    SharedPreferences sharedPreferences;
    long PROFILE_SETTING = 1212;
    String apiToken;
    Drawer result;

    RequestManager glide;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounts);

        toolbar = (Toolbar) findViewById(R.id.toolbar_accounts);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Dashboard");
        glide = Glide.with(this);

        checkUpdate(false); // check for app update


        sharedPreferences = getSharedPreferences(SP_NAME_API_TOKEN, 0);
        apiToken = sharedPreferences.getString("apiToken", "");

        connectionManager = new ConnectionManager(this);
        viewPager = (ViewPager) findViewById(R.id.view_pager);

        db = nBulletinDatabase.getInstance(getApplicationContext());


        FirebaseMessaging.getInstance().subscribeToTopic("NOTIFICATION");
        try {
            FirebaseInstanceId.getInstance().deleteInstanceId();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String token = FirebaseInstanceId.getInstance().getToken();
        registerToken(token);

        new DrawerBuilder().withActivity(this).build();

        DrawerImageLoader.init(new AbstractDrawerImageLoader() {
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder) {
                glide.load(uri).placeholder(placeholder).centerCrop().into(imageView);
            }

            @Override
            public void cancel(ImageView imageView) {
            }
        });

        PrimaryDrawerItem notice = new PrimaryDrawerItem().withIdentifier(1).withName("Notice").withIcon(R.drawable.notice).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));
        PrimaryDrawerItem news = new PrimaryDrawerItem().withIdentifier(2).withName("News").withIcon(R.drawable.newspaper).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));
        PrimaryDrawerItem inbox = new PrimaryDrawerItem().withIdentifier(3).withName("Inbox").withIcon(R.drawable.ic_personal).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));
        PrimaryDrawerItem profile1 = new PrimaryDrawerItem().withIdentifier(4).withName("Profile").withIcon(R.drawable.profile).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));

        PrimaryDrawerItem downloads = new PrimaryDrawerItem().withIdentifier(5).withName("Downloads").withIcon(R.drawable.download).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));
        PrimaryDrawerItem resources = new PrimaryDrawerItem().withIdentifier(6).withName("Resources").withIcon(R.drawable.resource).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));
        PrimaryDrawerItem routine = new PrimaryDrawerItem().withIdentifier(7).withName("Routine").withIcon(R.drawable.routine).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));
        PrimaryDrawerItem gallery = new PrimaryDrawerItem().withIdentifier(8).withName("Gallery").withIcon(R.drawable.gallery).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));
        PrimaryDrawerItem contacts = new PrimaryDrawerItem().withIdentifier(9).withName("Contacts").withIcon(R.drawable.contact).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));
        PrimaryDrawerItem events = new PrimaryDrawerItem().withIdentifier(10).withName("Events").withIcon(R.drawable.events).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));
        PrimaryDrawerItem busRoutes = new PrimaryDrawerItem().withIdentifier(11).withName("Bus Routes").withIcon(R.drawable.ic_bus_route).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));
        PrimaryDrawerItem diary = new PrimaryDrawerItem().withIdentifier(12).withName("Diary").withIcon(R.drawable.ic_diary).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));
        PrimaryDrawerItem aboutUs = new PrimaryDrawerItem().withIdentifier(13).withName("About Us").withIcon(R.drawable.about).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));
        PrimaryDrawerItem developerProfile = new PrimaryDrawerItem().withIdentifier(14).withName("Made By").withIcon(R.drawable.developer).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));
        PrimaryDrawerItem leaveRequest = new PrimaryDrawerItem().withIdentifier(16).withName("Leave Request").withIcon(R.drawable.ic_leave).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));
        PrimaryDrawerItem logout = new PrimaryDrawerItem().withIdentifier(17).withName("Log Out").withIcon(R.drawable.ic_logout).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));

        PrimaryDrawerItem results = new PrimaryDrawerItem().withIdentifier(18).withName("Results").withIcon(R.drawable.ic_result).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));
        PrimaryDrawerItem attendance = new PrimaryDrawerItem().withIdentifier(19).withName("Attendance").withIcon(R.drawable.ic_attendance).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));

        PrimaryDrawerItem assignment = new PrimaryDrawerItem().withIdentifier(20).withName("Homework/Assignments").withIcon(R.drawable.ic_assignment).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));

        PrimaryDrawerItem videoGallery = new PrimaryDrawerItem().withIdentifier(21).withName("Video Gallery").withIcon(R.drawable.ic_video).withIconTintingEnabled(true).withIconColor(Color.parseColor("#FFFFFF"));

        DividerDrawerItem divider = new DividerDrawerItem();

        List<LoginModel> loginModelList = db.getLoggedInAccounts();

        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this).withTranslucentStatusBar(true)
                .withHeaderBackground(R.drawable.nav_header_plain)
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        if (profile instanceof IDrawerItem && profile.getIdentifier() == PROFILE_SETTING) {
                            Intent in = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(in);
                            finish();
                        } else if (profile instanceof IDrawerItem) {
                            SharedPreferences.Editor sp_editor = sharedPreferences.edit();
                            sp_editor.putString("apiToken", ((IDrawerItem) profile).getTag().toString());
                            sp_editor.apply();
                            apiToken = sharedPreferences.getString("apiToken", "");

                        }
                        return true;
                    }
                }).build();

        for (int i = 0; i < loginModelList.size(); i++) {


            headerResult.addProfiles(
                    new ProfileDrawerItem().withEmail(loginModelList.get(i).getFull_name())
                            .withIcon(loginModelList.get(i).getImage()).withTag(loginModelList.get(i).getApi_token()).withNameShown(false));

        }

        headerResult.addProfiles(new ProfileSettingDrawerItem().withName("Add Account").withIcon(R.drawable.ic_plus).withIconTinted(true).withIdentifier(PROFILE_SETTING));


        for (IProfile profile : headerResult.getProfiles()) {
            if (profile instanceof ProfileDrawerItem) {
                ProfileDrawerItem item = (ProfileDrawerItem) profile;

                if (item.getTag().equals(apiToken)) {
                    headerResult.setActiveProfile(profile);
                    break;
                }
            }
        }

//Now create your drawer and pass the AccountHeader.Result
        result = new DrawerBuilder()
                .withAccountHeader(headerResult).withActivity(this)
                .withActionBarDrawerToggle(true)
                .withToolbar(toolbar)
                .addDrawerItems(
                        inbox, profile1, assignment, attendance, routine, leaveRequest, diary, divider, notice, news, downloads, resources, gallery, videoGallery, contacts, events, busRoutes, aboutUs, developerProfile, divider, logout
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem.equals(1)) {

                            Intent in = new Intent(getApplicationContext(), NoticeActivity.class);
                            startActivity(in);

                        } else if (drawerItem.equals(2)) {
                            Intent in = new Intent(getApplicationContext(), NewsActivity.class);
                            startActivity(in);
                        } else if (drawerItem.equals(3)) {
                            Intent in = new Intent(getApplicationContext(), InboxActivity.class);
                            startActivity(in);
                        } else if (drawerItem.equals(4)) {
                            Intent in = new Intent(getApplicationContext(), Activity_Profile.class);
                            startActivity(in);
                        } else if (drawerItem.equals(5)) {
                            Intent in = new Intent(getApplicationContext(), DownloadActivity.class);
                            startActivity(in);
                        } else if (drawerItem.equals(6)) {
                            Intent in = new Intent(getApplicationContext(), ResourceActivity.class);
                            startActivity(in);
                        } else if (drawerItem.equals(7)) {
                            StudentDetailModel studentDetailModel = db.getStudentDetails(apiToken);
                            if (null != studentDetailModel) {

                                Intent in = new Intent(getApplicationContext(), ImageViewerActivity.class);
                                in.putExtra("img", studentDetailModel.getRoutine());
                                in.putExtra("routine", true);
                                Log.d("routine & apitoken", studentDetailModel.getRoutine() + " " + apiToken);
                                startActivity(in);
                            }
                        } else if (drawerItem.equals(8)) {
                            Intent in = new Intent(getApplicationContext(), GalleryAlbumActivity.class);
                            startActivity(in);
                        } else if (drawerItem.equals(9)) {
                            Intent in = new Intent(getApplicationContext(), ContactActivity.class);
                            startActivity(in);
                        } else if (drawerItem.equals(10)) {
                            Intent in = new Intent(getApplicationContext(), EventActivity.class);
                            startActivity(in);
                        } else if (drawerItem.equals(11)) {
                            Intent in = new Intent(getApplicationContext(), Activity_Routes.class);
                            startActivity(in);
                        } else if (drawerItem.equals(12)) {
                            Intent in = new Intent(getApplicationContext(), Activity_Diary.class);
                            startActivity(in);
                        } else if (drawerItem.equals(13)) {
                            Intent in = new Intent(getApplicationContext(), AboutActivity.class);
                            startActivity(in);
                        } else if (drawerItem.equals(14)) {
                            Intent in = new Intent(getApplicationContext(), DeveloperActivity.class);
                            startActivity(in);
                        } else if (drawerItem.equals(16)) {
                            Intent in = new Intent(getApplicationContext(), Activity_LeaveRequest.class);
                            startActivity(in);
                        } else if (drawerItem.equals(17)) {
                            if (!apiToken.equals("")) {
                                db.deleteLoginAccount(apiToken);
                                SharedPreferences.Editor spEditor_LoggedIn = sharedPreferences.edit();
                                spEditor_LoggedIn.putString("apiToken", "");
                                spEditor_LoggedIn.apply();
                                Intent intent = new Intent(Activity_Accounts.this, LinkerActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(intent);
                                finishAffinity();

                            }
                        } else if (drawerItem.equals(18)) {
                            Intent in = new Intent(getApplicationContext(), Activity_Result.class);
                            startActivity(in);
                        } else if (drawerItem.equals(19)) {
                            Intent in = new Intent(getApplicationContext(), AttendanceActivity.class);
                            startActivity(in);
                        } else if (drawerItem.equals(20)) {
                            Intent in = new Intent(getApplicationContext(), AssignmentActivity.class);
                            startActivity(in);
                        } else if (drawerItem.equals(21)) {
                            Intent in = new Intent(getApplicationContext(), VideoListActivity.class);
                            startActivity(in);
                        }

                        return false;
                    }
                })
                .build();

        result.setSelection(100);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        result.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);

        //update widgets on login
        Intent intent = new Intent(Activity_Accounts.this, IconWidget.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        int[] ids = {0};
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        sendBroadcast(intent);

        Intent intent2 = new Intent(Activity_Accounts.this, NewAppWidget2.class);
        intent2.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        intent2.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        sendBroadcast(intent2);

        db = nBulletinDatabase.getInstance(getApplicationContext());

        DashboardFragment dashboardFragment = new DashboardFragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction =
                getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, dashboardFragment);
        fragmentTransaction.commit();
        getSupportActionBar().setTitle("Dashboard");
    }

    @Override
    public void onBackPressed() {
        long t = System.currentTimeMillis();
        if (t - backPressedTime > 2000) {
            backPressedTime = t;
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        result.setSelection(100);
    }


    public String getSavedToken() {
        SharedPreferences sp = getApplicationContext().getSharedPreferences("UserCid", MODE_PRIVATE);
        return sp.getString("cid", null);
    }


    private void registerToken(final String token) {

        StringRequest sr = new StringRequest(Request.Method.POST, URL_FCM_1 + apiToken, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("RESPONSE TOKEN", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", token);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyManager.getInstance(getApplicationContext()).addToRequestQueue(sr);
        SharedPreferences.Editor sp_editor = getSharedPreferences(SAVED_TOKEN_FILE, MODE_PRIVATE).edit();
        sp_editor.putString("token", token);
        sp_editor.apply();
    }

    ///check for update

    private void checkUpdate(final boolean manualCheck) {

        final ProgressBar pb = (ProgressBar) findViewById(R.id.progressbar);
        if (manualCheck) {
            pb.setVisibility(View.VISIBLE);
        }

        AlertDialog.Builder builder;
        final AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.custom_update_dialog,
                (ViewGroup) findViewById(R.id.rootLayout));

        final TextView title = (TextView) layout.findViewById(R.id.title);
        final WebView message = (WebView) layout.findViewById(R.id.message);
        Button update, later;

        update = (Button) layout.findViewById(R.id.button_update);
        later = (Button) layout.findViewById(R.id.button_later);

        builder = new AlertDialog.Builder(Activity_Accounts.this);
        builder.setView(layout);
        alertDialog = builder.create();

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

                alertDialog.dismiss();
            }
        });
        later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });


        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                UPDATE_URL + API_TOKEN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                pb.setVisibility(View.GONE);

                boolean _error = true;

                try {
                    Log.d("RESPONSE", response);
                    JSONObject jsonObject = new JSONObject(response);
                    _error = jsonObject.getBoolean("error");

                    if (!_error && jsonObject.getString("status_code").equals("200")) {
                        JSONObject admin = jsonObject.getJSONObject("school");
                        final String version_code = admin.getString("version_code");
                        String description = admin.getString("description");

                        title.setText("App Update !");

                        message.loadData(description, "text/html; charset=UTF-8", null);


                        if (version_code != null && !version_code.isEmpty() && BuildConfig.VERSION_CODE < Integer.parseInt(version_code)) {

                            alertDialog.show();

                        } else {
                            if (manualCheck) {
                                Snackbar snackbar = Snackbar.make(toolbar, "You have the latest version of " + getResources().getString(R.string.app_name) + " app.", 3000);
                                snackbar.show();
                            }
                        }


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pb.setVisibility(View.GONE);

                if (manualCheck) {
                    Snackbar snackbar = Snackbar.make(toolbar, "Update information not available.", 3000);
                    snackbar.show();
                }

            }
        });

        // Adding request to request queue
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyManager.getInstance(Activity_Accounts.this).addToRequestQueue(stringRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.check_update_afterlogin, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.check_update) {
            checkUpdate(true);

        }

        return super.onOptionsItemSelected(item);
    }

}