package com.leadingprotech.bagmatiboardingschool.navigation;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Scroller;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.leadingprotech.bagmatiboardingschool.BuildConfig;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.about_us.AboutActivity;
import com.leadingprotech.bagmatiboardingschool.bus_routes.routes_activity.Activity_Routes;
import com.leadingprotech.bagmatiboardingschool.contact.ContactActivity;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.developer_profile.DeveloperActivity;
import com.leadingprotech.bagmatiboardingschool.download.DownloadActivity;
import com.leadingprotech.bagmatiboardingschool.event.EventActivity;
import com.leadingprotech.bagmatiboardingschool.gallery_album.GalleryAlbumActivity;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;
import com.leadingprotech.bagmatiboardingschool.helper.CustomLinearLayout;
import com.leadingprotech.bagmatiboardingschool.helper.OnItemSelectedListener;
import com.leadingprotech.bagmatiboardingschool.helper.ParallaxPageTransformer;
import com.leadingprotech.bagmatiboardingschool.helper.VolleyManager;
import com.leadingprotech.bagmatiboardingschool.login.LoginActivity;
import com.leadingprotech.bagmatiboardingschool.navigation_drawer.DrawerAdapter;
import com.leadingprotech.bagmatiboardingschool.navigation_drawer.DrawerItem;
import com.leadingprotech.bagmatiboardingschool.news.NewsActivity;
import com.leadingprotech.bagmatiboardingschool.notice.NoticeFragment;
import com.leadingprotech.bagmatiboardingschool.notice.ViewPagerAdapter;
import com.leadingprotech.bagmatiboardingschool.resource.ResourceActivity;
import com.leadingprotech.bagmatiboardingschool.video_list.VideoListActivity;
import com.leadingprotech.bagmatiboardingschool.widget.IconWidget;
import com.leadingprotech.bagmatiboardingschool.widget.NewAppWidget2;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import me.relex.circleindicator.CircleIndicator;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.API_TOKEN;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.UPDATE_URL;

public class NavActivity extends AppCompatActivity
        implements AppBarLayout.OnOffsetChangedListener {

    public static final String SAVED_TOKEN_FILE = "fcm_token";
    private static int currentPage = 0;
    private static int Num_Pages = 0;
    String URL_FCM_1 = BASE_URL + "token?api_key=";
    String URL_SLIDER_1 = BASE_URL + "notice?api_key=" + API_TOKEN;
    long backPressedTime = 0;
    RelativeLayout layout;
    String slider_img;
    CircleIndicator indicator;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    nBulletinDatabase db;
    TextView header;
    ImageView header_img;
    String path;
    int position = 0;
    NavigationView navigationView;
    ConnectionManager connectionManager;

    ArrayList<String> IMAGES = new ArrayList<>();

    Handler h = new Handler();
    Runnable r = new Runnable() {
        @Override
        public void run() {
            currentPage = currentPage + 1;
            if (currentPage >= Num_Pages) {
                currentPage = 0;
            }
            setPage(currentPage);
        }
    };

    // AppBarLayout appBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Notice");

        connectionManager = new ConnectionManager(this);
        viewPager = (ViewPager) findViewById(R.id.view_pager);

        db = nBulletinDatabase.getInstance(getApplicationContext());

        checkUpdate(); // check for app update

        FirebaseMessaging.getInstance().subscribeToTopic("NOTIFICATION");
        try {
            FirebaseInstanceId.getInstance().deleteInstanceId();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String token = FirebaseInstanceId.getInstance().getToken();
        registerToken(token);

//update widgets on login
        Intent intent = new Intent(NavActivity.this, IconWidget.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        int[] ids = {0};
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        sendBroadcast(intent);

        Intent intent2 = new Intent(NavActivity.this, NewAppWidget2.class);
        intent2.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        intent2.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        sendBroadcast(intent2);

        db = nBulletinDatabase.getInstance(getApplicationContext());
        indicator = (CircleIndicator) findViewById(R.id.indicator_default);
        layout = (RelativeLayout) findViewById(R.id.indicator_viewpager);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        Context context = this;

        prepareNoticeSlider();


        NoticeFragment newsFragment = new NoticeFragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction =
                getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, newsFragment);
        fragmentTransaction.commit();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("HOME");
        }

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //Custom Navigation Drawer

        ImageView nav_header = (ImageView) findViewById(R.id.nav_header);
        Glide.with(this).load(R.drawable.nav_header).into(nav_header);

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.drawerRecyclerView);
        ArrayList<DrawerItem> mDrawerItemList = new ArrayList<DrawerItem>();

        // nav drawer items and icons from String array and int array resources
        String nav_items[] = getResources().getStringArray(R.array.nav_items);
        TypedArray nav_icons = getResources().obtainTypedArray(R.array.nav_icons);

        for (int i = 0; i < nav_items.length; i++) {
            mDrawerItemList.add(new DrawerItem(nav_icons.getDrawable(i), nav_items[i]));
        }

        nav_icons.recycle();

        DrawerAdapter adapter = new DrawerAdapter(mDrawerItemList, this);
        mRecyclerView.setLayoutManager(new CustomLinearLayout(this, LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setAdapter(adapter);

        adapter.setOnItemClickLister(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(View v, int position) {
                switch (position) {
                    case 0:
                        NoticeFragment fragment = new NoticeFragment();
                        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.fragment_container, fragment);
                        fragmentTransaction.commit();
                        getSupportActionBar().setTitle("Notice");

                        break;
                    case 1: {
                        Intent in = new Intent(NavActivity.this, NewsActivity.class);
                        startActivity(in);

                        break;
                    }
                    case 2: {
                        Intent in = new Intent(NavActivity.this, DownloadActivity.class);
                        startActivity(in);

                        break;
                    }
                    case 3: {
                        Intent in = new Intent(NavActivity.this, ResourceActivity.class);
                        startActivity(in);

                        break;
                    }
                    case 4: {
                        Intent in = new Intent(NavActivity.this, GalleryAlbumActivity.class);
                        startActivity(in);
                        break;
                    }
                    case 5: {
                        Intent in = new Intent(NavActivity.this, VideoListActivity.class);
                        startActivity(in);
                        break;
                    }
                    case 6: {
                        Intent in = new Intent(NavActivity.this, ContactActivity.class);
                        startActivity(in);
                        break;
                    }
                    case 7: {
                        Intent in = new Intent(NavActivity.this, EventActivity.class);
                        startActivity(in);
                        break;
                    }
                    case 8: {
                        Intent in = new Intent(NavActivity.this, Activity_Routes.class);
                        startActivity(in);
                        break;
                    }
                    case 9: {
                        Intent in = new Intent(NavActivity.this, AboutActivity.class);
                        startActivity(in);
                        break;
                    }

                    case 10: {
                        Intent in = new Intent(NavActivity.this, DeveloperActivity.class);
                        startActivity(in);

                        break;
                    }
                    case 11: {
                        Intent in = new Intent(NavActivity.this, LoginActivity.class);
                        startActivity(in);
                        finish();
                        break;
                    }
                }
            }
        });
/////////////////////////////////////////////////////////////


    }

    private void registerToken(final String token) {

        StringRequest sr = new StringRequest(Request.Method.POST, URL_FCM_1 + API_TOKEN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("RESPONSE TOKEN", response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", token);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        VolleyManager.getInstance(getApplicationContext()).addToRequestQueue(sr);
        SharedPreferences.Editor sp_editor = getSharedPreferences(SAVED_TOKEN_FILE, MODE_PRIVATE).edit();
        sp_editor.putString("token", token);
        sp_editor.apply();
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            long t = System.currentTimeMillis();
            if (t - backPressedTime > 2000) {
                backPressedTime = t;
            } else {
                super.onBackPressed();
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        h.removeCallbacks(r);
        h.postDelayed(r, 3000);

    }

    @Override
    protected void onPause() {
        h.removeCallbacks(r);
        super.onPause();
    }


    public void prepareNoticeSlider() {

        loadOfflineSliders();

        if (connectionManager.isNetworkConnected()) {

            StringRequest stringRequest1 = new StringRequest(Request.Method.GET, URL_SLIDER_1, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray1 = jsonObject.getJSONArray("slider");
                        boolean result = false;
                        if (jsonArray1.length() > 0) {
                            db.deleteSliderImages();
                        }
                        for (int j = 0; j < jsonArray1.length(); ++j) {
                            JSONObject object1 = jsonArray1.getJSONObject(j);
                            slider_img = object1.getString("image_thumb");
                            result = db.addSliderImages(slider_img);
                        }
                        if (result) {
                            loadOfflineSliders();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            VolleyManager.getInstance(getApplicationContext()).addToRequestQueue(stringRequest1);
        }
    }


    private void loadOfflineSliders() {

        IMAGES = db.getSliderImages();

        // slow things down; custom speed for slider image transition
        CustomScroller customScroller = new CustomScroller(this, new LinearInterpolator(), 500);
        viewPagerAdapter = new ViewPagerAdapter(NavActivity.this, IMAGES);
        viewPagerAdapter.notifyDataSetChanged();

        try {
            Field mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            mScroller.set(viewPager, customScroller);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setPageTransformer(true, new ParallaxPageTransformer());

        indicator.setViewPager(viewPager);
        Num_Pages = IMAGES.size();

        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                currentPage = position;
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

                currentPage = position;
                h.removeCallbacks(r);
                h.postDelayed(r, 3000);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    public String getSavedToken() {
        SharedPreferences sp = getApplicationContext().getSharedPreferences("UserCid", MODE_PRIVATE);
        return sp.getString("cid", null);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

    }

    public void setPage(int pos) {
        viewPager.setCurrentItem(pos, true);
    }

    ///check for update

    private void checkUpdate() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                UPDATE_URL + API_TOKEN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                boolean _error = true;

                try {
                    Log.d("RESPONSE", response);
                    JSONObject jsonObject = new JSONObject(response);
                    _error = jsonObject.getBoolean("error");

                    if (!_error && jsonObject.getString("status_code").equals("200")) {
                        JSONObject admin = jsonObject.getJSONObject("school");
                        final String version_code = admin.getString("version_code");
                        String description = admin.getString("description");

                        if (version_code != null && !version_code.isEmpty() && BuildConfig.VERSION_CODE < Integer.parseInt(version_code)) {

                            AlertDialog.Builder builder;
                            final AlertDialog alertDialog;

                            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                            View layout = inflater.inflate(R.layout.custom_update_dialog,
                                    (ViewGroup) findViewById(R.id.rootLayout));

                            final TextView title = (TextView) layout.findViewById(R.id.title);
                            final WebView message = (WebView) layout.findViewById(R.id.message);
                            Button update, later;

                            update = (Button) layout.findViewById(R.id.button_update);
                            later = (Button) layout.findViewById(R.id.button_later);

                            builder = new AlertDialog.Builder(NavActivity.this);
                            builder.setView(layout);
                            alertDialog = builder.create();

                            update.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                    }

                                    alertDialog.dismiss();
                                }
                            });
                            later.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();
                                }
                            });

                            title.setText(R.string.text_app_update);

                            message.loadData(description, "text/html; charset=UTF-8", null);
                            alertDialog.show();

                        }


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        // Adding request to request queue
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyManager.getInstance(NavActivity.this).addToRequestQueue(stringRequest);

    }


    ////////////////////////////////////////////////////

    public static class CustomScroller extends Scroller {

        private int mDuration;

        public CustomScroller(Context context, Interpolator interpolator, int duration) {
            super(context, interpolator);
            mDuration = duration;
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration);
        }
    }

    ///////////////////////////////////////////////////////

}
