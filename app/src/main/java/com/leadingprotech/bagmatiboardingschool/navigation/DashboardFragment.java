package com.leadingprotech.bagmatiboardingschool.navigation;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.assignments.AssignmentActivity;
import com.leadingprotech.bagmatiboardingschool.attendance.AttendanceActivity;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;
import com.leadingprotech.bagmatiboardingschool.helper.ParallaxPageTransformer;
import com.leadingprotech.bagmatiboardingschool.helper.VolleyManager;
import com.leadingprotech.bagmatiboardingschool.inbox.InboxActivity;
import com.leadingprotech.bagmatiboardingschool.notice.NoticeActivity;
import com.leadingprotech.bagmatiboardingschool.notice.ViewPagerAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.API_TOKEN;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {


    private static int currentPage = 0;
    private static int Num_Pages = 0;
    CardView notice, homework, attendance, inbox;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    ArrayList<String> IMAGES = new ArrayList<>();
    String URL_SLIDER_1 = BASE_URL + "notice?api_key=" + API_TOKEN;
    CircleIndicator indicator;
    TextView mTextView1,mTextView2,mTextView3,mTextView4;

    ConnectionManager connectionManager;
    nBulletinDatabase db;
    RequestManager glide;

    String slider_img;


    Handler h = new Handler();
    Runnable r = new Runnable() {
        @Override
        public void run() {
            currentPage = currentPage + 1;
            if (currentPage >= Num_Pages) {
                currentPage = 0;
            }
            setPage(currentPage);
        }
    };

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        connectionManager = new ConnectionManager(getActivity().getApplicationContext());
        db = nBulletinDatabase.getInstance(getActivity().getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/seguisb.ttf");


        mTextView1 = (TextView) view.findViewById(R.id.textView1);
        mTextView2 = (TextView) view.findViewById(R.id.textView2);
        mTextView3 = (TextView) view.findViewById(R.id.textView3);
        mTextView4 = (TextView) view.findViewById(R.id.textView4);
        notice = (CardView) view.findViewById(R.id.card_notice);
        homework = (CardView) view.findViewById(R.id.card_homework);
        attendance = (CardView) view.findViewById(R.id.card_attendance);
        inbox = (CardView) view.findViewById(R.id.card_inbox);

        mTextView1.setTypeface(typeface);
        mTextView2.setTypeface(typeface);
        mTextView3.setTypeface(typeface);
        mTextView4.setTypeface(typeface);


        //april 25th

        viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        indicator = (CircleIndicator) view.findViewById(R.id.indicator_default);

        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        glide = Glide.with(this);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
                h.removeCallbacks(r);
                h.postDelayed(r, 3000);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        prepareNoticeSlider();

        notice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Intent in = new Intent(getContext(), NoticeActivity.class);
                startActivity(in);

            }
        });

        homework.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                Intent in = new Intent(getContext(), AssignmentActivity.class);
                startActivity(in);
            }
        });

        attendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Intent in = new Intent(getContext(), AttendanceActivity.class);
                startActivity(in);
            }
        });

        inbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                Intent in = new Intent(getContext(), InboxActivity.class);
                startActivity(in);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        h.removeCallbacks(r);
        h.postDelayed(r, 3000);
    }

    @Override
    public void onPause() {

        h.removeCallbacks(r);
        super.onPause();
    }

    ////slider part


    public void prepareNoticeSlider() {

        loadOfflineSliders();

        if (connectionManager.isNetworkConnected()) {

            StringRequest stringRequest1 = new StringRequest(Request.Method.GET, URL_SLIDER_1, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray1 = jsonObject.getJSONArray("slider");
                        boolean result = false;
                        if (jsonArray1.length() > 0) {
                            db.deleteSliderImages();
                        }
                        for (int j = 0; j < jsonArray1.length(); ++j) {
                            JSONObject object1 = jsonArray1.getJSONObject(j);
                            slider_img = object1.getString("image_thumb");
                            result = db.addSliderImages(slider_img);
                        }
                        if (result) {
                            loadOfflineSliders();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            VolleyManager.getInstance(getActivity().getApplicationContext()).addToRequestQueue(stringRequest1);
        }
    }

    private void loadOfflineSliders() {

        IMAGES = db.getSliderImages();

        NavActivity.CustomScroller customScroller = new NavActivity.CustomScroller(getActivity(), new LinearInterpolator(), 500);
        viewPagerAdapter = new ViewPagerAdapter(getActivity(), IMAGES);
        viewPagerAdapter.notifyDataSetChanged();

        try {
            Field mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            mScroller.set(viewPager, customScroller);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setPageTransformer(true, new ParallaxPageTransformer());

        indicator.setViewPager(viewPager);
        Num_Pages = IMAGES.size();

        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                currentPage = position;
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void setPage(int pos) {
        viewPager.setCurrentItem(pos, true);
    }

}
