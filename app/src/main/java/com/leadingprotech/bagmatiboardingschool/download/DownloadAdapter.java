package com.leadingprotech.bagmatiboardingschool.download;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.services.Service_Download;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DownloadAdapter extends RecyclerView.Adapter<DownloadAdapter.MyViewHolder> {
    ProgressDialog pd;
    File directory;
    FileOutputStream out;
    InputStream in;
    HttpURLConnection con;
    int fileLength;
    int total = 0;


    private List<DownloadModel> downloadModelList;
    private Context context;


    public DownloadAdapter(List<DownloadModel> downloadModelList, Context context) {
        this.context = context;
        this.downloadModelList = downloadModelList;
    }

    @Override
    public DownloadAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_download_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DownloadAdapter.MyViewHolder holder, int position) {
        final DownloadModel downloadModel = downloadModelList.get(position);
        holder.title.setText(downloadModel.getTitle());
        holder.link = downloadModel.getLink();
        holder.date_time = downloadModel.getDate_time();
        holder.id = downloadModel.getId();
        holder.filename = downloadModel.getFilename();
        holder.extension = downloadModel.getExtension();
        holder.size.setText(downloadModel.getSize() + " Bytes");

        directory = new File(Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.app_name) + " Downloads/");
        holder.file = new File(Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.app_name) + " Downloads/" + downloadModel.getTitle() + "." + downloadModel.getExtension());

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy-MM-dd");
        Date date = null;
        try {
            date = simpleDateFormat.parse(holder.date_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int days = cal.get(Calendar.DAY_OF_WEEK);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        String monthString = new DateFormatSymbols().getShortMonths()[month];
        final String tarik = day + "";
        final String baki = monthString + "," + " " + year;
        holder.number_date.setText(tarik);
        holder.date.setText(baki);
        if (holder.file.exists()) {
            holder.img_download.setImageResource(R.drawable.download_complete_res);
        }
        holder.download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("clicked", "csd");

                Intent intent = new Intent(view.getContext(), Service_Download.class);
                intent.putExtra("title", downloadModel.getTitle());
                intent.putExtra("extension", downloadModel.getExtension());
                intent.putExtra("link", downloadModel.getLink().replaceAll(" ", "%20"));
                view.getContext().startService(intent);

//                final Handler handler = new Handler() {
//                    @Override
//                    public void handleMessage(Message msg) {
//                        pd.dismiss();
//                        holder.img_download.setImageResource(R.drawable.download_complete_res);
//                        Toast.makeText(context, "File Successfully Downloaded", Toast.LENGTH_SHORT).show();
//                    }
//                };
//                final Handler progressHandler = new Handler() {
//                    @Override
//                    public void handleMessage(Message msg) {
//                        pd.setProgress((total * 100) / fileLength);
//                    }
//                };
//                if (!holder.file.exists()) {
//                    if (new ConnectionManager(context).isNetworkConnected()) {
//
//                        pd.show();
//                        Thread downloadThread = new Thread() {
//                            public void run() {
//                                try {
//                                    URL url = new URL(downloadModel.getLink().replaceAll(" ", "%20"));
//                                    con = (HttpURLConnection) url.openConnection();
//                                    con.setRequestMethod("GET");
//                                    con.setDoInput(true);
//                                    con.connect();
//                                    in = con.getInputStream();
//                                    fileLength = con.getContentLength();
//                                    System.out.println(con.getResponseCode());
//                                    if (!directory.exists()) {
//                                        directory.mkdirs();
//                                    }
//
//                                    out = new FileOutputStream(holder.file);
//                                    byte[] buffer = new byte[1024];
//                                    int len1 = 0;
//                                    total = 0;
//                                    while ((len1 = in.read(buffer)) > 0) {
//                                        out.write(buffer, 0, len1);
//                                        total += len1;
//                                        progressHandler.sendEmptyMessage(0);
//                                    }
//                                    handler.sendEmptyMessage(0);
//                                    total = 0;
//                                    in.close();
//                                    out.close();
//
//                                } catch (java.io.IOException e) {
//                                    pd.dismiss();
//                                    Toast.makeText(context, "Error downloading the file.", Toast.LENGTH_SHORT).show();
//                                    e.printStackTrace();
//                                }
//                            }
//                        };
//                        downloadThread.start();
//                    } else {
//                        Toast.makeText(view.getContext(), "Connection Error!", Toast.LENGTH_SHORT).show();
//                    }
//                } else {
//                    Toast.makeText(view.getContext(), "File Already Exists", Toast.LENGTH_SHORT).show();
//                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return downloadModelList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_download;
        File file;
        String extension, filename;
        CardView download;
        String link, id, date_time;
        TextView number_date, date, title, size;

        public MyViewHolder(View itemView) {
            super(itemView);
            img_download = (ImageView) itemView.findViewById(R.id.status_download);
            download = (CardView) itemView.findViewById(R.id.cv_download);
            number_date = (TextView) itemView.findViewById(R.id.number_date_download);
            date = (TextView) itemView.findViewById(R.id.date_download);
            title = (TextView) itemView.findViewById(R.id.title_download);
            size = (TextView) itemView.findViewById(R.id.size_download);

            pd = new ProgressDialog(context);
            pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pd.setIndeterminate(false);
            pd.setCancelable(false);
            pd.setCanceledOnTouchOutside(false);
            pd.setMax(100);
            pd.setTitle("Please wait...");
            pd.setMessage("Downloading file");
        }
    }
}
