package com.leadingprotech.bagmatiboardingschool.download;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.leadingprotech.bagmatiboardingschool.R;

import java.util.List;

public class AvailableAdapter extends RecyclerView.Adapter<AvailableAdapter.MyViewHolder> {
    private List<AvailableModel> availableModelList;
    private Context context;

    public AvailableAdapter(List<AvailableModel> availableModelList, Context context) {
        this.availableModelList = availableModelList;
        this.context = context;
    }

    @Override
    public AvailableAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_available, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AvailableAdapter.MyViewHolder holder, int position) {
        final AvailableModel availableModel = availableModelList.get(position);
        holder.filename.setText(availableModel.getFilename());
        final String path = availableModel.getPath();


        if (path.contains(".doc") || path.contains(".docx")) {
            holder.file_icon.setImageResource(R.drawable.word_file);
        } else if (path.contains(".pdf")) {
            holder.file_icon.setImageResource(R.drawable.pdf_file);
        } else if (path.contains(".ppt") || path.contains(".pptx")) {
            holder.file_icon.setImageResource(R.drawable.ppt_file);
        } else if (path.contains(".xls") || path.contains(".xlsx")) {
            holder.file_icon.setImageResource(R.drawable.excel_file);
        } else if (path.contains(".zip") || path.contains(".rar")) {
            holder.file_icon.setImageResource(R.drawable.zip_file);
        } else if (path.contains(".wav") || path.contains(".mp3") || path.contains(".mp4") || path.contains(".m4a")) {
            holder.file_icon.setImageResource(R.drawable.music_file);
        } else if (path.contains(".jpg") || path.contains(".jpeg") || path.contains(".png") || path.contains(".PNG")) {
            holder.file_icon.setImageResource(R.drawable.picture_file);
        } else {
            holder.file_icon.setImageResource(R.drawable.unknown_file);
        }

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                try {
                    Uri uri = Uri.parse("file://"+availableModel.getPath());
                    Intent in = new Intent(android.content.Intent.ACTION_VIEW);
                    if (path.contains(".doc") || path.contains(".docx")) {
                        in.setDataAndType(uri, "application/msword");
                    } else if (path.contains(".pdf")) {
                        in.setDataAndType(uri, "application/pdf");
                    } else if (path.contains(".ppt") || uri.toString().contains(".pptx")) {
                        in.setDataAndType(uri, "application/vnd.ms-powerpoint");
                    } else if (path.contains(".xls") || uri.toString().contains(".xlsx")) {
                        in.setDataAndType(uri, "application/vnd.ms-excel");
                    } else if (path.contains(".zip") || path.contains(".rar")) {
                        in.setDataAndType(uri, "application/x.wav");
                    } else if (path.contains(".rtf")) {
                        // RTF file
                        in.setDataAndType(uri, "application/rtf");
                    } else if (path.contains(".wav") || path.contains(".mp3")) {
                        // WAV audio file
                        in.setDataAndType(uri, "audio/x-wav");
                    } else if (path.contains(".gif")) {
                        // GIF file
                        in.setDataAndType(uri, "image/gif");
                    } else if (path.contains(".jpg") || path.contains(".jpeg") || path.contains(".png")) {
                        // JPG file
                        in.setDataAndType(uri, "image/jpeg");
                    } else if (path.contains(".txt")) {
                        // Text file
                        in.setDataAndType(uri, "text/plain");
                    } else if (path.contains(".3gp") || path.contains(".mpg") || path.contains(".mpeg") || path.contains(".mpe") || path.contains(".mp4") || path.contains(".avi")) {
                        // Video files
                        in.setDataAndType(uri, "video/*");
                    } else {
                        in.setDataAndType(uri, "*/*");
                    }
                    in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(in);
                } catch (ActivityNotFoundException exception) {
                    exception.printStackTrace();
                    Snackbar snackbar = Snackbar.make(view, "No application found to open the file", Snackbar.LENGTH_SHORT).setAction("Retry", null);
                    snackbar.show();
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return availableModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout layout;
        ImageView file_icon;
        String id;
        TextView filename;

        public MyViewHolder(View itemView) {
            super(itemView);
            file_icon = (ImageView) itemView.findViewById(R.id.file_icon);
            layout = (RelativeLayout) itemView.findViewById(R.id.layout);
            filename = (TextView) itemView.findViewById(R.id.tv_available_file);
        }
    }


}
