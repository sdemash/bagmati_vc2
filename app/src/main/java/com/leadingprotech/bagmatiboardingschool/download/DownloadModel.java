package com.leadingprotech.bagmatiboardingschool.download;


public class DownloadModel {
    String id;
    String title;
    String link;
    String date_time;
    String extension;
    String filename;
    private String size;

    public DownloadModel() {

    }

    public DownloadModel(String id, String title, String link, String date_time, String extension, String filename, String size) {
        this.id = id;
        this.title = title;
        this.link = link;
        this.date_time = date_time;
        this.extension = extension;
        this.filename = filename;
        this.setSize(size);

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
