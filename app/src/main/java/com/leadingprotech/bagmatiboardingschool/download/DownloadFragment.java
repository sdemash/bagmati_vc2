package com.leadingprotech.bagmatiboardingschool.download;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;
import com.leadingprotech.bagmatiboardingschool.helper.VolleyManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.API_TOKEN;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;


public class DownloadFragment extends Fragment {
    protected List<DownloadModel> downloadModelList = new ArrayList<>();
    protected DownloadAdapter downloadAdapter;
    ConnectionManager connectionManager;
    LinearLayout no_data;
    TextView error_msg;
    ProgressBar progressBar;
    String URL = BASE_URL + "file?api_key=" + API_TOKEN;
    SwipeRefreshLayout swipe;
    String id, title, link, date_time, extension, filename, size;
    RecyclerView recyclerView;

    public DownloadFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_download, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_download);
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe_downloads);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_download);
        error_msg = (TextView) view.findViewById(R.id.error_msg);
        no_data = (LinearLayout) view.findViewById(R.id.no_downloads);

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        connectionManager = new ConnectionManager(getContext());

        if (connectionManager.isNetworkConnected()) {
            prepareDownloadData();
        } else {
            progressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);

        }
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(true);
                if (connectionManager.isNetworkConnected()) {
                    prepareDownloadData();
                } else {
                    swipe.setRefreshing(false);
                    Snackbar snackbar = Snackbar.make(swipe, "No Internet Connection", Snackbar.LENGTH_SHORT).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareDownloadData();
                        }
                    });
                    snackbar.show();
                }
            }
        });
    }

    private void prepareDownloadData() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);
                            downloadModelList.clear();
                            progressBar.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            JSONArray jsonArray = object.getJSONArray("file");
                            for (int i = 0; i < jsonArray.length(); ++i) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                id = jsonObject.getString("id");
                                title = jsonObject.getString("title");
                                link = jsonObject.getString("link");
                                date_time = jsonObject.getString("updated_at");
                                extension = jsonObject.getString("extension");
                                filename = jsonObject.getString("file");
                                size = jsonObject.getString("size");


                                downloadModelList.add(new DownloadModel(id, title, link, date_time, extension, filename, size));
                            }
                            swipe.setRefreshing(false);
                            downloadAdapter = new DownloadAdapter(downloadModelList, getContext());
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(layoutManager);
                            recyclerView.setItemAnimator(new DefaultItemAnimator());
                            recyclerView.setAdapter(downloadAdapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                /*if (error instanceof NetworkError) {
                    Snackbar snackbar = Snackbar.make(recyclerView, "Network Error", Snackbar.LENGTH_INDEFINITE).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            checkDownloadUrl();
                        }
                    });
                    snackbar.show();
                }
            else */
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(swipe, "No Internet Connection", Snackbar.LENGTH_SHORT).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prepareDownloadData();
                        }
                    });
                    snackbar.show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getActivity(), "Oops ! We are experiencing some problem.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyManager.getInstance(getContext()).addToRequestQueue(stringRequest);

    }

    public String getSavedToken() {
        SharedPreferences sp = getContext().getSharedPreferences("UserCid", Context.MODE_PRIVATE);
        return sp.getString("cid", String.valueOf(sp));
    }
}