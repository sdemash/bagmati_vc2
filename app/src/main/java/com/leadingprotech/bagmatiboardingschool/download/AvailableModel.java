package com.leadingprotech.bagmatiboardingschool.download;

public class AvailableModel {
    String filename;
    private String path;

    public AvailableModel(){

    }
    public AvailableModel(String filename, String path){
        this.filename = filename;
        this.setPath(path);
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
