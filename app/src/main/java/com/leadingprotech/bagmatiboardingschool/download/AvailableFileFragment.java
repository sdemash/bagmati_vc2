package com.leadingprotech.bagmatiboardingschool.download;


import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.leadingprotech.bagmatiboardingschool.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AvailableFileFragment extends Fragment {
    protected List<AvailableModel> availableModelList = new ArrayList<>();
    protected AvailableAdapter availableAdapter;
    SwipeRefreshLayout swipe;
    RecyclerView recyclerView;
    private ArrayList<File> fileList = new ArrayList<>();


    public AvailableFileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_available_file, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_available);
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe_available);


        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(true);
                getfile();
            }
        });
    }

    private void prepareData() {

        availableModelList.clear();
        for (int i = fileList.size() - 1; i >= 0; i--) {
            String filename = fileList.get(i).getName();
            String path = fileList.get(i).getAbsolutePath();
            if (!filename.substring(filename.lastIndexOf(".")).equals(".lptfile")) {
                AvailableModel availableModel = new AvailableModel(filename, path);
                availableModelList.add(availableModel);
            }
        }
        swipe.setRefreshing(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        availableAdapter = new AvailableAdapter(availableModelList, getContext());
        recyclerView.setAdapter(availableAdapter);
    }


    private void permission_check() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 100);
            } else {
                getfile();
            }
        } else {
            getfile();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 100: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getfile();

                } else {
                    Toast.makeText(getActivity(), "Permission was not granted. :(", Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
                return;
            }
        }
    }

    public void getfile() {
        File root = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + getResources().getString(R.string.app_name) + " Downloads/");
        if (root.exists()) {
            fileList.clear();

            File[] listOfFiles = root.listFiles();

            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    fileList.add(listOfFiles[i]);
                } else if (listOfFiles[i].isDirectory()) {
                    System.out.println("Directory " + listOfFiles[i].getName());
                }
            }
            prepareData();
        } else {
            swipe.setRefreshing(false);

        }

    }

    @Override
    public void onResume() {
        super.onResume();

        permission_check();
    }

    public String getSavedOrg() {
        SharedPreferences sp = getContext().getSharedPreferences("Users", Context.MODE_PRIVATE);
        return sp.getString("name", null);
    }
}
