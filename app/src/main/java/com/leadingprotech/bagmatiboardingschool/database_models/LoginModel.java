package com.leadingprotech.bagmatiboardingschool.database_models;


public class LoginModel {


    private String full_name;

    private String image;
    private String api_token;

    private String class_name;

    public LoginModel(String api_token, String full_name, String image, String class_name) {
        this.api_token = api_token;
        this.full_name = full_name;
        this.image = image;
        this.class_name = class_name;
    }


    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getApi_token() {
        return api_token;
    }

    public void setApi_token(String api_token) {
        this.api_token = api_token;
    }


    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }


}
