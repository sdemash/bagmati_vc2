package com.leadingprotech.bagmatiboardingschool.database_models;


public class StudentDetailModel {


    private String full_name;
    private String dob_ad;
    private String dob_bs;
    private String contact;
    private String permanent_address;
    private String temporary_address;
    private String image;
    private String api_token;
    private String description;

    private String class_name;
    private String routine;

    public StudentDetailModel(String api_token, String full_name, String dob_ad, String dob_bs, String contact, String permanent_address, String temporary_address,
                              String image, String description, String class_name, String routine) {
        this.api_token = api_token;
        this.full_name = full_name;
        this.dob_ad = dob_ad;
        this.dob_bs = dob_bs;
        this.contact = contact;
        this.permanent_address = permanent_address;
        this.temporary_address = temporary_address;
        this.image = image;
        this.description = description;

        this.class_name = class_name;
        this.routine = routine;


    }


    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getDob_ad() {
        return dob_ad;
    }

    public void setDob_ad(String dob_ad) {
        this.dob_ad = dob_ad;
    }

    public String getDob_bs() {
        return dob_bs;
    }

    public void setDob_bs(String dob_bs) {
        this.dob_bs = dob_bs;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPermanent_address() {
        return permanent_address;
    }

    public void setPermanent_address(String permanent_address) {
        this.permanent_address = permanent_address;
    }

    public String getTemporary_address() {
        return temporary_address;
    }

    public void setTemporary_address(String temporary_address) {
        this.temporary_address = temporary_address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getApi_token() {
        return api_token;
    }

    public void setApi_token(String api_token) {
        this.api_token = api_token;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getRoutine() {
        return routine;
    }

    public void setRoutine(String routine) {
        this.routine = routine;
    }
}
