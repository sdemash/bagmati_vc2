package com.leadingprotech.bagmatiboardingschool.database_models;


public class ClientDetailModel {
    String cid, name, email, contact, logo, about, website, title_location, coordinate;
    public ClientDetailModel(){

    }
    public ClientDetailModel(String cid, String name, String email, String contact, String logo, String about, String website,
                             String title_location, String coordinate){
        this.cid = cid;
        this.name = name;
        this.email = email;
        this.contact = contact;
        this.logo = logo;
        this.about = about;
        this.website = website;
        this.title_location= title_location;
        this.coordinate = coordinate;

    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getTitle_location() {
        return title_location;
    }

    public void setTitle_location(String title_location) {
        this.title_location= title_location;
    }

    public String getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(String coordinate) {
        this.coordinate= coordinate;
    }
}
