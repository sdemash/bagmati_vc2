package com.leadingprotech.bagmatiboardingschool.helper;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.leadingprotech.bagmatiboardingschool.R;

/**
 * Created by Raiz on 7/25/2017.
 */

public class ParallaxPageTransformer implements ViewPager.PageTransformer {

    public void transformPage(View view, float position) {

        ImageView img = (ImageView) view.findViewById(R.id.img);

        int pageWidth = view.getWidth();


        if (position < -1) { // [-Infinity,-1)
            // This page is way off-screen to the left.
            view.setAlpha(1);

        } else if (position <= 1) { // [-1,1]

            img.setTranslationX(-position * (pageWidth / 2)); //Half the normal speed

        } else { // (1,+Infinity]
            // This page is way off-screen to the right.
            view.setAlpha(1);
        }


    }
}