package com.leadingprotech.bagmatiboardingschool.helper;


import android.content.Context;
import android.net.ConnectivityManager;

public class ConnectionManager {

    private Context context;
    public ConnectionManager(Context context){
        this.context = context;
    }
    public boolean isNetworkConnected(){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() !=null;
    }
}
