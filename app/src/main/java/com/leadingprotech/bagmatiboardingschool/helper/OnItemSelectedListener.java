package com.leadingprotech.bagmatiboardingschool.helper;

import android.view.View;

/**
 * Created by Raiz on 6/21/2017.
 */

public interface OnItemSelectedListener {

    public void onItemSelected(View v, int position);

}
