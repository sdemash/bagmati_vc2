package com.leadingprotech.bagmatiboardingschool.helper;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

public class CustomLinearLayout extends LinearLayoutManager {

    public CustomLinearLayout(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    @Override
    public boolean canScrollVertically() {
        return false;
    }

    @Override
    public boolean canScrollHorizontally() {
        return false;
    }
}