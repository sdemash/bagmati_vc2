package com.leadingprotech.bagmatiboardingschool.attendance;

/**
 * Created by Raiz on 4/25/2017.
 */

public class AttendanceModel {

    private String date, monthYear, attendenceStatus;

    public AttendanceModel(String date, String monthYear, String attendenceStatus) {
        this.date = date;
        this.monthYear = monthYear;
        this.attendenceStatus = attendenceStatus;
    }

    public String getMonthYear() {
        return monthYear;
    }

    public void setMonthYear(String monthYear) {
        this.monthYear = monthYear;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAttendenceStatus() {
        return attendenceStatus;
    }

    public void setAttendenceStatus(String attendenceStatus) {
        this.attendenceStatus = attendenceStatus;
    }
}
