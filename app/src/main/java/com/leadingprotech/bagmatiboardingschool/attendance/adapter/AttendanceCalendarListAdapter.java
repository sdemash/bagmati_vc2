package com.leadingprotech.bagmatiboardingschool.attendance.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.attendance.AttendanceModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class AttendanceCalendarListAdapter extends RecyclerView.Adapter<AttendanceCalendarListAdapter.MyViewHolder> {

    public static List<String> day_string;
    public GregorianCalendar pmonth;
    /**
     * calendar instance for previous month for getting complete view
     */
    public GregorianCalendar pmonthmaxset;
    public List<AttendanceModel> attendanceModels;
    int firstDay;
    int maxWeeknumber;
    int maxP;
    int calMaxP;
    int lastWeekDay;
    int leftDays;
    int mnthlength;
    String itemvalue, curentDateString, currentMonthSaturday;
    DateFormat df;
    private int Year, Month;
    private Context context;
    private Calendar month;
    private GregorianCalendar selectedDate;
    private ArrayList<String> items;
    private View previousView;
    private String date;

    public AttendanceCalendarListAdapter(Context context, GregorianCalendar monthCalendar, List<AttendanceModel> attendanceList) {
        this.attendanceModels = attendanceList;
        day_string = new ArrayList<String>();
        Locale.setDefault(Locale.US);
        month = monthCalendar;
        selectedDate = (GregorianCalendar) monthCalendar.clone();
        this.context = context;
        month.set(GregorianCalendar.DAY_OF_MONTH, 1);

        this.items = new ArrayList<String>();

        df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        curentDateString = df.format(selectedDate.getTime());

        refreshDays();
    }

    public void setItems(ArrayList<String> items) {
        for (int i = 0; i != items.size(); i++) {
            if (items.get(i).length() == 1) {
                items.set(i, "0" + items.get(i));
            }
        }
        this.items = items;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cal_item, parent, false);
        return new MyViewHolder(itemView);
    }

    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        String[] separatedTime = day_string.get(position).split("-");


        String gridvalue = separatedTime[2].replaceFirst("^0*", "");
        if ((Integer.parseInt(gridvalue) > 1) && (position < firstDay)) {
            holder.dayView.setTextColor(Color.parseColor("#cccccc"));
            holder.dayView.setClickable(false);
            holder.dayView.setFocusable(false);
        } else if ((Integer.parseInt(gridvalue) < 7) && (position > 28)) {
            holder.dayView.setTextColor(Color.parseColor("#cccccc"));
            holder.dayView.setClickable(false);
            holder.dayView.setFocusable(false);
        } else {
            // setting curent month's days in blue color.
            holder.dayView.setTextColor(Color.BLACK);
        }
        Calendar cal = new GregorianCalendar(Year, Month, 1);
        do {
            // get the day of the week for the current day
            int day = cal.get(Calendar.DAY_OF_WEEK);
            // check if it is a Saturday or Sunday
            if (day == Calendar.SATURDAY) {
                // print the day - but you could add them to a list or whatever
                //System.out.println(cal.get(Calendar.DAY_OF_MONTH));
                String saturday, month;
                if (cal.get(Calendar.DAY_OF_MONTH) < 10) {
                    saturday = "0" + cal.get(Calendar.DAY_OF_MONTH);
                } else {
                    saturday = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
                }
                if ((Month + 1) < 10) {
                    month = "0" + String.valueOf(Month + 1);
                } else {
                    month = String.valueOf(Month + 1);
                }

                currentMonthSaturday = String.valueOf(Year) + "-" + month + "-" + saturday;
                //Log.d("din", currentMonthSaturday);

                int len1 = day_string.size();
                if (len1 > position) {

                    if (day_string.get(position).equals(currentMonthSaturday)) {
                        holder.dayView.setTextColor(ContextCompat.getColor(context, R.color.profile_dash));
                    }
                }
            }
            // advance to the next day
            cal.add(Calendar.DAY_OF_YEAR, 1);
        } while (cal.get(Calendar.MONTH) == Month);

        if (day_string.get(position).equals(curentDateString)) {

            holder.dayView.setTextColor(Color.WHITE);
            holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        } else {
            holder.itemView.setBackgroundColor(Color.parseColor("#fffcfc"));
        }


        holder.dayView.setText(gridvalue);

        // create date string for comparison
        String date = day_string.get(position);

        if (date.length() == 1) {
            date = "0" + date;
        }
        String monthStr = "" + (month.get(GregorianCalendar.MONTH) + 1);
        if (monthStr.length() == 1) {
            monthStr = "0" + monthStr;
        }

        // show icon if date is not empty and it exists in the items array
        /*ImageView iw = (ImageView) v.findViewById(R.id.date_icon);
        if (date.length() > 0 && items != null && items.contains(date)) {
			iw.setVisibility(View.VISIBLE);
		} else {
			iw.setVisibility(View.GONE);
		}
		*/

        setEventView(holder.itemView, position, holder.dayView);
    }

    @Override
    public int getItemCount() {
        return day_string.size();
    }

    public Object getItem(int position) {
        return day_string.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public View setSelected(View view, int pos) {

        if (previousView != null) {
            previousView.setBackgroundColor(Color.parseColor("#fffcfc"));

        }

        view.setBackgroundColor(Color.parseColor("#30000000"));

        int len = day_string.size();
        if (len > pos) {
/*

            if (day_string.get(pos).equals(currentMonthSaturday)) {
            view.setBackgroundColor(Color.parseColor("#fffcfc"));

            }
*/

            if (day_string.get(pos).equals(curentDateString)) {
                view.setBackgroundColor(Color.parseColor("#FF311280"));
            } else {

                previousView = view;

            }

        }


        return view;
    }

    public void refreshDays() {
        // clear items
        items.clear();
        day_string.clear();
        Locale.setDefault(Locale.US);
        pmonth = (GregorianCalendar) month.clone();
        Year = month.get(Calendar.YEAR);
        Month = month.get(Calendar.MONTH);
        // month start day. ie; sun, mon, etc
        firstDay = month.get(GregorianCalendar.DAY_OF_WEEK);
        // finding number of weeks in current month.
        maxWeeknumber = month.getActualMaximum(GregorianCalendar.WEEK_OF_MONTH);
        // allocating maximum row number for the gridview.
        mnthlength = maxWeeknumber * 7;
        maxP = getMaxP(); // previous month maximum day 31,30....


        calMaxP = maxP - (firstDay - 1);// calendar offday starting 24,25 ...
        /**
         * Calendar instance for getting a complete gridview including the three
         * month's (previous,current,next) dates.
         */
        pmonthmaxset = (GregorianCalendar) pmonth.clone();
        /**
         * setting the start date as previous month's required date.
         */
        pmonthmaxset.set(GregorianCalendar.DAY_OF_MONTH, calMaxP + 1);
        /**
         * filling calendar gridview.
         */
        for (int n = 0; n < mnthlength; n++) {

            itemvalue = df.format(pmonthmaxset.getTime());
            pmonthmaxset.add(GregorianCalendar.DATE, 1);
            day_string.add(itemvalue);

        }
    }

    private int getMaxP() {
        int maxP;
        if (month.get(GregorianCalendar.MONTH) == month
                .getActualMinimum(GregorianCalendar.MONTH)) {
            pmonth.set((month.get(GregorianCalendar.YEAR) - 1),
                    month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            pmonth.set(GregorianCalendar.MONTH,
                    month.get(GregorianCalendar.MONTH) - 1);
        }
        maxP = pmonth.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);

        return maxP;
    }

    public void setEventView(View v, int pos, TextView txt) {

        int len = attendanceModels.size();
        for (int i = 0; i < len; i++) {
            AttendanceModel eventModel = attendanceModels.get(i);
            //   date = cal_obj.date;
            int len1 = day_string.size();
            if (len1 > pos) {

                if (day_string.get(pos).equals(eventModel.getDate())) {

//         			v.setBackgroundColor(Color.parseColor("#fffcfc"));
//                    v.setBackgroundResource(R.drawable.rounded_calender_item);
                    if (eventModel.getAttendenceStatus().equals("P")) {
                        txt.setTextColor(Color.parseColor("#00f000"));

                    } else {
                        txt.setTextColor(Color.parseColor("#f00000"));
                    }
                }
            }
        }


    }

    public void getPositionList(String date, final Activity act) {

        int len = attendanceModels.size();
        for (int i = 0; i < len; i++) {
            AttendanceModel eventModel = attendanceModels.get(i);
            // String event_date = cal_collection.date;

            //String event_message = cal_collection.event_message;

            /*if (date.equals(event_date)) {

                Toast.makeText(context, "You have event on this date: " + event_date, Toast.LENGTH_SHORT).show();
                new AlertDialog.Builder(context)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Date: " + event_date)
                        .setCancelable(true)
                        .setMessage("Event: " + event_message)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                act.finish();
                            }
                        }).show();
                break;
            } else {

               // Toast.makeText(context,date, Toast.LENGTH_LONG).show();
            }
        */
        }


    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView dayView;


        public MyViewHolder(View view) {
            super(view);
            dayView = (TextView) view.findViewById(R.id.date);
        }
    }

}
