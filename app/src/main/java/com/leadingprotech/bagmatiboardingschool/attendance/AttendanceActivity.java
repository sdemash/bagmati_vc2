package com.leadingprotech.bagmatiboardingschool.attendance;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.attendance.adapter.AttendanceCalendarListAdapter;
import com.leadingprotech.bagmatiboardingschool.attendance.adapter.AttendanceListAdapter;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.database_models.LoginModel;
import com.leadingprotech.bagmatiboardingschool.event.DividerItemDecoration;
import com.leadingprotech.bagmatiboardingschool.event.DividerItemDecorationGrid;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;
import com.leadingprotech.bagmatiboardingschool.helper.VolleyManager;
import com.leadingprotech.bagmatiboardingschool.navigation.Activity_Accounts;
import com.leadingprotech.bagmatiboardingschool.navigation.NavActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;

public class AttendanceActivity extends AppCompatActivity {
    public static final String SP_NAME_API_TOKEN = "apiToken";

    public GregorianCalendar cal_month, cal_month_copy;
    String monthYear;
    RelativeLayout header;
    ConnectionManager connectionManager;
    View view2, view3;
    LinearLayout StatsLayout;
    TableRow tableRow;
    List<AttendanceModel> attendanceModelList = new ArrayList<>();
    List<AttendanceModel> attendanceDateList = new ArrayList<>();
    nBulletinDatabase db;
    RecyclerView gridview;
    RecyclerView.ItemDecoration itemDecoration;
    SharedPreferences sharedPreferences;
    String apiToken;
    private AttendanceCalendarListAdapter cal_adapter;
    private TextView tv_month, presentStatTextV, absentStatTextV;
    private RecyclerView recyclerView_List;
    private AttendanceListAdapter list_adapter;
    private DividerItemDecoration eventItemDecoration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Attendance");
        }
        db = nBulletinDatabase.getInstance(getApplicationContext());
        connectionManager = new ConnectionManager(this);

        sharedPreferences = getSharedPreferences(SP_NAME_API_TOKEN, 0);
        apiToken = sharedPreferences.getString("apiToken", "");

        presentStatTextV = (TextView) findViewById(R.id.presentStat);
        absentStatTextV = (TextView) findViewById(R.id.absentStat);

        header = (RelativeLayout) findViewById(R.id.header);
//        view = (View) findViewById(R.id.View);
        view2 = (View) findViewById(R.id.View2);
        view3 = (View) findViewById(R.id.View3);
        StatsLayout = (LinearLayout) findViewById(R.id.stats);
        tableRow = (TableRow) findViewById(R.id.tableRow1);
        gridview = (RecyclerView) findViewById(R.id.gv_calendar);
        recyclerView_List = (RecyclerView) findViewById(R.id.lv_android);
        eventItemDecoration = new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL);
        itemDecoration = new DividerItemDecorationGrid(
                getResources().getDimensionPixelSize(R.dimen.photos_list_spacing),
                getResources().getInteger(R.integer.photo_list_preview_columns));


        attendanceDateList = db.getAllAttendance(apiToken);
        createUI();

        if ((connectionManager.isNetworkConnected() && attendanceDateList.size() == 0) || connectionManager.isNetworkConnected()) {

            prepareAttendance();

        }

        final GestureDetector mGestureDetector = new GestureDetector(getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });


        gridview.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());


                if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {

                    int position = recyclerView.getChildPosition(child);
                    ((AttendanceCalendarListAdapter) recyclerView.getAdapter()).setSelected(child, position);
                    String selectedGridDate = AttendanceCalendarListAdapter.day_string
                            .get(position);

                    String[] separatedTime = selectedGridDate.split("-");
                    String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                    int gridvalue = Integer.parseInt(gridvalueString);

                    if ((gridvalue > 10) && (position < 8)) {
                        setPreviousMonth();
                        refreshCalendar();
                    } else if ((gridvalue < 7) && (position > 28)) {
                        setNextMonth();
                        refreshCalendar();
                    }
                    ((AttendanceCalendarListAdapter) recyclerView.getAdapter()).setSelected(child, position);


                    ((AttendanceCalendarListAdapter) recyclerView.getAdapter()).getPositionList(selectedGridDate, AttendanceActivity.this);


                }

                return false;
            }


            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    public void createUI() {
        header.setVisibility(View.VISIBLE);
//        view.setVisibility(View.VISIBLE);
        view2.setVisibility(View.VISIBLE);
        view3.setVisibility(View.VISIBLE);
        StatsLayout.setVisibility(View.VISIBLE);

        tableRow.setVisibility(View.VISIBLE);
        gridview.setVisibility(View.VISIBLE);
        recyclerView_List.setVisibility(View.VISIBLE);
        cal_month = (GregorianCalendar) GregorianCalendar.getInstance();
        cal_month_copy = (GregorianCalendar) cal_month.clone();
        attendanceDateList = db.getAllAttendance(apiToken);
        cal_adapter = new AttendanceCalendarListAdapter(this, cal_month, attendanceDateList);


        tv_month = (TextView) findViewById(R.id.tv_month);
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
        monthYear = android.text.format.DateFormat.format("yyyy-MM", cal_month).toString();
        getAttendanceList();
        ImageButton previous = (ImageButton) findViewById(R.id.ib_prev);

        previous.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setPreviousMonth();
                refreshCalendar();
            }
        });

        ImageButton next = (ImageButton) findViewById(R.id.Ib_next);
        next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setNextMonth();
                refreshCalendar();

            }
        });

        gridview.removeItemDecoration(itemDecoration);
        gridview.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 7);
        gridview.setLayoutManager(layoutManager);
        gridview.addItemDecoration(itemDecoration);
        cal_adapter.notifyDataSetChanged();
        gridview.setAdapter(cal_adapter);


    }

    public void getAttendanceList() {
        attendanceModelList = db.getAttendance(monthYear, apiToken);
        List<String> stats = db.getAttendanceStats(monthYear, apiToken);
        if (Integer.parseInt(stats.get(0)) == 0 && Integer.parseInt(stats.get(1)) == 0) {
            StatsLayout.setVisibility(View.GONE);
        } else {
            StatsLayout.setVisibility(View.VISIBLE);
            presentStatTextV.setText(String.format(Locale.ENGLISH, "PRESENT DAYS \n%s", stats.get(0)));
            absentStatTextV.setText(String.format(Locale.ENGLISH, "ABSENT DAYS \n%s", stats.get(1)));

        }
        recyclerView_List.removeItemDecoration(eventItemDecoration);
        recyclerView_List.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView_List.setLayoutManager(layoutManager);
        recyclerView_List.addItemDecoration(eventItemDecoration);
        list_adapter = new AttendanceListAdapter(this, attendanceModelList);
        recyclerView_List.setAdapter(list_adapter);

    }

    protected void setNextMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1),
                    cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) + 1);
        }

    }

    protected void setPreviousMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMinimum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1),
                    cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) - 1);
        }

    }

    public void refreshCalendar() {
        cal_adapter.refreshDays();
        cal_adapter.notifyDataSetChanged();
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
        monthYear = android.text.format.DateFormat.format("yyyy-MM", cal_month).toString();
        list_adapter.notifyDataSetChanged();
        getAttendanceList();
    }

    public void prepareAttendance() {

        header.setVisibility(View.GONE);
        view2.setVisibility(View.GONE);
        view3.setVisibility(View.GONE);
        StatsLayout.setVisibility(View.GONE);
        tableRow.setVisibility(View.GONE);
        gridview.setVisibility(View.GONE);
        recyclerView_List.setVisibility(View.GONE);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar3);
        progressBar.setVisibility(View.VISIBLE);

        String URL_ATTENDANCE = BASE_URL + "student/attendance/" + android.text.format.DateFormat.format("yyyy", cal_month).toString() + "?api_token=" + apiToken;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_ATTENDANCE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println("ATTENDANCE " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("attendance");

                    if (jsonArray.length() > 0) {
                        db.deleteAttendance(apiToken);
                    }
                    for (int i = 0; i < jsonArray.length(); ++i) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        String date = object.getString("date");
                        String status = object.getString("status");
                        if (null != date && !date.equals("") && null != status) {
                            db.addAttendanceRecord(new AttendanceModel(date, date.substring(0, 7), status), apiToken);
                        }

                    }
                    progressBar.setVisibility(View.GONE);
                    createUI();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                createUI();
                if (error instanceof NoConnectionError) {

                    Snackbar snackbar = Snackbar.make(recyclerView_List, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    });
                    snackbar.show();
                } else if (error instanceof ServerError) {

                    Toast.makeText(AttendanceActivity.this, "Oops ! We are experiencing some problem.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        VolleyManager.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

    }


    @Override
    public void onBackPressed() {
        List<LoginModel> loginModel = db.getLoggedInAccounts();
        if (loginModel.size() == 0) {
            Intent intent = new Intent(AttendanceActivity.this, NavActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else {
            Intent intent = new Intent(AttendanceActivity.this, Activity_Accounts.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            List<LoginModel> loginModel = db.getLoggedInAccounts();
            if (loginModel.size() == 0) {
                Intent intent = new Intent(AttendanceActivity.this, NavActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                Intent intent = new Intent(AttendanceActivity.this, Activity_Accounts.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
            finish();
        }
        if (id == R.id.refresh_view) {
            if (connectionManager.isNetworkConnected()) {
                prepareAttendance();
            } else {
                createUI();
                Snackbar snackbar = Snackbar.make(recyclerView_List, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        prepareAttendance();
                    }
                });
                snackbar.show();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
