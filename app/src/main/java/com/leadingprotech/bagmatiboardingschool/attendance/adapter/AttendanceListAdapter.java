package com.leadingprotech.bagmatiboardingschool.attendance.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.attendance.AttendanceModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AttendanceListAdapter extends RecyclerView.Adapter<AttendanceListAdapter.MyViewHolder> {

    private final Context context;
    private List<AttendanceModel> attendanceModelList;


    public AttendanceListAdapter(Context context, List<AttendanceModel> attendanceList) {

        this.context = context;
        this.attendanceModelList = attendanceList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final AttendanceModel attendanceModel = attendanceModelList.get(position);
        holder.tv_event.setVisibility(View.GONE);

        if (position == 0) {
            holder.separator.setVisibility(View.INVISIBLE);
        } else {
            holder.separator.setVisibility(View.VISIBLE);
        }

        SimpleDateFormat monthDay = new SimpleDateFormat("MMM dd - EEEE");
        final SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        Date eventDay = new Date();
        try {
            eventDay = date.parse(attendanceModel.getDate());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.tv_date.setText(monthDay.format(eventDay));

    }

    @Override
    public int getItemCount() {
        return attendanceModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_date, tv_event;
        LinearLayout row;
        View separator;
        String allday;


        public MyViewHolder(View view) {
            super(view);
            separator = (View) view.findViewById(R.id.lineSeparator);
            row = (LinearLayout) view.findViewById(R.id.eventRowitem);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            tv_event = (TextView) view.findViewById(R.id.tv_event);

        }
    }


}
