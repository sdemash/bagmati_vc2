package com.leadingprotech.bagmatiboardingschool.database_classes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.leadingprotech.bagmatiboardingschool.assignments.AssignmentModel;
import com.leadingprotech.bagmatiboardingschool.attendance.AttendanceModel;
import com.leadingprotech.bagmatiboardingschool.contact.ContactModel;
import com.leadingprotech.bagmatiboardingschool.database_models.LoginModel;
import com.leadingprotech.bagmatiboardingschool.database_models.StudentDetailModel;
import com.leadingprotech.bagmatiboardingschool.event.util.EventModel;
import com.leadingprotech.bagmatiboardingschool.inbox.ChatModel;
import com.leadingprotech.bagmatiboardingschool.news.NewsModel;
import com.leadingprotech.bagmatiboardingschool.notice.NoticeModel;
import com.leadingprotech.bagmatiboardingschool.video_list.VideoListModel;

import java.util.ArrayList;
import java.util.List;

public class nBulletinDatabase extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 4;
    public static final String DATABASE_NAME = "nBulletin_Database";
    // NOTICE TABLE CREATION
    public static final String TABLE_NOTICE = "notice_Table";
    // NEWS TABLE CREATION
    public static final String TABLE_NEWS = "news_Table";
    //EVENT TALBE CREATION
    public static final String TABLE_EVENTS = "events_table";
    //contact table creation
    public static final String TABLE_CONTACT = "contact_Table";

    //video gallery table

    public static final String TABLE_VIDEO_GALLERY = "videoList";


    public static final String TABLE_ROUTES = "table_routes";
    public static final String TABLE_ROUTES_LOCATION = "table_routes_location";

    //attendance table

    public static final String TABLE_ATTENDANCE = "attendance_table";

    // table assignments
    public static final String TABLE_ASSIGNMENT = "assignment_table";


    // NOTICE ATTRIBUTES
    public static final String KEY_ID = "id";
    public static final String KEY_TITLE = "title";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_DATE_TIME = "date_time";
    public static final String KEY_STATUS = "status";

    //NEWS ATTRIBUTES
    public static final String KEY_ID_NEWS = "id";
    public static final String KEY_TITLE_NEWS = "title";
    public static final String KEY_MESSAGE_NEWS = "message";
    public static final String KEY_DATE_TIME_NEWS = "date_time";
    public static final String KEY_IMAGE_NEWS = "image";
    public static final String KEY_STATUS_NEWS = "status";


    //EVENT ATTRIBUTES//
    public static final String KEY_IDS = "id";
    public static final String KEY_ID_EVENT = "event_id";
    public static final String KEY_TITLE_EVENT = "title";
    public static final String KEY_DATE_EVENT = "date";
    public static final String KEY_MONTHYEAR_EVENT = "monthYear";
    public static final String KEY_START_TIME_EVENT = "start_time";
    public static final String KEY_END_TIME_EVENT = "end_time";
    public static final String KEY_ALL_DAY_EVENT = "all_day";
    public static final String KEY_BACKGROUND_COLOR_EVENT = "background";
    public static final String KEY_DESCRIPTION_EVENT = "description";
    //Contacts
    public static final String KEY_CONTACT_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_DESIGNATION = "designation";
    public static final String KEY_CONTACT = "contact";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_ORDER = "order_id";


    // client details table

    public static final String KEY_ROUTE_TITLE = "routeTitle";
    public static final String KEY_ROUTE_ID = "routeID";
    public static final String KEY_ROUTE_NAME = "routeName";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";

    // bus routes details table
    public static final String TABLE_LOGIN = "login";
    public static final String TABLE_STUDENT_DETAILS = "student_details";
    //column for login table
    public static final String KEY_API_TOKEN_LOGIN = "api_token"; /// primary key
    public static final String KEY_NAME_LOGIN = "fullname";
    public static final String KEY_CLASS_NAME_LOGIN = "class";
    public static final String KEY_IMAGE_LOGIN = "image";

    // table for logged in version
    // login table , student_detail table , inbox table, diary table
    //column for student_details table
    public static final String KEY_NAME_STUDENT = "fullname";
    public static final String KEY_DOB_AD_STUDENT = "dob_ad";
    public static final String KEY_DOB_BS_STUDENT = "dob_bs";
    public static final String KEY_API_TOKEN_STUDENT = "api_token"; // primary key
    public static final String KEY_IMAGE_STUDENT = "image";
    public static final String KEY_TEMP_ADDRESS_STUDENT = "temp_address";
    public static final String KEY_PERMANENT_STUDENT = "permanent_address";
    public static final String KEY_CONTACT_STUDENT = "contact";
    public static final String KEY_DESCRIPTION_STUDENT = "description";
    public static final String KEY_CLASS_NAME_STUDENT = "class";
    public static final String KEY_ROUTINE_STUDENT = "routine";
    public static final String TABLE_DIARY = "DiaryTable";
    public static final String KEY_NOTE_DIARY = "note";
    public static final String KEY_NOTE_DIARY_TITLE = "title";
    public static final String KEY_DATE_DIARY = "date";


    //slider table
    private static final String TABLE_SLIDER = "slider";
    private static final String KEY_IMAGE_SLIDER = "image";
    // table inbox
    private static final String TABLE_INBOX = "inbox";
    private static final String KEY_MSG_INBOX = "message";
    private static final String KEY_DATE_TIME_INBOX = "date_time";
    private static final String KEY_SENDER_IMAGE_INBOX = "sender_img";
    private static final String KEY_TYPE_INBOX = "type";
    private static final String KEY_ID_INBOX = "inbox_id";
    private static final String KEY_TYPE_MSG_INBOX = "type_msg";
    private static final String KEY_FROM_INBOX = "from_name";
    private static final String KEY_SENDER_INBOX = "sender_name";

    //Results Table
    private static final String TABLE_RESULTS = "results";
    private static final String KEY_RESULT_TITLE = "title";
    private static final String KEY_RESULT_DATE = "date";

    //Results Subjects Table
    private static final String TABLE_RESULT_SUBJECTS = "result_subjects";
    private static final String KEY_RESULT_ID = "result_id";
    private static final String KEY_SUBJECT_TITLE = "title";
    private static final String KEY_SUBJECT_TH_MARKS = "th_marks";
    private static final String KEY_SUBJECT_PR_MARKS = "pr_marks";
    private static final String KEY_SUBJECT_MARKS = "marks";
    private static final String KEY_SUBJECT_GRADE = "grade";
    private static final String KEY_SUBJECT_INDICATOR = "indicator";

    //attendance Table attributes
    private static final String KEY_ATTENDANCE_STATUS = "title";
    private static final String KEY_ATTENDANCE_DATE = "date";
    private static final String KEY_ATTENDANCE_MONTHYR = "month_year";

    //assignment Table attributes

    private static final String KEY_ASSIGNMENT_SUBJECT = "subject";
    private static final String KEY_ASSIGNMENT_TITLE = "title";
    private static final String KEY_ASSIGNMENT_DOCUMENT = "document";
    private static final String KEY_ASSIGNMENT_LINK = "link";
    private static final String KEY_ASSIGNMENT_REMARK = "remark";
    private static final String KEY_ASSIGNMENT_DUEDATE = "due_date";
    private static final String KEY_ASSIGNMENT_TEACHER = "teacher_name";
    private static final String KEY_ASSIGNMENT_DATE = "download_date";

    //video tables attributes
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_URL = "video_url";
    private static final String KEY_VIDEO_ID = "video_id";


    private static nBulletinDatabase mInstance = null;

    public nBulletinDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static nBulletinDatabase getInstance(Context ctx) {
        if (mInstance == null) {
            mInstance = new nBulletinDatabase(ctx.getApplicationContext());
        }
        return mInstance;
    }

    private void createLoginTable(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_LOGIN +
                "("
                + KEY_API_TOKEN_LOGIN + " TEXT PRIMARY KEY, "
                + KEY_NAME_LOGIN + " TEXT, "
                + KEY_IMAGE_LOGIN + " TEXT, "
                + KEY_CLASS_NAME_LOGIN + " TEXT"
                + ")";
        db.execSQL(CREATE_LOGIN_TABLE);
    }

    private void createResultTable(SQLiteDatabase db) {
        String CREATE_RESULT_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_RESULTS +
                "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_RESULT_TITLE + " TEXT, "
                + KEY_RESULT_DATE + " TEXT, "
                + KEY_API_TOKEN_STUDENT + " TEXT"
                + ")";
        db.execSQL(CREATE_RESULT_TABLE);
    }

    private void createResultSubjectTable(SQLiteDatabase db) {
        String CREATE_SUBJECT_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_RESULT_SUBJECTS +
                "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_RESULT_ID + " INTEGER, "
                + KEY_SUBJECT_TITLE + " TEXT, "
                + KEY_SUBJECT_TH_MARKS + " REAL, "
                + KEY_SUBJECT_PR_MARKS + " REAL, "
                + KEY_SUBJECT_MARKS + " REAL, "
                + KEY_SUBJECT_GRADE + " TEXT, "
                + KEY_SUBJECT_INDICATOR + " INTEGER"
                + ")";
        db.execSQL(CREATE_SUBJECT_TABLE);
    }

    private void createStudentTable(SQLiteDatabase db) {
        String CREATE_STUDENT_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_STUDENT_DETAILS +
                "("
                + KEY_API_TOKEN_STUDENT + " TEXT PRIMARY KEY, "
                + KEY_NAME_STUDENT + " TEXT, "
                + KEY_DOB_AD_STUDENT + " TEXT, "
                + KEY_DOB_BS_STUDENT + " TEXT, "
                + KEY_IMAGE_STUDENT + " TEXT, "
                + KEY_TEMP_ADDRESS_STUDENT + " TEXT, "
                + KEY_PERMANENT_STUDENT + " TEXT, "
                + KEY_CONTACT_STUDENT + " TEXT, "
                + KEY_DESCRIPTION_STUDENT + " TEXT, "
                + KEY_CLASS_NAME_STUDENT + " TEXT, "
                + KEY_ROUTINE_STUDENT + " TEXT"
                + ")";
        db.execSQL(CREATE_STUDENT_TABLE);
    }

    // for inbox messages
    private void createInboxTable(SQLiteDatabase db) {
        String CREATE_INBOX_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_INBOX +
                "("
                + KEY_ID_INBOX + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_API_TOKEN_STUDENT + " TEXT, "
                + KEY_MSG_INBOX + " TEXT, "
                + KEY_DATE_TIME_INBOX + " TEXT, "
                + KEY_SENDER_IMAGE_INBOX + " TEXT, "
                + KEY_FROM_INBOX + " TEXT, "
                + KEY_TYPE_MSG_INBOX + " INTEGER, "
                + KEY_SENDER_INBOX + " TEXT, "
                + KEY_TYPE_INBOX + " TEXT"
                + ")";
        db.execSQL(CREATE_INBOX_TABLE);
    }

    //for attendance table creation

    private void createAttendanceTable(SQLiteDatabase db) {
        String CREATE_ATTENDANCE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_ATTENDANCE +
                "("
                + KEY_API_TOKEN_STUDENT + " TEXT, "
                + KEY_ATTENDANCE_DATE + " TEXT, "
                + KEY_ATTENDANCE_MONTHYR + " TEXT, "

                + KEY_ATTENDANCE_STATUS + " TEXT"
                + ")";
        db.execSQL(CREATE_ATTENDANCE_TABLE);
    }


    //for Assignments table creation

    private void createAssignmentTable(SQLiteDatabase db) {
        String CREATE_ATTENDANCE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_ASSIGNMENT +
                "("
                + KEY_ID + " TEXT, "
                + KEY_API_TOKEN_STUDENT + " TEXT, "
                + KEY_ASSIGNMENT_TITLE + " TEXT, "
                + KEY_ASSIGNMENT_DOCUMENT + " TEXT, "
                + KEY_ASSIGNMENT_LINK + " TEXT, "

                + KEY_ASSIGNMENT_SUBJECT + " TEXT, "

                + KEY_ASSIGNMENT_REMARK + " TEXT, "
                + KEY_ASSIGNMENT_DUEDATE + " TEXT, "
                + KEY_ASSIGNMENT_TEACHER + " TEXT, "

                + KEY_ASSIGNMENT_DATE + " TEXT"
                + ")";
        db.execSQL(CREATE_ATTENDANCE_TABLE);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_NOTICE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NOTICE +
                "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_TITLE + " TEXT,"
                + KEY_MESSAGE + " TEXT,"
                + KEY_DATE_TIME + " TEXT,"
                + KEY_STATUS + " INTEGER" +
                ")";
        db.execSQL(CREATE_NOTICE_TABLE);

        String CREATE_NEWS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NEWS +
                "("
                + KEY_ID_NEWS + " INTEGER PRIMARY KEY,"
                + KEY_TITLE_NEWS + " TEXT,"
                + KEY_MESSAGE_NEWS + " TEXT,"
                + KEY_DATE_TIME_NEWS + " TEXT,"
                + KEY_IMAGE_NEWS + " TEXT,"
                + KEY_STATUS_NEWS + " INTEGER" +
                ")";
        db.execSQL(CREATE_NEWS_TABLE);

        String CREATE_EVENTS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_EVENTS +
                "("
                + KEY_IDS + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + KEY_ID_EVENT + " INTEGER,"
                + KEY_TITLE_EVENT + " TEXT,"
                + KEY_DATE_EVENT + " TEXT,"
                + KEY_MONTHYEAR_EVENT + " TEXT,"
                + KEY_START_TIME_EVENT + " TEXT,"
                + KEY_END_TIME_EVENT + " TEXT,"
                + KEY_ALL_DAY_EVENT + " INTEGER,"
                + KEY_BACKGROUND_COLOR_EVENT + " TEXT,"
                + KEY_DESCRIPTION_EVENT + " TEXT" +
                ")";
        db.execSQL(CREATE_EVENTS_TABLE);


        String CREATE_SLIDER = "CREATE TABLE IF NOT EXISTS " + TABLE_SLIDER +
                "(" + KEY_IMAGE_SLIDER + " TEXT" + ")";
        db.execSQL(CREATE_SLIDER);


        String CREATE_TABLE_ROUTES = "CREATE TABLE IF NOT EXISTS "
                + TABLE_ROUTES + "("
                + KEY_ID + " INTEGER PRIMARY KEY, "
                + KEY_ROUTE_TITLE + " TEXT" + ")";

        db.execSQL(CREATE_TABLE_ROUTES);

        String CREATE_TABLE_ROUTES_LOCATION = "CREATE TABLE IF NOT EXISTS "
                + TABLE_ROUTES_LOCATION + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_ROUTE_ID + " INTEGER, "
                + KEY_ROUTE_NAME + " TEXT, "
                + KEY_LATITUDE + " REAL, "
                + KEY_LONGITUDE + " REAL" + ")";
        db.execSQL(CREATE_TABLE_ROUTES_LOCATION);

        String CREATE_TABLE_DIARY = "CREATE TABLE IF NOT EXISTS "
                + TABLE_DIARY + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_API_TOKEN_LOGIN + " TEXT, "
                + KEY_NOTE_DIARY_TITLE + " TEXT, "
                + KEY_NOTE_DIARY + " TEXT, "
                + KEY_DATE_DIARY + " TEXT" + ")";
        db.execSQL(CREATE_TABLE_DIARY);


        createContact(db);
        createLoginTable(db);
        createStudentTable(db);
        createInboxTable(db);
        createResultTable(db);
        createResultSubjectTable(db);

        createAttendanceTable(db);

        createAssignmentTable(db);

        String CREATE_TABLE_VIDEO = "CREATE TABLE IF NOT EXISTS " + TABLE_VIDEO_GALLERY +
                "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_TITLE + " TEXT,"
                + KEY_DESCRIPTION + " TEXT,"
                + KEY_URL + " TEXT,"
                + KEY_IMAGE + " TEXT,"
                + KEY_VIDEO_ID + " TEXT,"
                + KEY_DATE_TIME + " TEXT" +
                ")";
        db.execSQL(CREATE_TABLE_VIDEO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("DATABASE UPDATED", "HELLO");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTICE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NEWS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SLIDER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROUTES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROUTES_LOCATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOGIN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STUDENT_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INBOX);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESULTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESULT_SUBJECTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ATTENDANCE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ASSIGNMENT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VIDEO_GALLERY);


        onCreate(db);
    }

    public boolean addNotice(NoticeModel noticeModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, noticeModel.getId());
        values.put(KEY_TITLE, noticeModel.getTitle());
        values.put(KEY_MESSAGE, noticeModel.getMessage());
        values.put(KEY_DATE_TIME, noticeModel.getDate_time());
        values.put(KEY_STATUS, noticeModel.getStatus());
        long result = db.insert(TABLE_NOTICE, null, values);

        return result != -1;
    }

    public int updateNotice(NoticeModel noticeModel, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, noticeModel.getTitle());
        values.put(KEY_MESSAGE, noticeModel.getMessage());
        values.put(KEY_DATE_TIME, noticeModel.getDate_time());
        /*values.put(KEY_STATUS, noticeModel.getStatus());*/

        String args[] = {id};

        return db.update(TABLE_NOTICE, values, KEY_ID + "=?", args);
    }


    public boolean addNews(NewsModel newsModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID_NEWS, newsModel.getId());
        values.put(KEY_TITLE_NEWS, newsModel.getTitle());
        values.put(KEY_MESSAGE_NEWS, newsModel.getDescription());
        values.put(KEY_DATE_TIME_NEWS, newsModel.getDate_time());
        values.put(KEY_IMAGE_NEWS, newsModel.getImage());
        values.put(KEY_STATUS_NEWS, newsModel.getStatus());
        long result = db.insert(TABLE_NEWS, null, values);

        return result != -1;
    }

    public int updateNews(NewsModel newsModel, String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TITLE_NEWS, newsModel.getTitle());
        values.put(KEY_MESSAGE_NEWS, newsModel.getDescription());
        values.put(KEY_DATE_TIME_NEWS, newsModel.getDate_time());
        values.put(KEY_IMAGE_NEWS, newsModel.getImage());
//        values.put(KEY_STATUS_NEWS, newsModel.getStatus());

        String args[] = {id};

        return db.update(TABLE_NEWS, values, KEY_ID_NEWS + "=?", args);
    }


    public List<NoticeModel> getNotice() {
        List<NoticeModel> noticeModelList = new ArrayList<NoticeModel>();
        String SELECT = "SELECT * FROM " + TABLE_NOTICE + " ORDER BY " + KEY_DATE_TIME + " DESC ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SELECT, null);
        if (cursor.moveToFirst()) {
            do {
                NoticeModel noticeModel = new NoticeModel();
                noticeModel.setId(cursor.getString(0));
                noticeModel.setTitle(cursor.getString(1));
                noticeModel.setMessage(cursor.getString(2));
                noticeModel.setDate_time(cursor.getString(3));
                noticeModel.setStatus(cursor.getString(4));
                noticeModelList.add(noticeModel);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return noticeModelList;
    }


    public List<NewsModel> getNews() {
        List<NewsModel> newsModelList = new ArrayList<NewsModel>();
        String SELECT = "SELECT * FROM " + TABLE_NEWS + " ORDER BY " + KEY_DATE_TIME_NEWS + " DESC ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SELECT, null);
        if (cursor.moveToFirst()) {
            do {
                NewsModel newsModel = new NewsModel();
                newsModel.setId(cursor.getString(0));
                newsModel.setTitle(cursor.getString(1));
                newsModel.setDescription(cursor.getString(2));
                newsModel.setDate_time(cursor.getString(3));
                newsModel.setImage(cursor.getString(4));
                newsModel.setStatus(cursor.getString(5));
                newsModelList.add(newsModel);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return newsModelList;
    }

    public void deleteANotice(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NOTICE, KEY_ID + " =?", new String[]{id});
    }

    public void deleteNotice() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_NOTICE);
    }

    public void deleteANews(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NEWS, KEY_ID_NEWS + " =?", new String[]{id});
    }

    public void deleteNews() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_NEWS);
    }


    public boolean updateNotice(String id, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_STATUS, status);
        db.update(TABLE_NOTICE, contentValues, KEY_ID + " =? ", new String[]{id});
        return true;
    }

    public boolean updateNews(String id, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_STATUS_NEWS, status);
        db.update(TABLE_NEWS, contentValues, KEY_ID_NEWS + " =? ", new String[]{id});
        return true;
    }


    private void createContact(SQLiteDatabase db) {
        String CREATE_CONTACT_TABLE = "CREATE TABLE " + TABLE_CONTACT +
                "("
                + KEY_CONTACT_ID + " INTEGER PRIMARY KEY, "
                + KEY_NAME + " TEXT, "
                + KEY_DESIGNATION + " TEXT, "
                + KEY_CONTACT + " TEXT, "
                + KEY_EMAIL + " TEXT, "
                + KEY_IMAGE + " TEXT, "
                + KEY_ORDER + " INTEGER"
                + ")";
        db.execSQL(CREATE_CONTACT_TABLE);
    }

    public boolean addContact(ContactModel contactModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, contactModel.getId());
        values.put(KEY_NAME, contactModel.getName());
        values.put(KEY_DESIGNATION, contactModel.getDesignation());
        values.put(KEY_CONTACT, contactModel.getContact());
        values.put(KEY_EMAIL, contactModel.getEmail());
        values.put(KEY_IMAGE, contactModel.getImage());
        values.put(KEY_ORDER, contactModel.getOrder());
        long result = db.insert(TABLE_CONTACT, null, values);

        return result != -1;
    }

    public List<ContactModel> getContacts() {
        List<ContactModel> contactModelList = new ArrayList<ContactModel>();
        String SELECT = "SELECT * FROM " + TABLE_CONTACT + " ORDER BY " + KEY_ORDER + " ASC ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SELECT, null);
        if (cursor.moveToFirst()) {
            do {
                ContactModel contactModel = new ContactModel();
                contactModel.setId(cursor.getString(0));
                contactModel.setName(cursor.getString(1));
                contactModel.setDesignation(cursor.getString(2));
                contactModel.setContact(cursor.getString(3));
                contactModel.setEmail(cursor.getString(4));
                contactModel.setImage(cursor.getString(5));
                contactModel.setOrder(cursor.getInt(6));
                contactModelList.add(contactModel);
            }
            while (cursor.moveToNext());
        }
        cursor.close();

        return contactModelList;
    }

    public void deleteContact() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_CONTACT);
    }

    public boolean addEvent(EventModel eventModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID_EVENT, eventModel.getEventId());
        values.put(KEY_TITLE_EVENT, eventModel.getTitle());
        values.put(KEY_DATE_EVENT, eventModel.getDate());
        values.put(KEY_MONTHYEAR_EVENT, eventModel.getMonthYear());
        values.put(KEY_START_TIME_EVENT, eventModel.getStart_time());
        values.put(KEY_END_TIME_EVENT, eventModel.getEnd_time());
        values.put(KEY_ALL_DAY_EVENT, eventModel.getIs_all_day());
        values.put(KEY_BACKGROUND_COLOR_EVENT, eventModel.getBackground_color());
        values.put(KEY_DESCRIPTION_EVENT, eventModel.getDescription());
        long result = db.insert(TABLE_EVENTS, null, values);

        return result != -1;
    }

    public List<EventModel> getEvents(String monthYear) {
        List<EventModel> eventModelList = new ArrayList<EventModel>();
        String SELECT = "SELECT * FROM " + TABLE_EVENTS + " WHERE " + KEY_MONTHYEAR_EVENT + " =?  ORDER BY " + KEY_DATE_EVENT + " ASC ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SELECT, new String[]{monthYear});
        if (cursor.moveToFirst()) {
            do {
                EventModel eventModel = new EventModel();
                eventModel.setId(cursor.getString(0));
                eventModel.setEventId(cursor.getInt(1));
                eventModel.setTitle(cursor.getString(2));
                eventModel.setDate(cursor.getString(3));
                eventModel.setMonthYear(cursor.getString(4));
                eventModel.setStart_time(cursor.getString(5));
                eventModel.setEnd_time(cursor.getString(6));
                eventModel.setIs_all_day(cursor.getInt(7));
                eventModel.setBackground_color(cursor.getString(8));
                eventModel.setDescription(cursor.getString(9));
                eventModelList.add(eventModel);
            }
            while (cursor.moveToNext());
        }
        cursor.close();

        return eventModelList;
    }

    public List<EventModel> getAllEvents() {
        List<EventModel> eventModelList = new ArrayList<EventModel>();
        String SELECT = "SELECT * FROM " + TABLE_EVENTS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SELECT, null);
        if (cursor.moveToFirst()) {
            do {
                EventModel eventModel = new EventModel();
                eventModel.setEventId(cursor.getInt(1));
                eventModel.setTitle(cursor.getString(2));
                eventModel.setDate(cursor.getString(3));
                eventModel.setMonthYear(cursor.getString(4));
                eventModel.setStart_time(cursor.getString(5));
                eventModel.setEnd_time(cursor.getString(6));
                eventModel.setIs_all_day(cursor.getInt(7));
                eventModel.setBackground_color(cursor.getString(8));
                eventModel.setDescription(cursor.getString(9));
                eventModelList.add(eventModel);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return eventModelList;
    }

    public String getEventToday(String date) {
        String eventsTitle = null;
        int i = 0;
        String SELECT = "SELECT * FROM " + TABLE_EVENTS + " WHERE " + KEY_DATE_EVENT + " =? ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SELECT, new String[]{date});
        if (cursor.moveToFirst()) {
            eventsTitle = cursor.getString(2);
            do {
                i++;
            }
            while (cursor.moveToNext());

        }
        if (i > 1) {
            eventsTitle = eventsTitle + " " + (i - 1) + " more";
        }
        cursor.close();

        return eventsTitle;
    }

    public void deleteEvent() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_EVENTS);
    }


    //slider images table

    public boolean addSliderImages(String imageLink) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_IMAGE_SLIDER, imageLink);

        long result = db.insert(TABLE_SLIDER, null, values);

        return result != -1;
    }

    public ArrayList<String> getSliderImages() {
        ArrayList<String> sliderImages = new ArrayList<String>();
        String SELECT_IMAGES = "SELECT " + KEY_IMAGE_SLIDER + " FROM " + TABLE_SLIDER;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SELECT_IMAGES, null);
        if (cursor.moveToFirst()) {
            do {
                sliderImages.add(cursor.getString(cursor.getColumnIndex(KEY_IMAGE_SLIDER)));
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return sliderImages;
    }

    public void deleteSliderImages() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_SLIDER);
    }

    // bus routes table works

    public long insertRouteData(String route_title, int route_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_ROUTE_TITLE, route_title);
        contentValues.put(KEY_ID, route_id);


        return db.insert(TABLE_ROUTES, null, contentValues);
    }

    public Cursor getAllRouteData() {
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery("select * from " + TABLE_ROUTES, null);
    }

    public int deleteAllRouteData() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_ROUTES, null, null);
    }

    public boolean insertRouteLocationData(int route_id, String location, double latitude, double longitude) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_ROUTE_ID, route_id);
        contentValues.put(KEY_ROUTE_NAME, location);
        contentValues.put(KEY_LATITUDE, latitude);
        contentValues.put(KEY_LONGITUDE, longitude);

        long result = db.insert(TABLE_ROUTES_LOCATION, null, contentValues);
        return result != -1;
    }

    public Cursor getAllRouteLocationData(int route_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery("select * from " + TABLE_ROUTES_LOCATION + " WHERE " + KEY_ROUTE_ID + " =?", new String[]{String.valueOf(route_id)});
    }

    public int deleteARouteLocationData(int route_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_ROUTES_LOCATION, KEY_ROUTE_ID + " =?", new String[]{String.valueOf(route_id)});
    }

    public int deleteAllRouteLocationData() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_ROUTES_LOCATION, null, null);
    }

    /// Student Details methods

    public boolean addStudent(StudentDetailModel student) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_NAME_STUDENT, student.getFull_name());
        values.put(KEY_DOB_AD_STUDENT, student.getDob_ad());
        values.put(KEY_DOB_BS_STUDENT, student.getDob_bs());
        values.put(KEY_API_TOKEN_STUDENT, student.getApi_token());
        values.put(KEY_IMAGE_STUDENT, student.getImage());
        values.put(KEY_TEMP_ADDRESS_STUDENT, student.getTemporary_address());
        values.put(KEY_PERMANENT_STUDENT, student.getPermanent_address());
        values.put(KEY_CONTACT_STUDENT, student.getContact());
        values.put(KEY_DESCRIPTION_STUDENT, student.getDescription());
        values.put(KEY_CLASS_NAME_STUDENT, student.getClass_name());
        values.put(KEY_ROUTINE_STUDENT, student.getRoutine());

        long result = db.insert(TABLE_STUDENT_DETAILS, null, values);

        return result != -1;
    }

    public void updateStudent(StudentDetailModel student) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_NAME_STUDENT, student.getFull_name());
        values.put(KEY_DOB_AD_STUDENT, student.getDob_ad());
        values.put(KEY_DOB_BS_STUDENT, student.getDob_bs());
        values.put(KEY_IMAGE_STUDENT, student.getImage());
        values.put(KEY_TEMP_ADDRESS_STUDENT, student.getTemporary_address());
        values.put(KEY_PERMANENT_STUDENT, student.getPermanent_address());
        values.put(KEY_CONTACT_STUDENT, student.getContact());
        values.put(KEY_DESCRIPTION_STUDENT, student.getDescription());
        values.put(KEY_CLASS_NAME_STUDENT, student.getClass_name());
        values.put(KEY_ROUTINE_STUDENT, student.getRoutine());

        db.update(TABLE_STUDENT_DETAILS, values, KEY_API_TOKEN_STUDENT + " =?", new String[]{student.getApi_token()});

    }


    public boolean addAccount(LoginModel student) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_NAME_LOGIN, student.getFull_name());
        values.put(KEY_API_TOKEN_LOGIN, student.getApi_token());
        values.put(KEY_IMAGE_LOGIN, student.getImage());
        values.put(KEY_CLASS_NAME_LOGIN, student.getClass_name());

        long result = db.insert(TABLE_LOGIN, null, values);

        return result != -1;
    }

    public int deleteLoginAccount(String apiToken) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_LOGIN, KEY_API_TOKEN_LOGIN + " =?", new String[]{String.valueOf(apiToken)});
    }

    public List<LoginModel> getLoggedInAccounts() {
        List<LoginModel> loginModels = new ArrayList<LoginModel>();
        String SELECT = "SELECT * FROM " + TABLE_LOGIN;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SELECT, null);
        if (cursor.moveToFirst()) {
            do {
                LoginModel account = new LoginModel(cursor.getString(cursor.getColumnIndex(KEY_API_TOKEN_LOGIN)), cursor.getString(cursor.getColumnIndex(KEY_NAME_LOGIN)), cursor.getString(cursor.getColumnIndex(KEY_IMAGE_LOGIN)), cursor.getString(cursor.getColumnIndex(KEY_CLASS_NAME_LOGIN)));

                loginModels.add(account);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return loginModels;
    }

    public StudentDetailModel getStudentDetails(String apiToken) {
        String SELECT = "SELECT * FROM " + TABLE_STUDENT_DETAILS + " WHERE " + KEY_API_TOKEN_STUDENT + " =?";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SELECT, new String[]{apiToken});
        if (cursor.moveToFirst()) {
            StudentDetailModel account = new StudentDetailModel(cursor.getString(cursor.getColumnIndex(KEY_API_TOKEN_STUDENT)),
                    cursor.getString(cursor.getColumnIndex(KEY_NAME_STUDENT)),
                    cursor.getString(cursor.getColumnIndex(KEY_DOB_AD_STUDENT)),
                    cursor.getString(cursor.getColumnIndex(KEY_DOB_BS_STUDENT)),
                    cursor.getString(cursor.getColumnIndex(KEY_CONTACT_STUDENT)),
                    cursor.getString(cursor.getColumnIndex(KEY_PERMANENT_STUDENT)),
                    cursor.getString(cursor.getColumnIndex(KEY_TEMP_ADDRESS_STUDENT)),
                    cursor.getString(cursor.getColumnIndex(KEY_IMAGE_STUDENT)),
                    cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION_STUDENT)),
                    cursor.getString(cursor.getColumnIndex(KEY_CLASS_NAME_STUDENT)),
                    cursor.getString(cursor.getColumnIndex(KEY_ROUTINE_STUDENT)));

            cursor.close();
            return account;

        }
        return null;

    }

    public boolean addDiary(String apiToken, String title, String note, String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_API_TOKEN_LOGIN, apiToken);
        values.put(KEY_NOTE_DIARY_TITLE, title);
        values.put(KEY_NOTE_DIARY, note);
        values.put(KEY_DATE_DIARY, date);
        long result = db.insert(TABLE_DIARY, null, values);

        return result != -1;
    }

    public boolean updateDiary(int id, String apiToken, String title, String note, String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_API_TOKEN_LOGIN, apiToken);
        values.put(KEY_NOTE_DIARY_TITLE, title);
        values.put(KEY_NOTE_DIARY, note);
        values.put(KEY_DATE_DIARY, date);

        long resultt = db.update(TABLE_DIARY, values, KEY_ID + " =?", new String[]{String.valueOf(id)});

        return resultt != -1;
    }

    public Cursor getADiaryData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery("select * from " + TABLE_DIARY + " WHERE " + KEY_ID + " =?", new String[]{String.valueOf(id)});
    }

    public void deleteADiaryData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("delete from " + TABLE_DIARY + " WHERE " + KEY_ID + " =?", new String[]{String.valueOf(id)});
    }

    public Cursor getAllDiaryData(String apiToken) {
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery("select * from " + TABLE_DIARY + " WHERE " + KEY_API_TOKEN_LOGIN + " =?", new String[]{apiToken});
    }

    // inbox methods

    public boolean addInboxMessage(ChatModel message) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        values.put(KEY_API_TOKEN_LOGIN, message.getApi_token());
        values.put(KEY_MSG_INBOX, message.getMsg());
        values.put(KEY_DATE_TIME_INBOX, message.getDatetime());
        values.put(KEY_SENDER_IMAGE_INBOX, message.getImage());
        values.put(KEY_TYPE_INBOX, message.getType());
        values.put(KEY_TYPE_MSG_INBOX, message.getTYPE());
        values.put(KEY_FROM_INBOX, message.getFrom());
        values.put(KEY_SENDER_INBOX, message.getSender_name());


        long result = db.insert(TABLE_INBOX, null, values);

        return result != -1;
    }

    public List<ChatModel> getInboxMessages(int TYPE, String api_key) { // true for personal
        List<ChatModel> messages = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();

        String selectQuery;

        Cursor cursor = null;

        switch (TYPE) {

            case 1:
                selectQuery = "SELECT  * FROM " + TABLE_INBOX + " WHERE " + KEY_TYPE_MSG_INBOX + " =? AND " + KEY_TYPE_INBOX + " =? AND " + KEY_API_TOKEN_LOGIN + " =?";
                cursor = db.rawQuery(selectQuery, new String[]{"1", "0", api_key});

                break;

            case 0:
                selectQuery = "SELECT  * FROM " + TABLE_INBOX + " WHERE " + KEY_TYPE_MSG_INBOX + " =? AND " + KEY_TYPE_INBOX + " =? AND " + KEY_API_TOKEN_LOGIN + " =?";

                cursor = db.rawQuery(selectQuery, new String[]{"0", "0", api_key});

                break;


        }

        if (null != cursor && cursor.moveToFirst()) {

            do {
                ChatModel msg = new ChatModel(cursor.getString(cursor.getColumnIndex(KEY_API_TOKEN_LOGIN)), cursor.getInt(cursor.getColumnIndex(KEY_TYPE_INBOX)), cursor.getString(cursor.getColumnIndex(KEY_MSG_INBOX)), cursor.getString(cursor.getColumnIndex(KEY_DATE_TIME_INBOX)), cursor.getString(cursor.getColumnIndex(KEY_SENDER_IMAGE_INBOX)), cursor.getString(cursor.getColumnIndex(KEY_FROM_INBOX)), cursor.getString(cursor.getColumnIndex(KEY_SENDER_INBOX)), cursor.getInt(cursor.getColumnIndex(KEY_TYPE_MSG_INBOX)));
                messages.add(msg);

            } while (cursor.moveToNext());
            cursor.close();

            return messages;

        }

        return null;

    }

    public void deleteInboxMsgs(String apiToken, int type) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_INBOX, KEY_API_TOKEN_LOGIN + " =? AND " + KEY_TYPE_MSG_INBOX + " =?", new String[]{apiToken, String.valueOf(type)});
    }

    // results methods

    public long addResult(String title, String date, String apiToken) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_RESULT_TITLE, title);
        values.put(KEY_RESULT_DATE, date);
        values.put(KEY_API_TOKEN_STUDENT, apiToken);

        return db.insert(TABLE_RESULTS, null, values);
    }

    public Cursor getAllResultsData(String apiToken) {
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery("select * from " + TABLE_RESULTS + " WHERE "
                + KEY_API_TOKEN_STUDENT + " =?", new String[]{apiToken});
    }

    public void deleteAllResultsData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_RESULTS, null, null);
    }

    // results subjects methods

    public long addResultSubjects(int resultId, String subject, Double thMarks, Double prMarks, Double marks, String grade, int indicator) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_RESULT_ID, resultId);
        values.put(KEY_SUBJECT_TITLE, subject);
        values.put(KEY_SUBJECT_TH_MARKS, thMarks);
        values.put(KEY_SUBJECT_PR_MARKS, prMarks);
        values.put(KEY_SUBJECT_MARKS, marks);
        values.put(KEY_SUBJECT_GRADE, grade);
        values.put(KEY_SUBJECT_INDICATOR, indicator);

        return db.insert(TABLE_RESULT_SUBJECTS, null, values);
    }

    public Cursor getAllResultSubjectsData(int resultId) {
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery("select * from " + TABLE_RESULT_SUBJECTS + " WHERE "
                + KEY_RESULT_ID + " =?", new String[]{String.valueOf(resultId)});
    }

    public void deleteAllResultSubjectsData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_RESULT_SUBJECTS, null, null);
    }


    ////////////////attendance methods

    public boolean addAttendanceRecord(AttendanceModel attendanceModel, String apiToken) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ATTENDANCE_DATE, attendanceModel.getDate());
        values.put(KEY_ATTENDANCE_MONTHYR, attendanceModel.getMonthYear());
        values.put(KEY_ATTENDANCE_STATUS, attendanceModel.getAttendenceStatus());
        values.put(KEY_API_TOKEN_STUDENT, apiToken);

        long result = db.insert(TABLE_ATTENDANCE, null, values);
        return result != -1;
    }

    public List<AttendanceModel> getAttendance(String monthYear, String apiToken) {
        List<AttendanceModel> attendanceModels = new ArrayList<AttendanceModel>();
        String SELECT = "SELECT * FROM " + TABLE_ATTENDANCE + " WHERE " + KEY_ATTENDANCE_MONTHYR + " =? AND " + KEY_ATTENDANCE_STATUS + " =\"A\" AND " + KEY_API_TOKEN_STUDENT + " =? ORDER BY " + KEY_ATTENDANCE_DATE + " DESC ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SELECT, new String[]{monthYear, apiToken});
        if (cursor.moveToFirst()) {
            do {
                AttendanceModel attendanceModel = new AttendanceModel(cursor.getString(cursor.getColumnIndex(KEY_ATTENDANCE_DATE)), cursor.getString(cursor.getColumnIndex(KEY_ATTENDANCE_MONTHYR)), cursor.getString(cursor.getColumnIndex(KEY_ATTENDANCE_STATUS)));
                attendanceModels.add(attendanceModel);
            }
            while (cursor.moveToNext());
        }
        cursor.close();

        return attendanceModels;
    }

    public List<String> getAttendanceStats(String monthYear, String apiToken) {
        List<AttendanceModel> attendanceModels = new ArrayList<AttendanceModel>();
        List<String> stats = new ArrayList<>();
        int present = 0, absent = 0;
        String SELECT = "SELECT * FROM " + TABLE_ATTENDANCE + " WHERE " + KEY_ATTENDANCE_MONTHYR + " =? AND " + KEY_API_TOKEN_STUDENT + " =?";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SELECT, new String[]{monthYear, apiToken});
        if (cursor.moveToFirst()) {
            do {
                if (cursor.getString(cursor.getColumnIndex(KEY_ATTENDANCE_STATUS)).equals("P")) {
                    present++;
                }

            }
            while (cursor.moveToNext());
            absent = cursor.getCount() - present;
        }
        stats.add(0, String.valueOf(present));
        stats.add(1, String.valueOf(absent));
        cursor.close();

        return stats;
    }

    public List<AttendanceModel> getAllAttendance(String apiToken) {
        List<AttendanceModel> attendanceModels = new ArrayList<AttendanceModel>();
        String SELECT = "SELECT * FROM " + TABLE_ATTENDANCE + " WHERE " + KEY_API_TOKEN_STUDENT + " =?";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SELECT, new String[]{apiToken});
        if (cursor.moveToFirst()) {
            do {
                AttendanceModel attendanceModel = new AttendanceModel(cursor.getString(cursor.getColumnIndex(KEY_ATTENDANCE_DATE)), cursor.getString(cursor.getColumnIndex(KEY_ATTENDANCE_MONTHYR)), cursor.getString(cursor.getColumnIndex(KEY_ATTENDANCE_STATUS)));
                attendanceModels.add(attendanceModel);
            }
            while (cursor.moveToNext());
        }
        cursor.close();

        return attendanceModels;
    }

    public void deleteAttendance(String apiToken) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ATTENDANCE, KEY_API_TOKEN_STUDENT + " =?", new String[]{apiToken});
    }

    //////////////////////// ASSIGNMENT METHODS

    public boolean addAssignmentRecord(AssignmentModel assignmentModel, String apiToken) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, assignmentModel.getId());
        values.put(KEY_ASSIGNMENT_TITLE, assignmentModel.getTitle());
        values.put(KEY_ASSIGNMENT_DOCUMENT, assignmentModel.getDocument());
        values.put(KEY_ASSIGNMENT_LINK, assignmentModel.getLink());

        values.put(KEY_ASSIGNMENT_DUEDATE, assignmentModel.getDue_date());
        values.put(KEY_ASSIGNMENT_REMARK, assignmentModel.getRemark());
        values.put(KEY_ASSIGNMENT_SUBJECT, assignmentModel.getSubject());
        values.put(KEY_ASSIGNMENT_TEACHER, assignmentModel.getBy_teacher());
        values.put(KEY_ASSIGNMENT_DATE, assignmentModel.getGiven_date());

        values.put(KEY_API_TOKEN_STUDENT, apiToken);


        long result = db.insert(TABLE_ASSIGNMENT, null, values);

        if (result == -1) {
            result = db.update(TABLE_ASSIGNMENT, values, KEY_API_TOKEN_STUDENT + "=? AND " + KEY_ID + "=?", new String[]{apiToken, assignmentModel.getId()});
        }
        return result != -1;
    }


    public List<AssignmentModel> getAllAssignments(String apiToken) {
        List<AssignmentModel> assignmentModels = new ArrayList<AssignmentModel>();
        String SELECT = "SELECT * FROM " + TABLE_ASSIGNMENT + " WHERE " + KEY_API_TOKEN_STUDENT + " =?";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SELECT, new String[]{apiToken});
        if (cursor.moveToFirst()) {
            do {
                AssignmentModel assignment = new AssignmentModel(cursor.getString(cursor.getColumnIndex(KEY_ID)), cursor.getString(cursor.getColumnIndex(KEY_ASSIGNMENT_SUBJECT)), cursor.getString(cursor.getColumnIndex(KEY_ASSIGNMENT_TITLE)), cursor.getString(cursor.getColumnIndex(KEY_ASSIGNMENT_DOCUMENT)), cursor.getString(cursor.getColumnIndex(KEY_ASSIGNMENT_LINK)), cursor.getString(cursor.getColumnIndex(KEY_ASSIGNMENT_REMARK)), cursor.getString(cursor.getColumnIndex(KEY_ASSIGNMENT_DUEDATE)), cursor.getString(cursor.getColumnIndex(KEY_ASSIGNMENT_DATE)), cursor.getString(cursor.getColumnIndex(KEY_ASSIGNMENT_TEACHER)));
                assignmentModels.add(assignment);
            }
            while (cursor.moveToNext());
        }
        cursor.close();

        return assignmentModels;
    }

    public void deleteAssignments(String apiToken) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ASSIGNMENT, KEY_API_TOKEN_STUDENT + " =?", new String[]{apiToken});
    }

    ////////////////////video methods

    public boolean addVideos(VideoListModel videoListModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, videoListModel.getId());
        values.put(KEY_TITLE, videoListModel.getTitle());
        values.put(KEY_VIDEO_ID, videoListModel.getVideo_id());
        values.put(KEY_DESCRIPTION, videoListModel.getDescription());
        values.put(KEY_IMAGE, videoListModel.getImage());
        values.put(KEY_URL, videoListModel.getUrl());
        values.put(KEY_DATE_TIME, videoListModel.getUpdated_at());

        long result = db.insert(TABLE_VIDEO_GALLERY, null, values);

        if (result == -1) {
            result = db.update(TABLE_VIDEO_GALLERY, values, KEY_ID + " =?", new String[]{String.valueOf(videoListModel.getId())});
        }
        return result != -1;
    }

    public ArrayList<VideoListModel> getVideosList() {
        ArrayList<VideoListModel> videoListModels = new ArrayList<VideoListModel>();
        String SELECT = "SELECT * FROM " + TABLE_VIDEO_GALLERY + " ORDER BY " + KEY_DATE_TIME + " DESC ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SELECT, null);
        if (cursor.moveToFirst()) {
            do {
                VideoListModel videoListModel = new VideoListModel(cursor.getInt(cursor.getColumnIndex(KEY_ID)),
                        cursor.getString(cursor.getColumnIndex(KEY_TITLE)),
                        cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION)),
                        cursor.getString(cursor.getColumnIndex(KEY_URL)),
                        cursor.getString(cursor.getColumnIndex(KEY_IMAGE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VIDEO_ID)),
                        cursor.getString(cursor.getColumnIndex(KEY_DATE_TIME)));
                videoListModels.add(videoListModel);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return videoListModels;
    }

    public void deleteAllVideo() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_VIDEO_GALLERY, null, null);
    }
}
