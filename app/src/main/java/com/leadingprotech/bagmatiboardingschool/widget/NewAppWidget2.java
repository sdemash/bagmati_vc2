package com.leadingprotech.bagmatiboardingschool.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.event.EventActivity;
import com.leadingprotech.bagmatiboardingschool.navigation.NavActivity;
import com.leadingprotech.bagmatiboardingschool.news.NewsActivity;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class NewAppWidget2 extends AppWidgetProvider {

    nBulletinDatabase db;
    private String todayMonth, today, day, dateNotice, dateNews, dateEvents,
            dateTodayEvent;


    public static void pushWidgetUpdate(Context context, RemoteViews remoteViews) {
        ComponentName myWidget = new ComponentName(context, NewAppWidget2.class);
        AppWidgetManager manager = AppWidgetManager.getInstance(context);
        manager.updateAppWidget(myWidget, remoteViews);

    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        db = nBulletinDatabase.getInstance(context);
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.new_app_widget2);

        Intent intent2 = new Intent(context, NavActivity.class);
        PendingIntent pendingIntent2 = PendingIntent.getActivity(context, 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.noticeRowWidget, pendingIntent2);

        Intent intent3 = new Intent(context, NewsActivity.class);
        PendingIntent pendingIntent3 = PendingIntent.getActivity(context, 0, intent3, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.newsRowWidget, pendingIntent3);

        Intent intent4 = new Intent(context, EventActivity.class);
        PendingIntent pendingIntent4 = PendingIntent.getActivity(context, 0, intent4, 0);
        remoteViews.setOnClickPendingIntent(R.id.eventsTitleWidget, pendingIntent4);

        getDate();

        remoteViews.setTextViewText(R.id.dateTvWidget, todayMonth + "\n" + day);
        remoteViews.setTextViewText(R.id.dateTv2Widget, today);


        remoteViews.setImageViewResource(R.id.widgetIcon, R.drawable.institution_logo);


        if (db.getNotice().size() != 0) {
            remoteViews.setTextViewText(R.id.noticeTitleWidget, db.getNotice().get(0).getTitle());
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = df.parse(db.getNotice().get(0).getDate_time());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);
            String monthString = new DateFormatSymbols().getShortMonths()[month];
            dateNotice = monthString + " " + day;
            remoteViews.setTextViewText(R.id.noticeDateWidget, dateNotice);
        } else {
            remoteViews.setTextViewText(R.id.noticeTitleWidget, "No notices.");

        }
        if (db.getNews().size() != 0) {
            remoteViews.setTextViewText(R.id.newsTitleWidget, db.getNews().get(0).getTitle());
            SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
            Date date2 = null;
            try {
                date2 = df2.parse(db.getNews().get(0).getDate_time());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar cal2 = Calendar.getInstance();

            cal2.setTime(date2);
            int month2 = cal2.get(Calendar.MONTH);
            int day2 = cal2.get(Calendar.DAY_OF_MONTH);
            String monthString2 = new DateFormatSymbols().getShortMonths()[month2];
            dateNews = monthString2 + " " + day2;
            remoteViews.setTextViewText(R.id.newsDateWidget, dateNews);
        } else {
            remoteViews.setTextViewText(R.id.newsTitleWidget, "No news.");

        }
        if (db.getEventToday(dateEvents) != null) {
            remoteViews.setTextViewText(R.id.eventsTitleWidget, db.getEventToday(dateEvents));
            remoteViews.setTextViewText(R.id.eventsDateWidget, dateTodayEvent);

        } else {
            remoteViews.setTextViewText(R.id.eventsTitleWidget, "No events.");

        }


        pushWidgetUpdate(context, remoteViews);
    }


    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        // When the user deletes the widget, delete the preference associated with it.

    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    public void getDate() {

        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        todayMonth = String.valueOf(new SimpleDateFormat("MMM", Locale.ENGLISH).format(date.getTime()));
        today = String.valueOf(new SimpleDateFormat("dd", Locale.ENGLISH).format(date.getTime()));
        day = String.valueOf(new SimpleDateFormat("EE", Locale.ENGLISH).format(date.getTime()));
        dateEvents = String.valueOf(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(date.getTime()));
        dateTodayEvent = String.valueOf(new SimpleDateFormat("MMM dd", Locale.ENGLISH).format(date.getTime()));

    }


}

