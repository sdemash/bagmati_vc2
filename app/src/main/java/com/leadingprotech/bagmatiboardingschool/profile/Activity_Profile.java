package com.leadingprotech.bagmatiboardingschool.profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.SplashScreen;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.database_models.StudentDetailModel;

import org.json.JSONException;
import org.json.JSONObject;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;

public class Activity_Profile extends AppCompatActivity {

    public static final String SP_NAME_API_TOKEN = "apiToken";
    SharedPreferences sharedPreferences;
    nBulletinDatabase db;
    TextView name, class_name, contact, address, dob;
    ImageView photo;
    RelativeLayout profile;
    String apiToken;
    RequestManager mRequestManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Student Profile");
        }

        mRequestManager = Glide.with(this);

        profile = (RelativeLayout) findViewById(R.id.profile_layout);
        name = (TextView) findViewById(R.id.username_profile);
        class_name = (TextView) findViewById(R.id.class_profile);
        contact = (TextView) findViewById(R.id.contact_profile);
        address = (TextView) findViewById(R.id.address_profile);
        dob = (TextView) findViewById(R.id.dob_profile);
        photo = (ImageView) findViewById(R.id.user_profile_photo);

        sharedPreferences = getSharedPreferences(SP_NAME_API_TOKEN, 0);
        apiToken = sharedPreferences.getString("apiToken", "");
        db = nBulletinDatabase.getInstance(getApplicationContext());

        loadUserProfile();


        ImageView imgBtn_logOut = (ImageView) findViewById(R.id.img_logout);
        imgBtn_logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!apiToken.equals("")) {
                    db.deleteLoginAccount(apiToken);
                    SharedPreferences.Editor spEditor_LoggedIn = sharedPreferences.edit();
                    spEditor_LoggedIn.putString("apiToken", "");
                    spEditor_LoggedIn.apply();
                    Intent intent = new Intent(Activity_Profile.this, SplashScreen.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    finishAffinity();

                }
            }
        });

    }

    private void loadUserProfile() {

        StudentDetailModel student = db.getStudentDetails(apiToken);
        if (null != student) {
            String student_name = student.getFull_name();
            String student_class = getString(R.string.class_profile, student.getClass_name());
            String student_address = getString(R.string.address_profile, student.getPermanent_address());
            String student_contact = getString(R.string.contact_profile, student.getContact());
            String student_dob = getString(R.string.dob_profile, student.getDob_bs()) + " / " + student.getDob_ad();

            name.setText(student_name);
            class_name.setText(student_class);
            address.setText(student_address);
            contact.setText(student_contact);
            dob.setText(student_dob);

            mRequestManager.load(student.getImage()).placeholder(R.drawable.no_thumb).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(photo);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
        } else if (id == R.id.refresh_view) {
            downloadData();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh_menu, menu);
        return true;
    }


    public void downloadData() {

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String URL_FEED = BASE_URL + "student/show?api_token=" + apiToken;
        JsonObjectRequest obreq = new JsonObjectRequest(Request.Method.GET, URL_FEED, null, new
                Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("profile log", response.toString());
                            final String error = response.getString("error");
                            if (error.equals("true")) {

                                Snackbar snackbar = Snackbar
                                        .make(profile, "Error Updating profile data.", Snackbar.LENGTH_LONG);
                                snackbar.show();

                            } else {
                                JSONObject obj = response.getJSONObject("student");

                                String fullname = obj.getString("full_name");
                                String dob_ad = obj.getString("dob_ad");
                                String dob_bs = obj.getString("dob_bs");
                                String contact = obj.getString("contact");
                                String permanent_address = obj.getString("permanent_address");
                                String temporary_address = obj.getString("temporary_address");
                                String image = obj.getString("image");
                                String api_token = obj.getString("api_token");
                                String description = obj.getString("description");

                                JSONObject obj1 = response.getJSONObject("class");
                                String classname = obj1.getString("name");
                                String routine = obj1.getString("routine");

                                db.updateStudent(new StudentDetailModel(api_token, fullname, dob_ad, dob_bs, contact, permanent_address,
                                        temporary_address, image, description, classname, routine));

                                loadUserProfile();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                String errorText = "Unknown Error.";
                if (error instanceof TimeoutError) {
                    errorText = "Connection timed out.";
                }
                if (error instanceof NoConnectionError) {
                    errorText = "No Internet Connection.";
                }
                if (error instanceof AuthFailureError) {
                    errorText = "Wrong Email or Password.";
                }
                if (error instanceof com.android.volley.NetworkError) {
                    errorText = "Unable to connect to the Internet.";
                }
                if (error instanceof com.android.volley.ServerError) {

                    errorText = "Unable to connect to the Server.";
                }
                if (error instanceof com.android.volley.ParseError) {
                    errorText = "Unable to fetch data from the server.";
                }

                Snackbar snackbar = Snackbar
                        .make(profile, errorText, Snackbar.LENGTH_LONG)
                        .setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                downloadData();
                            }
                        });

                View sbView = snackbar.getView();
                sbView.setBackgroundColor(Color.parseColor("#f42f27"));
                snackbar.setActionTextColor(Color.WHITE);
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbar.show();
            }
        });

        obreq.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        queue.add(obreq);
    }
}


