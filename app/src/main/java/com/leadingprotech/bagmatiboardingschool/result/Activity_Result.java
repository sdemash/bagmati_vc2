package com.leadingprotech.bagmatiboardingschool.result;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;

import java.util.ArrayList;
import java.util.List;

public class Activity_Result extends AppCompatActivity {

    public static final String SP_NAME_API_TOKEN = "apiToken";
    nBulletinDatabase database;
    RecyclerView recyclerView;
    List<ResultsModel> resultsModelList = new ArrayList<>();
    ResultsAdapater resultsAdapater;
    SharedPreferences sharedPreferences;
    String apiToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Results");
        }
        database = new nBulletinDatabase(this);

        sharedPreferences = getSharedPreferences(SP_NAME_API_TOKEN, 0);
        apiToken = sharedPreferences.getString("apiToken", "");

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_result);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        database.deleteAllResultsData();
        database.deleteAllResultSubjectsData();

        long id = database.addResult("First Terminal Examination", "2017-03-04", apiToken);
        if (id != -1) {
            database.addResultSubjects((int) id, "Physics", 69.0, 25.0, 94.0, "A+", 1);
            database.addResultSubjects((int) id, "Chemistry", 53.0, 23.0, 76.0, "B", 1);
            database.addResultSubjects((int) id, "Mathematics", 73.0, 25.0, 98.0, "A+", 1);
            database.addResultSubjects((int) id, "Nepali", 37.0, 21.0, 58.0, "D", 0);
        }
        id = database.addResult("Second Terminal Examination", "2017-02-21", apiToken);
        if (id != -1) {
            database.addResultSubjects((int) id, "Physics", 53.0, 23.0, 76.0, "B", 1);
            database.addResultSubjects((int) id, "Chemistry", 69.0, 25.0, 94.0, "A+", 1);
            database.addResultSubjects((int) id, "Mathematics", 37.0, 21.0, 58.0, "D", 0);
            database.addResultSubjects((int) id, "Nepali", 73.0, 25.0, 98.0, "A+", 1);
        }
        id = database.addResult("Final Terminal Examination", "2017-04-18", apiToken);
        if (id != -1) {
            database.addResultSubjects((int) id, "Physics", 37.0, 21.0, 58.0, "D", 0);
            database.addResultSubjects((int) id, "Chemistry", 53.0, 23.0, 76.0, "B", 1);
            database.addResultSubjects((int) id, "Mathematics", 73.0, 25.0, 98.0, "A+", 1);
            database.addResultSubjects((int) id, "Nepali", 69.0, 25.0, 94.0, "A+", 1);
        }

        populateRecyclerView();
    }

    public void populateRecyclerView() {
        resultsModelList.clear();

        Cursor cursor = database.getAllResultsData(apiToken);
        if (cursor.getCount() == 0) {

        } else {
            cursor.moveToFirst();
            do {
                int id = cursor.getInt(0);
                String title = cursor.getString(1);
                String date = cursor.getString(2);

                ResultsModel resultsModel = new ResultsModel(id, date, title, apiToken);
                resultsModelList.add(resultsModel);
            } while (cursor.moveToNext());
        }

        runOnUiThread(new Runnable() {
            public void run() {
                resultsAdapater = new ResultsAdapater(resultsModelList, Activity_Result.this);
                recyclerView.setAdapter(resultsAdapater);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
