package com.leadingprotech.bagmatiboardingschool.result;

public class SubjectsModel {

    int id, resultId, indicatorColor;
    String title, grade;
    double theory, practical, marks;

    public SubjectsModel(int id, int resultId, int indicatorColor, String title, String grade, double theory, double practical, double marks) {
        this.id = id;
        this.resultId = resultId;
        this.indicatorColor = indicatorColor;
        this.title = title;
        this.grade = grade;
        this.theory = theory;
        this.practical = practical;
        this.marks = marks;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getResultId() {
        return resultId;
    }

    public void setResultId(int resultId) {
        this.resultId = resultId;
    }

    public int getIndicatorColor() {
        return indicatorColor;
    }

    public void setIndicatorColor(int indicatorColor) {
        this.indicatorColor = indicatorColor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public double getTheory() {
        return theory;
    }

    public void setTheory(double theory) {
        this.theory = theory;
    }

    public double getPractical() {
        return practical;
    }

    public void setPractical(double practical) {
        this.practical = practical;
    }

    public double getMarks() {
        return marks;
    }

    public void setMarks(double marks) {
        this.marks = marks;
    }
}
