package com.leadingprotech.bagmatiboardingschool.result;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.helper.MonthName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class ResultsAdapater extends RecyclerView.Adapter<ResultsAdapater.MyViewHolder> {

    private nBulletinDatabase database;
    private List<ResultsModel> resultsModelList;
    private Context context;


    public ResultsAdapater(List<ResultsModel> resultsModelList, Context context) {
        this.context = context;
        this.resultsModelList = resultsModelList;
        database = new nBulletinDatabase(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_results, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final ResultsModel resultsModel = resultsModelList.get(position);

        holder.title.setText(resultsModel.getTitle());

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date_date = null;
        try {
            date_date = format.parse(resultsModel.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date_date);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        final String date = MonthName.getMonthName(month + 1, 0) + " " + day + " " + year;

        holder.date.setText(date);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Activity_ResultView.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra("result_title", resultsModel.getTitle());
                intent.putExtra("result_id", resultsModel.getResultId());
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return resultsModelList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title, date;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tv_result_title);
            date = (TextView) itemView.findViewById(R.id.tv_result_date);
            cardView = (CardView) itemView.findViewById(R.id.cardView_results);
        }

    }
}
