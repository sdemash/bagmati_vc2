package com.leadingprotech.bagmatiboardingschool.result;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;

import java.util.List;


public class ResultSubjectsAdapater extends RecyclerView.Adapter<ResultSubjectsAdapater.MyViewHolder> {

    private nBulletinDatabase database;
    private List<SubjectsModel> subjectsModelList;
    private Context context;


    public ResultSubjectsAdapater(List<SubjectsModel> subjectsModelList, Context context) {
        this.context = context;
        this.subjectsModelList = subjectsModelList;
        database = new nBulletinDatabase(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_subject_results, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final SubjectsModel subjectsModel = subjectsModelList.get(position);

        holder.title.setText(subjectsModel.getTitle());
        holder.theory.setText(String.valueOf(subjectsModel.getTheory()));
        holder.practical.setText(String.valueOf(subjectsModel.getPractical()));
        holder.marks.setText(String.valueOf(subjectsModel.getMarks()));
        holder.grade.setText(subjectsModel.getGrade());

        if (subjectsModel.getIndicatorColor() == 0) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#ff2222"));
        } else if (subjectsModel.getIndicatorColor() == 1) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#4CAF50"));
        }
    }

    @Override
    public int getItemCount() {
        return subjectsModelList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title, theory, practical, marks, grade;
        RelativeLayout relativeLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tv_result_subjectName);
            theory = (TextView) itemView.findViewById(R.id.tv_result_thMarks);
            practical = (TextView) itemView.findViewById(R.id.tv_result_prMarks);
            marks = (TextView) itemView.findViewById(R.id.tv_result_Marks);
            grade = (TextView) itemView.findViewById(R.id.tv_result_Grade);

            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relativeLayout_resindicator);
        }

    }
}
