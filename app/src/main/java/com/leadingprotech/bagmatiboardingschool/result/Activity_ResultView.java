package com.leadingprotech.bagmatiboardingschool.result;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;

import java.util.ArrayList;
import java.util.List;

public class Activity_ResultView extends AppCompatActivity {

    public static final String SP_NAME_API_TOKEN = "apiToken";
    RecyclerView recyclerView;
    List<SubjectsModel> subjectsModelList = new ArrayList<>();
    ResultSubjectsAdapater subjectsAdapater;
    nBulletinDatabase database;
    SharedPreferences sharedPreferences;
    String apiToken;

    int resultId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_view);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        String resultTitle = "";

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            resultId = extras.getInt("result_id");
            resultTitle = extras.getString("result_title");
        } else {
            finish();
        }
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setTitle(resultTitle + " Result");
        }
        database = new nBulletinDatabase(this);

        sharedPreferences = getSharedPreferences(SP_NAME_API_TOKEN, 0);
        apiToken = sharedPreferences.getString("apiToken", "");

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_Subjects);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        populateRecyclerView();
    }

    public void populateRecyclerView() {
        subjectsModelList.clear();

        Cursor cursor = database.getAllResultSubjectsData(resultId);
        if (cursor.getCount() == 0) {

        } else {
            cursor.moveToFirst();
            do {
                int id = cursor.getInt(0);
                String title = cursor.getString(2);
                Double th_marks = cursor.getDouble(3);
                Double pr_marks = cursor.getDouble(4);
                Double marks = cursor.getDouble(5);
                String grade = cursor.getString(6);
                int indicator = cursor.getInt(7);
                SubjectsModel subjectsModel = new SubjectsModel(id, resultId, indicator, title, grade, th_marks, pr_marks, marks);
                subjectsModelList.add(subjectsModel);
            } while (cursor.moveToNext());
        }

        runOnUiThread(new Runnable() {
            public void run() {
                subjectsAdapater = new ResultSubjectsAdapater(subjectsModelList, Activity_ResultView.this);
                recyclerView.setAdapter(subjectsAdapater);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
