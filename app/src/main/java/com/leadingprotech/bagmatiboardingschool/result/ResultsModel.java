package com.leadingprotech.bagmatiboardingschool.result;


public class ResultsModel {

    int resultId;
    String date, title, apkToken;

    public ResultsModel(int resultId, String date, String title, String apkToken) {
        this.resultId = resultId;
        this.date = date;
        this.title = title;
        this.apkToken = apkToken;
    }

    public int getResultId() {
        return resultId;
    }

    public void setResultId(int resultId) {
        this.resultId = resultId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getApkToken() {
        return apkToken;
    }

    public void setApkToken(String apkToken) {
        this.apkToken = apkToken;
    }
}
