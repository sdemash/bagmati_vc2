package com.leadingprotech.bagmatiboardingschool.navigation_drawer;

import android.graphics.drawable.Drawable;

/**
 * Created by Raiz on 6/20/2017.
 */


public class DrawerItem {
    private Drawable icon;
    private String title;

    public DrawerItem(Drawable icon, String title) {
        this.icon = icon;
        this.title = title;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
