package com.leadingprotech.bagmatiboardingschool.navigation_drawer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.helper.OnItemSelectedListener;

import java.util.ArrayList;

/**
 * Created by Raiz on 6/20/2017.
 */

public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.DrawerViewHolder> {
    private ArrayList<DrawerItem> drawerMenuList;

    private OnItemSelectedListener mListener;
    Context context;

    public DrawerAdapter(ArrayList<DrawerItem> drawerMenuList, Context context) {
        this.context = context;
        this.drawerMenuList = drawerMenuList;
    }

    @Override
    public DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_drawer_item, parent, false);
        return new DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DrawerViewHolder holder, int position) {

        /*if(position==0){
            holder.title.setBackground(ContextCompat.getDrawable(context,R.drawable.nav_drawer_item_bg_top));
        }*/
        holder.title.setText(drawerMenuList.get(position).getTitle());
        holder.icon.setImageDrawable(drawerMenuList.get(position).getIcon());
    }

    @Override
    public int getItemCount() {
        return drawerMenuList.size();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView icon;

        public DrawerViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            icon = (ImageView) itemView.findViewById(R.id.icon);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemSelected(view, getAdapterPosition());
                }
            });

        }
    }

    public void setOnItemClickLister(OnItemSelectedListener mListener) {
        this.mListener = mListener;
    }
}