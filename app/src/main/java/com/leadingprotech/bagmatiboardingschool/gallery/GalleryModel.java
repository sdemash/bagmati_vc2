package com.leadingprotech.bagmatiboardingschool.gallery;


public class GalleryModel {
    private int id;
    private String updated_at;
    private String cover_image;
    private String thumb;

    public GalleryModel() {
    }

    public GalleryModel(int id, String updated_at, String cover_image, String thumb) {
        this.id = id;
        this.updated_at = updated_at;
        this.cover_image = cover_image;
        this.thumb = thumb;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCover_image() {
        return cover_image;
    }

    public void setCover_image(String cover_image) {
        this.cover_image = cover_image;
    }
}
