package com.leadingprotech.bagmatiboardingschool.gallery;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;
import com.github.chrisbanes.photoview.PhotoView;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.helper.HackyViewPager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class SlideshowDailogFragment extends DialogFragment {
    ProgressDialog pd;
    File file;
    File directory;
    FileOutputStream out;
    InputStream in;
    HttpURLConnection con;
    int fileLength;
    int total = 0;


    TextView counter;
    String img_url;
    int id;
    ImageView download;
    private String TAG = SlideshowDailogFragment.class.getSimpleName();
    private ArrayList<GalleryModel> images= new ArrayList<>();
    //  page change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            displayMetaInfo(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };
    private HackyViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private int selectedPosition = 0;

    static SlideshowDailogFragment newInstance() {
        SlideshowDailogFragment f = new SlideshowDailogFragment();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_image_slider, container, false);
        viewPager = (HackyViewPager) v.findViewById(R.id.viewpager);
        counter = (TextView) v.findViewById(R.id.count_img);
        download = (ImageView) v.findViewById(R.id.img_downloader);
        images = (ArrayList<GalleryModel>) getArguments().getSerializable("images");
        selectedPosition = getArguments().getInt("position");


        directory = new File(Environment.getExternalStorageDirectory() + "/Downloads/");


        pd = new ProgressDialog(getContext());
        pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pd.setIndeterminate(false);
        pd.setCancelable(false);
        pd.setMax(100);
        pd.setTitle("Please wait...");
        pd.setMessage("Downloading file");

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        setCurrentItem(selectedPosition);


        return v;
    }

    private void setCurrentItem(int position) {
        viewPager.setCurrentItem(position, false);
        displayMetaInfo(selectedPosition);
    }

    private void displayMetaInfo(int position) {
        GalleryModel galleryModel = images.get(position);
        counter.setText((position + 1) + " of " + images.size());
        img_url = images.get(position).getCover_image();
        id = images.get(position).getId();
        file = new File(Environment.getExternalStorageDirectory() + "/Downloads/" + id + ".jpg");
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DownloadImage();
            }
        });

        //lblCount.setText((position + 1) + " of " + images.size());
    }

    @Override
    public void onPause() {
        super.onPause();
        getArguments().remove("position");
        getArguments().remove("images");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }

    private void DownloadImage() {

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                pd.dismiss();
                Toast.makeText(getActivity(), "Image has been downloaded", Toast.LENGTH_SHORT).show();

            }
        };
        final Handler progressHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                pd.setProgress((total * 100) / fileLength);
            }
        };
        if (!file.exists()) {
            pd.show();
            Thread downloadThread = new Thread() {
                public void run() {
                    try {
                        URL url = new URL(img_url);
                        con = (HttpURLConnection) url.openConnection();
                        con.setRequestMethod("GET");
                        con.setDoInput(true);
                        con.connect();
                        in = con.getInputStream();
                        fileLength = con.getContentLength();
                        if (!directory.exists()) {
                            directory.mkdirs();
                        }

                        out = new FileOutputStream(file);
                        byte[] buffer = new byte[1024];
                        int len1 = 0;
                        while ((len1 = in.read(buffer)) > 0) {
                            out.write(buffer, 0, len1);
                            total += len1;
                            progressHandler.sendEmptyMessage(0);
                        }
                        handler.sendEmptyMessage(0);
                        total = 0;
                        in.close();
                        out.close();

                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    }
                }
            };
            downloadThread.start();
        } else {
            Toast.makeText(getActivity(), "File Already Exists", Toast.LENGTH_SHORT).show();
        }
    }

    //  adapter
    public class MyViewPagerAdapter extends PagerAdapter {

        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.image_fullscreen_preview, container, false);

            PhotoView imageViewPreview = (PhotoView) view.findViewById(R.id.image_preview);

            GalleryModel image = images.get(position);

            Target<Bitmap> bitmap = Glide.with(getActivity()).load(image.getCover_image())
                    .asBitmap()
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageViewPreview);


            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return images.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == ((View) obj);
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);

        }
    }

}
