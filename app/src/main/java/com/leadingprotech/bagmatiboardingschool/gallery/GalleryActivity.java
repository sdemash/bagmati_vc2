package com.leadingprotech.bagmatiboardingschool.gallery;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;
import com.leadingprotech.bagmatiboardingschool.helper.VolleyManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.leadingprotech.bagmatiboardingschool.helper.Config.API_TOKEN;
import static com.leadingprotech.bagmatiboardingschool.helper.Config.BASE_URL;

public class GalleryActivity extends AppCompatActivity {
    ConnectionManager connectionManager;
    LinearLayout no_data;
    RelativeLayout root;
    ProgressBar progress;
    RecyclerView recyclerView;
    String URL_1 = BASE_URL + "";
    String id;
    nBulletinDatabase db;
    private ArrayList<GalleryModel> images;
    private GalleryAdapter galleryAdapter;

    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 180);
        return noOfColumns;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        db = nBulletinDatabase.getInstance(getApplicationContext());

        String title_holder = getIntent().getStringExtra("title");
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(title_holder);
        }

        connectionManager = new ConnectionManager(GalleryActivity.this);
        images = new ArrayList<>();
        no_data = (LinearLayout) findViewById(R.id.gallery_no);
        root = (RelativeLayout) findViewById(R.id.rootLayout);

        id = getIntent().getStringExtra("id");
        recyclerView = (RecyclerView) findViewById(R.id.recycler_gallery);
        int mNoOfColumns = calculateNoOfColumns(getApplicationContext());
        progress = (ProgressBar) findViewById(R.id.progress_gallery);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        if (connectionManager.isNetworkConnected()) {
            prepareGallery();
        } else {
            Snackbar snackbar = Snackbar.make(root, "No Internet Connection", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    prepareGallery();
                }
            });
            snackbar.show();
        }
    }

    private void prepareGallery() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_1 + "gallery/" + id + "?api_key=" + API_TOKEN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progress.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            JSONObject object = new JSONObject(response);
                            Log.e("Gallery", String.valueOf(response));
                            JSONArray jsonArray = object.getJSONArray("gallery");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject json = jsonArray.getJSONObject(i);
                                int id = json.getInt("id");
                                String updated_at = json.getString("updated_at");
                                String cover_image = json.getString("image");
                                String thumb = json.getString("image_thumb");
                                images.add(new GalleryModel(id, updated_at, cover_image, thumb));
                            }
                            if (images.isEmpty()) {
                                recyclerView.setVisibility(View.GONE);
                                progress.setVisibility(View.GONE);
                                no_data.setVisibility(View.VISIBLE);

                            } else {
                                galleryAdapter = new GalleryAdapter(images, getApplicationContext());
                                recyclerView.setAdapter(galleryAdapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        VolleyManager.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
        recyclerView.addOnItemTouchListener(new GalleryAdapter.RecyclerTouchListener(getApplicationContext(), recyclerView, new GalleryAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", images);
                bundle.putInt("position", position);

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                SlideshowDailogFragment newFragment = SlideshowDailogFragment.newInstance();
                newFragment.setArguments(bundle);
                newFragment.show(ft, "slideshow");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
           super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}
