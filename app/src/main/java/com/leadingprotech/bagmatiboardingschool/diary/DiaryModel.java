package com.leadingprotech.bagmatiboardingschool.diary;

/**
 * Created by karsk on 09/03/2017.
 */

public class DiaryModel {

    int id;
    String apiToken, title, content, date;

    public DiaryModel(int id, String apiToken, String title, String content, String date) {
        this.id = id;
        this.apiToken = apiToken;
        this.title = title;
        this.content = content;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
