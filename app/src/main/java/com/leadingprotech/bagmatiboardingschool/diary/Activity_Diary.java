package com.leadingprotech.bagmatiboardingschool.diary;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Activity_Diary extends AppCompatActivity {

    public static final String SP_NAME_API_TOKEN = "apiToken";
    SharedPreferences sharedPreferences;

    nBulletinDatabase db;
    RecyclerView recyclerView;

    List<DiaryModel> diaryModelList = new ArrayList<>();
    DiaryAdapter diaryAdapter;

    String apiToken;

    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diary);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Diary");
        }
        db = new nBulletinDatabase(this);

        sharedPreferences = getSharedPreferences(SP_NAME_API_TOKEN, 0);
        apiToken = sharedPreferences.getString("apiToken", "");

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_diary);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        populateRecyclerView();

        fab = (FloatingActionButton) findViewById(R.id.fab_diary_add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Activity_Diary.this, Activity_DiaryView.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });
    }

    public void populateRecyclerView() {
        diaryModelList.clear();

        Cursor cursor = db.getAllDiaryData(apiToken);
        TextView textView_noentries = (TextView) findViewById(R.id.tv_diary_null);
        if (cursor.getCount() == 0) {
            textView_noentries.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            return;
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            textView_noentries.setVisibility(View.GONE);
        }
        try {
            cursor.moveToFirst();
            do {
                int diary_id = cursor.getInt(0);
                String apiToken = cursor.getString(1);
                String title = cursor.getString(2);
                String content = cursor.getString(3);
                String date = cursor.getString(4);

                Date updated_date = new Date();
                updated_date.setTime(Long.parseLong(date));

                SimpleDateFormat displayFormat = new SimpleDateFormat("MMMM dd yyyy, HH:mm:ss");

                String formattedDate = displayFormat.format(updated_date);

                DiaryModel diaryModel = new DiaryModel(diary_id, apiToken, title, content, formattedDate);

                diaryModelList.add(diaryModel);
            }
            while (cursor.moveToNext());
        } finally {
            cursor.close();
        }
        runOnUiThread(new Runnable() {
            public void run() {
                diaryAdapter = new DiaryAdapter(diaryModelList, Activity_Diary.this);
                recyclerView.setAdapter(diaryAdapter);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        populateRecyclerView();
    }
}
