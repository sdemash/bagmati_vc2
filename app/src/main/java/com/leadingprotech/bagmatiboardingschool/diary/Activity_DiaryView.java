package com.leadingprotech.bagmatiboardingschool.diary;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Activity_DiaryView extends AppCompatActivity {
    public static final String SP_NAME_API_TOKEN = "apiToken";
    SharedPreferences sharedPreferences;
    EditText editText_text, editText_title;
    TextView updatedDate;
    CardView diary;
    nBulletinDatabase db;
    String title = "", text = "", date = "NA", apiToken;

    int diary_id;

    boolean neww = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diary_view);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        Intent intentExtra = getIntent(); // <-- returns null
        if (intentExtra.getExtras() != null) {
            Bundle extras = intentExtra.getExtras();
            diary_id = extras.getInt("diary_id");
            if(getSupportActionBar()!=null) {
                getSupportActionBar().setTitle(extras.getString("title"));
            }
            neww = false;
        } else {
            if(getSupportActionBar()!=null) {
                getSupportActionBar().setTitle("Add a note");
            }

            neww = true;
        }

        db = nBulletinDatabase.getInstance(getApplicationContext());
        sharedPreferences = getSharedPreferences(SP_NAME_API_TOKEN, 0);
        apiToken = sharedPreferences.getString("apiToken", "");

        editText_text = (EditText) findViewById(R.id.et_diary);
        editText_title = (EditText) findViewById(R.id.et_diary_title);
        updatedDate = (TextView) findViewById(R.id.textView_diary_date);

        if (!neww) {
            getLastUpdatedDetails();
        }

        FloatingActionButton fab_save = (FloatingActionButton) findViewById(R.id.fab_diary_save);
        fab_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (neww) {
                    saveDiary();
                } else {
                    updateDiary();
                }
                Snackbar snackbar = Snackbar
                        .make(view, "Saved!", Snackbar.LENGTH_SHORT);
                snackbar.show();
            }
        });
    }

    private void saveDiary() {
        Calendar calendar = Calendar.getInstance();
        date = String.valueOf(calendar.getTime());

        db.addDiary(apiToken, editText_title.getText().toString().trim(), editText_text.getText().toString(), String.valueOf(new Date().getTime()));
    }

    private void updateDiary() {
        Calendar calendar = Calendar.getInstance();
        date = String.valueOf(calendar.getTime());

        db.updateDiary(diary_id, apiToken, editText_title.getText().toString().trim(), editText_text.getText().toString(), String.valueOf(new Date().getTime()));
    }

    public void getLastUpdatedDetails() {

        Cursor cursor = db.getADiaryData(diary_id);
        if (cursor.moveToFirst()) {
            title = cursor.getString(2);
            text = cursor.getString(3);
            String DateToStr = cursor.getString(4);

            Date updated_date = new Date();
            updated_date.setTime(Long.parseLong(DateToStr));

            SimpleDateFormat displayFormat = new SimpleDateFormat("MMMM dd yyyy, HH:mm:ss zzzz");

            date = displayFormat.format(updated_date);

        }

        String last_saved = getString(R.string.updated_diary, date);
        updatedDate.setText(last_saved);

        editText_text.setText(text);
        editText_title.setText(title);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        if (!neww) {
            updateDiary();
        }
        super.onPause();
    }
}
