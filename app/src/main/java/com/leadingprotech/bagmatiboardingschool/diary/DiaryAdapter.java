package com.leadingprotech.bagmatiboardingschool.diary;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.database_classes.nBulletinDatabase;

import java.util.List;

/**
 * Created by karsk on 09/03/2017.
 */

public class DiaryAdapter extends RecyclerView.Adapter<DiaryAdapter.MyViewHolder> {

    nBulletinDatabase database;
    private List<DiaryModel> diaryModelList;
    private Context context;


    public DiaryAdapter(List<DiaryModel> diaryModelList, Context context) {
        this.context = context;
        this.diaryModelList = diaryModelList;
        database = new nBulletinDatabase(context);
    }

    @Override
    public DiaryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_diary, parent, false);
        return new DiaryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DiaryAdapter.MyViewHolder holder, final int position) {
        final DiaryModel diaryModel = diaryModelList.get(position);

        holder.title.setText(diaryModel.getTitle());
        holder.content.setText(diaryModel.getContent());
        holder.date.setText(diaryModel.getDate());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Activity_DiaryView.class);
                intent.putExtra("diary_id", diaryModel.getId());
                intent.putExtra("title", diaryModel.getTitle());

                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                view.getContext().startActivity(intent);
            }
        });

        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setMessage("Are you sure you want to delete this note?");
                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        database.deleteADiaryData(diaryModel.getId());
                        Toast.makeText(context, "Note deleted.", Toast.LENGTH_SHORT).show();
                        diaryModelList.remove(position);
                        notifyDataSetChanged();
                    }
                });
                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                return true;
            }
        });

    }

    @Override
    public int getItemCount() {
        return diaryModelList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title, content, date;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tv_diary_title);
            content = (TextView) itemView.findViewById(R.id.tv_diary_desc);
            date = (TextView) itemView.findViewById(R.id.tv_diary_date);
            cardView = (CardView) itemView.findViewById(R.id.cardView_row_diary);
        }

    }
}
