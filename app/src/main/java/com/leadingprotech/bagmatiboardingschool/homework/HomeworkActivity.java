package com.leadingprotech.bagmatiboardingschool.homework;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.leadingprotech.bagmatiboardingschool.R;

public class HomeworkActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework);
    }
}
