package com.leadingprotech.bagmatiboardingschool.homework;


public class HomeworkModel {

//TODO homework


    private String id;
    private String subject;
    private String title;
    private String document;
    private String link;
    private String remark;
    private String due_date, given_date;
    private String by_teacher;

    public HomeworkModel(String id, String subject, String title, String document, String link, String remark, String due_date, String given_date, String by_teacher) {
        this.id = id;
        this.subject = subject;
        this.title = title;
        this.document = document;
        this.link = link;
        this.remark = remark;
        this.due_date = due_date;
        this.given_date = given_date;
        this.by_teacher = by_teacher;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDue_date() {
        return due_date;
    }

    public void setDue_date(String due_date) {
        this.due_date = due_date;
    }

    public String getGiven_date() {
        return given_date;
    }

    public void setGiven_date(String given_date) {
        this.given_date = given_date;
    }

    public String getBy_teacher() {
        return by_teacher;
    }

    public void setBy_teacher(String by_teacher) {
        this.by_teacher = by_teacher;
    }
}
