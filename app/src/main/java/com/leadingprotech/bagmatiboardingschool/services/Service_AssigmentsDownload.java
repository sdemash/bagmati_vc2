package com.leadingprotech.bagmatiboardingschool.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.leadingprotech.bagmatiboardingschool.R;
import com.leadingprotech.bagmatiboardingschool.helper.ConnectionManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class Service_AssigmentsDownload extends Service {
    NotificationManager mNotifyManager;
    NotificationCompat.Builder bBuilder;
    File directory, file, tempFile;
    FileOutputStream out;
    InputStream in;
    HttpURLConnection con;
    int fileLength;
    int total = 0;

    String title, link;

    boolean downloading;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        makeForeground();
        Log.d("ddd", "onCreated");

    }

    private void makeForeground() {
        Notification notification;
        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        bBuilder = new NotificationCompat.Builder(
                this).setSmallIcon(R.drawable.ic_file_download)
                .setContentTitle(getResources().getString(R.string.institution_name))
                .setPriority(Notification.PRIORITY_MAX)
                .setContentText("Downloading file...").setOngoing(true);
        notification = bBuilder.build();

        notification.flags |= Notification.FLAG_FOREGROUND_SERVICE;
        notification.flags |= Notification.FLAG_NO_CLEAR;
        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        startForeground(1456, notification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.d("ddd", "onStarted");

        if (intent != null) {
            if (!downloading) {
                title = intent.getStringExtra("title");

                directory = new File(Environment.getExternalStorageDirectory() + "/" +
                        getResources().getString(R.string.app_name) + " Downloads/");
                tempFile = new File(Environment.getExternalStorageDirectory() + "/" +
                        getResources().getString(R.string.app_name) + " Downloads/" + title
                        + "." + "lptfile");
                file = new File(Environment.getExternalStorageDirectory() + "/" +
                        getResources().getString(R.string.app_name) + " Downloads/" + title);
                link = intent.getStringExtra("link");

                download();
            } else {
                Toast.makeText(this, "Please wait, another download is in progress.", Toast.LENGTH_SHORT).show();
            }
        } else {
            stopSelf();
        }

        return START_STICKY;
    }

    public void download() {
        downloading = true;

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == 0) {
                    Toast.makeText(Service_AssigmentsDownload.this, "File Successfully Downloaded", Toast.LENGTH_SHORT).show();

                    bBuilder.setContentText("Download Completed");
                    // Removes the progress bar
                    bBuilder.setProgress(0, 0, false);
                    mNotifyManager.notify(1456, bBuilder.build());

                    showNotification("Download Completed", true);
                    tempFile.renameTo(new File(Environment.getExternalStorageDirectory() + "/" +
                            getResources().getString(R.string.app_name) + " Downloads/" + title));

                    //send braodcast locally to Assignment activity to change download status
                    Intent iintent = new Intent(
                            "com.leadingprotech.assignment.downloaded");
                    LocalBroadcastManager.getInstance(Service_AssigmentsDownload.this).sendBroadcast(iintent);


                    stopSelf();
                } else if (msg.what == 1) {
                    Toast.makeText(Service_AssigmentsDownload.this, "Error downloading " + title , Toast.LENGTH_SHORT).show();

                    showNotification("Error downloading " + title, false);

                }
            }
        };
        bBuilder.setContentText("Downloading " + title);
        bBuilder.setProgress(100, 0, false);
        mNotifyManager.notify(1456, bBuilder.build());

        final Handler progressHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                bBuilder.setProgress(100, (total * 100) / fileLength, false);
                mNotifyManager.notify(1456, bBuilder.build());
            }
        };
        if (!file.exists()) {
            if (new ConnectionManager(Service_AssigmentsDownload.this).isNetworkConnected()) {
                Thread downloadThread = new Thread() {
                    public void run() {
                        try {
                            URL url = new URL(link);
                            con = (HttpURLConnection) url.openConnection();
                            con.setRequestMethod("GET");
                            con.setDoInput(true);
                            con.connect();
                            in = con.getInputStream();
                            fileLength = con.getContentLength();
                            System.out.println(con.getResponseCode());
                            if (!directory.exists()) {
                                directory.mkdirs();
                            }

                            out = new FileOutputStream(tempFile);
                            byte[] buffer = new byte[1024];
                            int len1 = 0;
                            total = 0;
                            while ((len1 = in.read(buffer)) > 0) {
                                out.write(buffer, 0, len1);
                                total += len1;
                                progressHandler.sendEmptyMessage(0);
                            }
                            handler.sendEmptyMessage(0);
                            total = 0;
                            in.close();
                            out.close();

                        } catch (java.io.IOException e) {
                            handler.sendEmptyMessage(1);
                            e.printStackTrace();
                            stopSelf();
                        }
                    }
                };
                downloadThread.start();
                Toast.makeText(Service_AssigmentsDownload.this, "Download has started!", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(Service_AssigmentsDownload.this, "Connection Error!", Toast.LENGTH_SHORT).show();
                stopSelf();
            }
        } else {
            Toast.makeText(Service_AssigmentsDownload.this, "File Already Exists", Toast.LENGTH_SHORT).show();
            stopSelf();
        }
    }

    @Override
    public void onDestroy() {
        stopForeground(true);
        Log.d("ddd", "onDestroyed");
        super.onDestroy();
    }

    public void showNotification(String msg, boolean success) {
        Notification n = new Notification.Builder(Service_AssigmentsDownload.this)
                .setContentTitle(getResources().getString(R.string.institution_name))
                .setContentText(msg)
                .setSmallIcon(success ? R.drawable.ic_file_downloaded : R.drawable.ic_file_download_error)
                .setAutoCancel(true).build();
        NotificationManager notificationManagerr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        notificationManagerr.notify(0, n);
    }

}
